<%@ Page Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="OrdersAdmin.aspx.cs"
    Inherits="OrdersAdmin" Title="Untitled Page" %>

<%@ Register Src="UserControls/OrderDetailsAdmin.ascx" TagName="OrderDetailsAdmin"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4>Orders Admin</h4>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="alertBox" class="alert alert-error" runat="server" visible="false">
        <asp:Label ID="errorLabel" runat="server" EnableViewState="False"></asp:Label>
        <asp:RangeValidator ID="startDateValidator" runat="server" ControlToValidate="startDateTextBox"
            Display="None" ErrorMessage="Invalid start date" MaximumValue="1/1/2009" MinimumValue="1/1/1999"
            Type="Date"></asp:RangeValidator>
        <asp:RangeValidator ID="endDateValidator" runat="server" ControlToValidate="endDateTextBox"
            Display="None" ErrorMessage="Invalid end date" MaximumValue="1/1/2009" MinimumValue="1/1/1999"
            Type="Date"></asp:RangeValidator>
        <asp:CompareValidator ID="compareDatesValidator" runat="server" ControlToCompare="endDateTextBox"
            ControlToValidate="startDateTextBox" Display="None" ErrorMessage="Start date should be more recent than end date"
            Operator="LessThan" Type="Date"></asp:CompareValidator><br />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="AdminErrorText"
            HeaderText="Please fix these errors before submitting requests:" />
    </div>
    <ul class="order-admin-list">
        <li class="clearfix">
            <div>
                <asp:Label runat="server">Show the most recent</asp:Label>
                <asp:TextBox ID="recentCountTextBox" runat="server" MaxLength="4" Width="40px" Text="20" />
                <asp:Label runat="server">records</asp:Label>
            </div>

            <asp:Button ID="byRecentGo" runat="server" CssClass="btn" Text="Go" OnClick="byRecentGo_Click"
                CausesValidation="False" />
        </li>
        <li class="clearfix">
            <div>
                <asp:Label runat="server">Show all records created between</asp:Label>
                <asp:TextBox ID="startDateTextBox" runat="server" Width="72px" />
                <asp:Label runat="server">and</asp:Label>
                <asp:TextBox ID="endDateTextBox" runat="server" Width="72px" />
            </div>

            <asp:Button ID="byDateGo" runat="server" CssClass="btn" Text="Go" OnClick="byDateGo_Click" />
        </li>
        <li class="clearfix">
            <div>
                <asp:Label runat="server">Show all unverified, uncanceled orders</asp:Label>
            </div>

            <asp:Button ID="unverfiedGo" runat="server" CssClass="btn" Text="Go" OnClick="unverfiedGo_Click" CausesValidation="False" />
        </li>
        <li class="clearfix">
            <div>
                <asp:Label runat="server">Show all verified, uncompleted orders</asp:Label>
            </div>

            <asp:Button ID="uncompletedGo" runat="server" CssClass="btn" Text="Go" OnClick="uncompletedGo_Click" CausesValidation="False" />
        </li>
    </ul>
    <asp:GridView ID="grid" CssClass="table table-bordered table-striped table-hover" border="0" runat="server" AutoGenerateColumns="False" DataKeyNames="OrderID" OnSelectedIndexChanged="grid_SelectedIndexChanged">
        <Columns>
            <asp:BoundField DataField="OrderID" HeaderText="Order ID" ReadOnly="True" SortExpression="OrderID" />
            <asp:BoundField DataField="DateCreated" HeaderText="Date Created" ReadOnly="True"
                SortExpression="DateCreated" />
            <asp:BoundField DataField="DateShipped" HeaderText="Date Shipped" ReadOnly="True"
                SortExpression="DateShipped" />
            <asp:CheckBoxField DataField="Verified" HeaderText="Verified" ReadOnly="True" SortExpression="Verified" />
            <asp:CheckBoxField DataField="Completed" HeaderText="Completed" ReadOnly="True" SortExpression="Completed" />
            <asp:CheckBoxField DataField="Canceled" HeaderText="Canceled" ReadOnly="True" SortExpression="Canceled" />
            <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" ReadOnly="True"
                SortExpression="CustomerName" />
            <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Select">
                <ControlStyle CssClass="btn btn-primary" />
            </asp:ButtonField>
        </Columns>
    </asp:GridView>
    <div class="row-fluid">
        <uc1:OrderDetailsAdmin EnableViewState="false" ID="orderDetailsAdmin" runat="server"></uc1:OrderDetailsAdmin>
    </div>

</asp:Content>
