<%@ Page Language="C#" MasterPageFile="~/GuitarStore.master" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Search" Title="Untitled Page" %>

<%@ Register Src="UserControls/ProductsList.ascx" TagName="ProductsList" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/CategoriesList.ascx" TagPrefix="uc1" TagName="CategoriesList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
  <asp:Label ID="titleLabel" runat="server" CssClass="search-title"></asp:Label>
      <div class="container-fluid container-fluid-no-padding">
        <div class="row-fluid">
            <div class="span3">
                <!--Sidebar content-->
                <uc1:CategoriesList runat="server" id="CategoriesList" />
            </div>
            <div class="span9">
                <!--Body content-->
                <uc1:ProductsList ID="ProductsList1" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

