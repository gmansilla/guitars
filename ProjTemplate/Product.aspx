<%@ Page Language="C#" MasterPageFile="~/GuitarStore.master" AutoEventWireup="true"
    CodeFile="Product.aspx.cs" Inherits="Product" Title="Untitled Page" %>

<%@ Register Src="UserControls/ProductRecommendations.ascx" TagName="ProductRecommendations"
    TagPrefix="uc1" %>
    <%@ Register Src="UserControls/ProductRating.ascx" TagName="ProductRating"
    TagPrefix="uc1" %>
<asp:Content ID="content" ContentPlaceHolderID="contentPlaceHolder" runat="Server">
    <div class="product-title-div">
        <h1 class="product-title">
            <asp:Label ID="titleLabel" runat="server" Text="Label"></asp:Label>
        </h1>
        <div class="row-fluid">
            <div class="span8 product-sub-text">
                <span>Model #</span>
                <asp:Label ID="productIdLabel" runat="server" Text="Label"></asp:Label>
            </div>
            <div class="span4">
            </div>
        </div>

    </div>
    <div class="row-fluid product-desc-div">
        <div class="span8 product-img-div">
            <asp:Image ID="productImage" runat="server" />
        </div>
        <div class="span4">
            <div class="row-fluid">
                <div class="product-price-div span6">
                    <asp:Label CssClass="product-price" ID="priceLabel" runat="server" Text="Label" />
                </div>
                <div class="product-option-div span6">
                    <ul class="product-options">
                        <li class="freeShipping basePromo"><span></span>Free Shipping</li>
                        <li class="topRated basePromo"><span></span>Top Rated</li>
                        <li class="topSeller basePromo"><span></span>Top Seller</li>
                        <li class="guaranteePriceMatch basePromo"><span></span>Price Match</li>
                        <li class="guarantee basePromo clearfix"><span></span><span class="guarantee-text">Satisfaction Guarantee</span></li>
                    </ul>
                </div>
            </div>
            <div class="row-fluid product-purc clearfix">
                <div class="pull-left product-qty-div">
                    <div><span class=<%= (int.Parse(productQty.Text) > 0) ? "\"text-success\">In Stock" : "\"text-error\">Out of Stock>"%></span></div>
                    <div>
                        <span>Quantity:</span>
                        <asp:Label CssClass="product-qty" ID="productQty" runat="server" Text="Label" />
                    </div>
                </div>
            <div class="pull-left">
                    <asp:LinkButton ID="addToCartButton" runat="server" Text="Add to Cart" CssClass="btn-primary btn-addcart" OnClick="addToCartButton_Click" />
            </div>
            </div>
        </div>
    </div>
    <div class="section-heading rounded">
        <h2>Product Overview</h2>
    </div>
    <div class="row-fluid row-alternate product-spec-div">
         <asp:Label CssClass="ProductDescription" ID="descriptionLabel" runat="server" Text="Label"></asp:Label>
         <h2 class="heading">Features</h2>
         <asp:Label CssClass="ProductFeatures" ID="featuresLabel" runat="server" Text="Label"></asp:Label>
    </div>
    
    <uc1:ProductRecommendations ID="ProductRecommendations1" runat="server"></uc1:ProductRecommendations>
    <uc1:ProductRating ID="ProductRating1" runat="server"></uc1:ProductRating>
</asp:Content>
