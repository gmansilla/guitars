<%@ Page Language="C#" MasterPageFile="~/GuitarStore.master" AutoEventWireup="true"
    CodeFile="Login.aspx.cs" Inherits="Login" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolder" runat="Server">
    <asp:Login ID="login" runat="server" RenderOuterTable="false " OnLoginError="OnLoginError">
        <LayoutTemplate>

            <div class="row-fluid">
                <div class="section-heading rounded shopping-cart-header">
                    <h2>Please login</h2>
                </div>
                <div class="shopping-cart-total row-alternate text-center">
                    <div id="alertBox" class="alert alert-error" runat="server" visible="false">
                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                    </div>
                    <ul class="login-list">
                        <li class="clearfix control-group">
                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label></td>
                            <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                ErrorMessage="User Name is required." ToolTip="User Name is required." Display="Dynamic"></asp:RequiredFieldValidator>
                        </li>
                        <li class="clearfix control-group">
                            <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                            <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                ErrorMessage="Password is required." ToolTip="Password is required." Display="Dynamic"></asp:RequiredFieldValidator>
                        </li>
                        <li class="clearfix login-remember-li">
                            <asp:CheckBox ID="RememberMe" CssClass="login-remember" runat="server" Text="Remember me next time." />

                        </li>
                        <li class="clearfix">
                            <asp:Button ID="LoginButton" CssClass="btn btn-primary btn-large btn-login" runat="server" CommandName="Login" Text="Log In" OnClientClick="return BtnClick();" />
                        </li>
                    </ul>
                    <script type="text/javascript">
                        function BtnClick() {
                            var val = Page_ClientValidate();
                            if (!val) {
                                var i = 0;
                                for (; i < Page_Validators.length; i++) {
                                    if (!Page_Validators[i].isvalid) {
                                        $("#" + Page_Validators[i].controltovalidate)
                                         .parent().addClass("error");
                                    } else {
                                        $("#" + Page_Validators[i].controltovalidate)
                                        .parent().removeClass("error");
                                    }
                                }
                            }
                            return val;
                        }
                    </script>
                </div>
            </div>
        </LayoutTemplate>
    </asp:Login>
</asp:Content>
