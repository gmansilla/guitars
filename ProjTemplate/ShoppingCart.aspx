<%@ Page Language="C#" MasterPageFile="~/GuitarStore.master" AutoEventWireup="true" CodeFile="ShoppingCart.aspx.cs" Inherits="ShoppingCart" Title="Untitled Page" %>

<%@ Register Src="UserControls/ProductRecommendations.ascx" TagName="ProductRecommendations"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolder" runat="Server">
    <div class="alert alert-success">
        <asp:Label ID="titleLabel" runat="server" Text="Your Shopping Cart" /><br />
        <asp:Label ID="statusLabel" runat="server" />
    </div>

    <div class="row-fluid shopping-cart-button">
        <asp:Button ID="continueShoppingButton" runat="server" Text="Continue Shopping" CssClass="shopping-cart-continue pull-left" OnClick="continueShoppingButton_Click" />
        <asp:Button ID="checkoutButton" runat="server" CssClass="shopping-cart-checkout pull-right" Text="Proceed to Checkout" OnClick="checkoutButton_Click" />
    </div>
    <div class="section-heading rounded shopping-cart-div">
        <h2>Shopping Cart</h2>
    </div>
    <div class="row-fluid row-alternate shopping-cart">
        <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" DataKeyNames="ProductID" Width="100%" BorderWidth="0px"
            OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" CssClass="shopping-cart-table">
            <Columns>
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <div class="product-list-image">
                            <a href="Product.aspx?ProductId=<%#Eval("ProductID")%>">
                                <img src="ProductImages/<%#Eval("Image")%>" /></a>
                        </div>
                    </ItemTemplate>
                    <HeaderStyle CssClass="shopping-cart-image" />
                    <ItemStyle CssClass="shopping-cart-image" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Product Name">
                    <ItemTemplate>
                        <p class="product-list-name">
                            <a class="ProductName" href="Product.aspx?ProductId=<%#Eval("ProductID")%>"><%#Eval("Name")%></a>
                        </p>
                        <p class="product-list-sku">Model #: 000<%#Eval("ProductID")%></p>

                    </ItemTemplate>
                    <HeaderStyle CssClass="shopping-cart-item" />
                    <ItemStyle CssClass="shopping-cart-item" />
                </asp:TemplateField>
                <asp:BoundField DataField="Price" DataFormatString="{0:c}" HeaderText="Price" ReadOnly="True"
                    SortExpression="Price">
                    <ItemStyle CssClass="shopping-cart-price" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Quantity">
                    <ItemTemplate>
                        <asp:TextBox ID="editQuantity" runat="server" CssClass="shopping-cart-qty" Width="24px" MaxLength="2" Text='<%#Eval("Quantity")%>' />
                        <p class="shopping-cart-option">
                            <asp:LinkButton CommandName="Update" Text="Update" runat="server"></asp:LinkButton>
                        </p>
                        <p class="shopping-cart-option">
                            <asp:LinkButton CommandName="Delete" Text="Delete" runat="server"></asp:LinkButton>
                        </p>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Subtotal" DataFormatString="{0:c}" HeaderText="Subtotal"
                    ReadOnly="True" SortExpression="Subtotal">
                    <ItemStyle CssClass="shopping-cart-price" />
                </asp:BoundField>

            </Columns>
        </asp:GridView>
    </div>
    <div class="row-fluid">
        <div class="pull-right span4">
            <div class="section-heading rounded shopping-cart-header">
                <h2>Order Summary</h2>
            </div>
            <div class="shopping-cart-total row-alternate">
                <span class="shopping-cart-total-label pull-left">Order Total:</span>
                <asp:Label ID="totalAmountLabel" runat="server" Text="Label" CssClass="shopping-cart-total-price pull-right" />
            </div>
        </div>
    </div>
    <div class="row-fluid shopping-cart-button">
        <asp:Button ID="btnCheckPutBtm" runat="server" CssClass="shopping-cart-checkout pull-right" Text="Proceed to Checkout" OnClick="checkoutButton_Click" />
    </div>
    <uc1:ProductRecommendations ID="ProductRecommendations1" runat="server"></uc1:ProductRecommendations>
</asp:Content>

