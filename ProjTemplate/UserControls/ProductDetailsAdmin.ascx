<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductDetailsAdmin.ascx.cs" Inherits="ProductDetailsAdmin" %>

<div class="alert alert-success">
    <asp:Label ID="statusLabel" runat="server" Text="Product Details Loaded for "></asp:Label>
    <asp:Label ID="productNameLabel" runat="server" />
</div>
<asp:HyperLink ID="goBackLink" CssClass="btn btn-primary" runat="server">(go back to products)</asp:HyperLink>
<ul class="order-admin-list product-admin-list">
    <li class="clearfix">
        <asp:Label runat="server" AssociatedControlID="categoriesLabel">Product belongs to these categories:</asp:Label>
        <asp:Label ID="categoriesLabel" runat="server"></asp:Label>
    </li>
    <li class="clearfix">
        <asp:Label runat="server" AssociatedControlID="categoriesListRemove">Remove product from this category:</asp:Label>
        <asp:DropDownList ID="categoriesListRemove" runat="server"></asp:DropDownList>
        <asp:Button ID="removeButton" runat="server" CssClass="btn" Text="Go!" OnClick="removeButton_Click" />
        <asp:Button ID="deleteButton" runat="server" CssClass="btn" Text="DELETE FROM CATALOG" OnClick="deleteButton_Click" />
    </li>
    <li class="clearfix">
        <asp:Label runat="server" AssociatedControlID="categoriesListAssign">Assign product to this category:</asp:Label>
        <asp:DropDownList ID="categoriesListAssign" runat="server">
        </asp:DropDownList>
        <asp:Button ID="assignButton" CssClass="btn" runat="server" Text="Go!" OnClick="assignButton_Click" />
    </li>
    <li class="clearfix">
        <asp:Label ID="moveLabel" AssociatedControlID="categoriesListMove" runat="server" Text="Move product to this category:" />
        <asp:DropDownList ID="categoriesListMove" runat="server">
        </asp:DropDownList>
        <asp:Button ID="moveButton" runat="server" CssClass="btn" Text="Go!" OnClick="moveButton_Click" />
    </li>
    <li class="clearfix">
        <asp:Label runat="server" AssociatedControlID="image1FileNameLabel">Image file name:</asp:Label>
        <asp:Label ID="image1FileNameLabel" runat="server"></asp:Label>
        <asp:FileUpload ID="image1FileUpload" runat="server" CssClass="btn" />
        <asp:Button ID="upload1Button" runat="server" CssClass="btn" Text="Upload" OnClick="upload1Button_Click" /><br />
        <asp:Image ID="image1" runat="server" CssClass="product-admin-image" />
    </li>
</ul>
