<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductsList.ascx.cs"
    Inherits="ProductsList" %>
<div id="paginationListTop" class="row-fluid pagination-div" runat="server" visible="false">
    <ul class="pager">
        <li id="previousContainerTop" class="previous" runat="server">
            <asp:HyperLink ID="previousLinkTop" runat="server">&larr; Previous</asp:HyperLink>
        </li>
        <li>
            <asp:Label ID="pagingLabelTop" runat="server" CssClass="PagingText" /></li>
        <li id="nextContainerTop" class="next" runat="server">
            <asp:HyperLink ID="nextLinkTop" runat="server">Next &rarr;</asp:HyperLink>
        </li>
    </ul>
</div>
<ul class="product-list-view">
    <asp:Repeater ID="list" runat="server" EnableViewState="False" OnItemCommand="list_ItemCommand" OnItemDataBound="list_ItemDataBound">
        <ItemTemplate>
            <li class="clearfix">
                <div class="product-list-image">
                    <a href='Product.aspx?ProductID=<%# Eval("ProductID")%>'>
                        <img src='ProductImages/<%# Eval("Image") %>' border="0" />
                    </a>
                </div>
                <div class="span7 product-list-desc">
                    <p class="product-list-name">
                        <a class="ProductName" href='Product.aspx?ProductID=<%# Eval("ProductID")%>'>
                            <%# Eval("Name") %>
                        </a>
                    </p>
                    <p class="product-list-sku">Model #: 000<%# Eval("ProductId") %></p>
                    <div class="clearfix">
                        <div id="reviewStars" class="review-summary-stars-small pull-left" runat="server"></div>
                        <div id="reviewScore" class="review-score-small pull-left" runat="server"></div>
                    </div>
                    <p class="product-list-<%# (int)Eval("Stock") > 0 ? "available": "not-available" %>"><%# (int)Eval("Stock") > 0 ? "Available" : "Not Available"%></p>
                </div>
                <div class="span2 product-list-price">
                    <span><%# Eval("Price", "{0:c}") %></span>
                    <p>
                        <asp:Button ID="addToCartButton" runat="server" Text="Add to Cart" CommandArgument='<%# Eval("ProductID") %>' CssClass="btn btn-primary" />
                    </p>
                </div>
                <asp:HiddenField ID="productIdHidden" runat="server" Value='<%# Eval("ProductID") %>' />
            </li>
        </ItemTemplate>
    </asp:Repeater>
</ul>
<div id="paginationListBtm" class="row-fluid pagination-div" runat="server" visible="false">
    <ul class="pager">
        <li id="previousContainerBottom" class="previous" runat="server">
            <asp:HyperLink ID="previousLinkBottom" runat="server">&larr; Previous</asp:HyperLink>
        </li>
        <li>
            <asp:Label ID="pagingLabelBottom" runat="server" CssClass="PagingText" /></li>
        <li id="nextContainerBottom" class="next" runat="server">
            <asp:HyperLink ID="nextLinkBottom" runat="server">Next &rarr;</asp:HyperLink>
        </li>
    </ul>
</div>
