<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductRecommendations.ascx.cs" Inherits="ProductRecommendations" %>
<div id="productRecomDiv" class="product-recom-div" runat="server" visible="false">
    <strong class="product-recom-header">Also consider<span></span></strong>
    <div id="product-recom-inner" class="row-fluid row-alternate">
        <asp:Repeater ID="list" runat="server">
            <ItemTemplate>
                <div class="span2">
                    <a class="prod-recom-link" href="Product.aspx?ProductID=<%# Eval("ProductID") %>">
                        <img src="ProductImages/<%# Eval("Image") %>" alt="<%# Eval("Name") %>"/>
                        <p><%# Eval("Name") %></p>
                    </a>
                    <p class="prod-recom-price"><%# Eval("Price", "{0:c}") %></p>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
