<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserInfo.ascx.cs" Inherits="UserInfo" %>
<ul class="nav pull-right user-links-list">
    <asp:LoginView ID="LoginView1" runat="server">
        <AnonymousTemplate>
            <li>
                <span>Welcome. Please
                    <asp:LoginStatus ID="LoginStatus1" runat="server" LoginText="sign in" />
                </span>
            </li>
        </AnonymousTemplate>
        <RoleGroups>
            <asp:RoleGroup Roles="User">
                <ContentTemplate>
                    <li>
                        <asp:LoginName ID="LoginName2" runat="server" FormatString="Welcome Back <b>{0}</b>"
                            CssClass="UserInfoText" />
                    </li>
                    <li>
                        <asp:LoginStatus ID="LoginStatus2" runat="server" CssClass="UserInfoLink" />
                    </li>
                </ContentTemplate>
            </asp:RoleGroup>
            <asp:RoleGroup Roles="Administrators">
                <ContentTemplate>
                    <li>
                        <asp:LoginName ID="LoginName2" runat="server" FormatString="Welcome Back <b>{0}</b>"
                            CssClass="UserInfoText" />
                    </li>
                    <li>
                        <asp:LoginStatus ID="LoginStatus2" runat="server" CssClass="UserInfoLink" />
                    </li>
                    <li>
                        <ul class="nav">
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle admin-options" href="#">Admin Options<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="CatalogAdmin.aspx">Catalog Admin</a></li>
                                    <li><a href="ShoppingCartAdmin.aspx">Shopping Cart Admin</a></li>
                                    <li><a href="OrdersAdmin.aspx">Orders Admin</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ContentTemplate>
            </asp:RoleGroup>
        </RoleGroups>
    </asp:LoginView>
</ul>
