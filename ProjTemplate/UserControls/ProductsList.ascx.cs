using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;

public partial class ProductsList : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PopulateControls();
    }

    private void PopulateControls()
    {
        // Retrieve DepartmentID from the query string
        string departmentId = Request.QueryString["DepartmentID"];
        // Retrieve CategoryID from the query string
        string categoryId = Request.QueryString["CategoryID"];
        // Retrieve Page from the query string
        // Retrieve Brand from the query string
        string brandId = Request.QueryString["BrandID"];
        string page = Request.QueryString["Page"];
        if (page == null) page = "1";
        // Retrieve Search string from query string
        string searchString = Request.QueryString["Search"];
        // How many pages of products?
        int howManyPages = 1;
        // If performing a product search
        if (searchString != null)
        {
            // Retrieve AllWords from query string
            // string allWords = Request.QueryString["AllWords"];
            // Perform search
            list.DataSource = CatalogAccess.Search(searchString, page, out howManyPages);
            list.DataBind();
        }
        // If browsing a category...
        else if (categoryId != null && brandId == null)
        {
            // Retrieve list of products in a category
            list.DataSource = CatalogAccess.GetProductsInCategory(categoryId, page, out howManyPages);
            list.DataBind();
        }
        else if (departmentId != null)
        {
            // Retrieve list of products on department promotion
            list.DataSource = CatalogAccess.GetProductsOnDepartmentPromotion(departmentId, page, out howManyPages);
            list.DataBind();
        }
        else if (categoryId != null && brandId != null)
        {
            // Retrieve list of products on category and brand
            list.DataSource = CatalogAccess.GetProductsInBrandAndCategory(categoryId, brandId, page, out howManyPages);
            list.DataBind();
        }
        else
        {
            // Retrieve list of products on catalog promotion
            //list.DataSource = CatalogAccess.GetProductsOnCatalogPromotion(page, out howManyPages);
            list.DataBind();
        }
        // display paging controls
        if (howManyPages > 1)
        {
            // have the current page as integer
            int currentPage = Int32.Parse(page);
            // make controls visible
            paginationListTop.Visible = true;
            paginationListBtm.Visible = true;
            // set the paging text
            pagingLabelTop.Text = "Page " + page + " of " + howManyPages.ToString();
            pagingLabelBottom.Text = pagingLabelTop.Text;
            // create the Previous link
            if (currentPage == 1)
            {
                previousLinkTop.Enabled = false;
                previousContainerTop.Attributes.Add("class", "previous disabled");
                previousContainerBottom.Attributes.Add("class", "previous disabled");
                previousLinkBottom.Enabled = false;
            }
            else
            {
                NameValueCollection query = Request.QueryString;
                string paramName, newQueryString = "?";
                for (int i = 0; i < query.Count; i++)
                    if (query.AllKeys[i] != null)
                        if ((paramName = query.AllKeys[i].ToString()).ToUpper() != "PAGE")
                            newQueryString += paramName + "=" + query[i] + "&";
                previousLinkTop.NavigateUrl = Request.Url.AbsolutePath + newQueryString + "Page=" + (currentPage - 1).ToString();
                previousLinkBottom.NavigateUrl = previousLinkTop.NavigateUrl;
            }
            // create the Next link
            if (currentPage == howManyPages)
            {
                nextLinkTop.Enabled = false;
                nextContainerTop.Attributes.Add("class", "next disabled");
                nextContainerBottom.Attributes.Add("class", "next disabled");
                nextLinkBottom.Enabled = false;
            }
            else
            {
                NameValueCollection query = Request.QueryString;
                string paramName, newQueryString = "?";
                for (int i = 0; i < query.Count; i++)
                    if (query.AllKeys[i] != null)
                        if ((paramName = query.AllKeys[i].ToString()).ToUpper() != "PAGE")
                            newQueryString += paramName + "=" + query[i] + "&";
                nextLinkTop.NavigateUrl = Request.Url.AbsolutePath + newQueryString + "Page=" + (currentPage + 1).ToString();
                nextLinkBottom.NavigateUrl = nextLinkTop.NavigateUrl;
            }
        }
    }

    protected void list_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //display review info 
            HiddenField productIdHidden = (HiddenField)e.Item.FindControl("productIdHidden");
            string productId = productIdHidden.Value;
            DataTable reviewData = CatalogAccess.GetProductRating(productId);
            string rating = reviewData.Rows[0]["rate"].ToString() == "" ? "0" : reviewData.Rows[0]["rate"].ToString();

            HtmlGenericControl reviewSummaryStars = (HtmlGenericControl)e.Item.FindControl("reviewStars");
            HtmlGenericControl reviewSummaryScore = (HtmlGenericControl)e.Item.FindControl("reviewScore");
            reviewSummaryStars.Attributes.Add("class", "review-summary-stars-small pull-left " + getBackgroundPosition(rating, "small"));
            reviewSummaryScore.InnerText = reviewData.Rows[0]["numberOfRates"].ToString() + " review(s)";

        }
    }

    private string getBackgroundPosition(string rating, string bigsmall)
    {
        rating = Math.Floor(Double.Parse(rating)).ToString();
        return "star" + rating + "-" + bigsmall;
        //return "star4-" + bigsmall;
    }

    // fires when an Add to Cart button is clicked
    protected void list_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        // The CommandArgument of the clicked Button contains the ProductID 
        string productId = e.CommandArgument.ToString();
        // Add the product to the shopping cart
        ShoppingCartAccess.AddItem(productId);
    }
}