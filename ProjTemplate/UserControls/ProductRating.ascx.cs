﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class WebUserControl : System.Web.UI.UserControl
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        String productID = Request.QueryString["ProductID"];
        if (!IsPostBack)
        {
            PopulateControls(productID);
        }

       
       
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        String productIdStr = Request.QueryString["ProductID"];
        MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
        
        
        LoginView lv = (LoginView)FindControl("LoginView1");
        HiddenField t2 = (HiddenField)lv.FindControl("RatingId");
        TextBox t3 = (TextBox)lv.FindControl("CommentId");
        string userID = user.ProviderUserKey.ToString();

        int productID = int.Parse(productIdStr);
        int rating = int.Parse(t2.Value);
        string comments = t3.Text;
        Boolean success = CatalogAccess.InsertRating(userID, productID, rating, comments);
        PopulateControls(productIdStr);

       
    }

    private void PopulateControls(string productID)
    {
        DataTable data = CatalogAccess.GetRatings(productID);
        Repeater1.DataSource = data;
        Repeater1.DataBind();

        DataTable reviewData = CatalogAccess.GetProductRating(productID);
        string rating = reviewData.Rows[0]["rate"].ToString() == "" ? "0" : reviewData.Rows[0]["rate"].ToString();
        reviewSummaryStars.Attributes.Add("class", "review-summary-stars-big pull-left " + getBackgroundPosition(rating, "big")); 
        reviewSummaryScore.InnerText = String.Format("{0:0.0}", Double.Parse(rating));
        reviewSummaryCount.InnerText = "Based on " + reviewData.Rows[0]["numberOfRates"].ToString() + " review(s).";
    }

    private string getBackgroundPosition(string rating, string bigsmall)
    {
        rating = Math.Floor(Double.Parse(rating)).ToString();
        return "star" + rating + "-" + bigsmall;
        //return "star4-" + bigsmall;
    }
}






