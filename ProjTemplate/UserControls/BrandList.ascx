<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BrandList.ascx.cs" Inherits="CategoriesList" %>
<div class="nav-category-header">
    <p>Brand</p>
</div>
<div class="nav-category-body">
    <ul>
        <asp:Repeater ID="list" runat="server">
            <ItemTemplate>
                <li>&nbsp;&raquo;
    <asp:HyperLink
        ID="HyperLink1"
        runat="server"
        NavigateUrl='<%# "../Catalog.aspx?CategoryID=" + Request.QueryString["CategoryID"] + "&BrandID=" + Eval("BrandID")  %>'
        Text='<%# Eval("BName") %>'
        CssClass='<%# Eval("BrandID").ToString() == Request.QueryString["BrandID"] ? "CategorySelected" : "CategoryUnselected" %>'>>
    </asp:HyperLink>
                </li>

    
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</div>
