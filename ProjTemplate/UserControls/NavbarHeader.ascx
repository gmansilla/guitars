﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NavbarHeader.ascx.cs" Inherits="NavbarHeader" %>
<%@ Register Src="~/UserControls/UserInfo.ascx" TagPrefix="uc1" TagName="UserInfo" %>

<!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<!-- This code is taken from http://twitter.github.com/bootstrap/examples/hero.html -->
<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target="#nav-collapse1">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div id="nav-collapse1" class="nav-collapse collapse">
                <div class="pull-left header-left">
                    <a id="A1" class="brand" href="~/" runat="server">
                        <img src="img/guitar_studio.png" /></a>

                </div>
                <div class="pull-right header-right">
                    <div class="row-fluid">
                        <uc1:UserInfo runat="server" ID="UserInfo" />
                    </div>

                    <div class="input-append search-box">
                        <input runat="server" id="searchTextBox" class="span4" type="text" placeholder="Enter keywords, item # or catalog #">
                        <asp:Button ID="goButton" runat="server" CssClass="btn btn-search" OnClick="goButton_Click" />
                    </div>

                    <ul class="nav">
                        <asp:Repeater ID="list" runat="server" OnItemDataBound="list_ItemDataBound">
                            <ItemTemplate>
                                <li class="dropdown<%# GetItemClass(Container.ItemIndex) %>">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><%# Eval("Name") %><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <asp:Label ID="lblCategoryId" runat="server" Visible="false" Text='<%# Bind("CategoryID") %>'>'></asp:Label>
                                        <li><a href="Catalog.aspx?CategoryID=<%# Eval("CategoryID") %>">View all <%# Eval("Name")%> Equipments</a></li>
                                        <li class="divider"></li>
                                        <li class="nav-header">Brands</li>
                                        <asp:Repeater id="brandList" runat="server">
                                            <ItemTemplate>
                                            <li><a href="Catalog.aspx?CategoryID=<%# Eval("CategoryID") %>&BrandID=<%# Eval("BrandId") %>"><%# Eval("BName") %></a></li>
                                            </ItemTemplate>
                                        
                                        </asp:Repeater>
                                       <!-- <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li class="nav-header">Nav header</li>
                                        <li><a href="#">Separated link</a></li>
                                        <li><a href="#">One more separated link</a></li>-->
                                    </ul>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="pull-right div-cart">
                        <a title="Cart" class="cart-image" href="ShoppingCart.aspx">
                            <img class="div-cart-img" alt="Cart" src="img/icon_cart.png" />
                            <span id="shoppingCartCount" runat="server" class="shipping-cart-count"></span>
                        </a>
                    </div>
                </div>

            </div>
        </div>


    </div>
</div>
