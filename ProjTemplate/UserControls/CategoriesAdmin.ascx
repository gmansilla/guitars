<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CategoriesAdmin.ascx.cs"
    Inherits="CategoriesAdmin" %>
<div class="row-fluid">
    <div class="alert alert-success">
        <asp:Label ID="statusLabel" runat="server" Text="Categories Loaded"></asp:Label></div>
    <p>
        <asp:Label ID="locationLabel" runat="server" Text="Displaying All Categories..."></asp:Label></p>
</div>

<asp:GridView ID="grid" CssClass="table table-bordered table-hover table-striped" runat="server" AutoGenerateColumns="False" DataKeyNames="CategoryID"
    Width="100%" OnRowCancelingEdit="grid_RowCancelingEdit" OnRowDeleting="grid_RowDeleting" OnRowEditing="grid_RowEditing" OnRowUpdating="grid_RowUpdating">
    <Columns>
        <asp:TemplateField HeaderText="Category Description" SortExpression="Description">
            <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Name") %>'>
                </asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="descriptionTextBox" runat="server" TextMode="MultiLine" Text='<%# Bind("Name") %>'
                    Height="70px" Width="350px" />
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:HyperLink runat="server" ID="link" NavigateUrl='<%# "../CatalogAdmin.aspx?CategoryID=" + Eval("CategoryID")%>'
                    Text="View Products">
                </asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:CommandField ShowEditButton="True" />
        <asp:ButtonField CommandName="Delete" Text="Delete" />
    </Columns>
</asp:GridView>

<div class="row-fluid">
    <h4>Create a new category:</h4>
    <table cellspacing="0">
        <tr>
            <td valign="top" width="100">Name:</td>
            <td>
                <asp:TextBox ID="newName" runat="server" Width="400px" />
            </td>
        </tr>
    </table>
    <asp:Button ID="createCategory" Text="Create Category" runat="server" CssClass="btn btn-primary" OnClick="createCategory_Click" />
</div>

