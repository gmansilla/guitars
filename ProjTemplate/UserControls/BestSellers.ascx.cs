using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;

public partial class ProductsList : System.Web.UI.UserControl
{
  protected void Page_Load(object sender, EventArgs e)
  {
      if (!Page.IsPostBack)
      {
          PopulateControls();
      }
    
  }

  private void PopulateControls()
  {

    // Retrieve list of products on catalog promotion
    list.DataSource = CatalogAccess.GetBestSellingProducts();
    list.DataBind();
  
  }

  // fires when an Add to Cart button is clicked
  protected void list_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    // The CommandArgument of the clicked Button contains the ProductID 
    string productId = e.CommandArgument.ToString();
    // Add the product to the shopping cart
    ShoppingCartAccess.AddItem(productId);
  }
}