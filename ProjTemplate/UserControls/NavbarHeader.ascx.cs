﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NavbarHeader : System.Web.UI.UserControl
{
    private int ItemCount;

    // fill cart summary contents in the PreRender stage
    protected void Page_PreRender(object sender, EventArgs e)
    {
        PopulateControls();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // don't repopulate control on postback
        if (!IsPostBack)
        {
            DataBind();
            // tie the search text box to the Go button
            Utilities.TieButton(this.Page, searchTextBox, goButton);
            // load search box controls' values
            string searchString = Request.QueryString["Search"];
            if (searchString != null)
                searchTextBox.Value = searchString;
        }
    }

    // fill the controls with data
    private void PopulateControls()
    {
        // get the items in the shopping cart
        DataTable dt = ShoppingCartAccess.GetItems();
        // if the shopping cart is empty...
        if (dt.Rows.Count == 0)
        {
            shoppingCartCount.InnerText = "";
        }
        else if (dt.Rows.Count == 1)
        {
            shoppingCartCount.InnerText = "(1) Item";
        }
        else
        // if the shopping cart is not empty...
        {
            // populate the list with the shopping cart contents
            shoppingCartCount.InnerText = "(" + dt.Rows.Count + ") Items";
        }
    }

    public override void DataBind()
    {
        DataTable categoryData = CatalogAccess.GetBrandsForCategory();
        ViewState["mydata"] = categoryData;
        this.ItemCount = categoryData.Rows.Count;
        String [] parameters = {"CategoryID","Name"};
        list.DataSource = SelectDistinct(categoryData, parameters);
        list.DataBind();
    }

    protected string GetItemClass(int itemIndex)
    {
        if (itemIndex == 0)
            return " first";
        else if (itemIndex == this.ItemCount - 1)
            return " last";
        else
            return "";
    }

    // Perform the product search
    protected void goButton_Click(object sender, EventArgs e)
    {
        ExecuteSearch();
    }

    // Redirect to the search results page
    private void ExecuteSearch()
    {
        if (searchTextBox.Value.Trim() != "")
            Response.Redirect(Request.ApplicationPath +
                         "/Search.aspx?Search=" + searchTextBox.Value);
    }


    private DataTable SelectDistinct(DataTable SourceTable, params string[] FieldNames)
    {
        object[] lastValues;
        DataTable newTable;
        DataRow[] orderedRows;


        if (FieldNames == null || FieldNames.Length == 0)
            throw new ArgumentNullException("FieldNames");


        lastValues = new object[FieldNames.Length];
        newTable = new DataTable();


        foreach (string fieldName in FieldNames)
            newTable.Columns.Add(fieldName, SourceTable.Columns[fieldName].DataType);


        orderedRows = SourceTable.Select("", string.Join(", ", FieldNames));


        foreach (DataRow row in orderedRows)
        {
            if (!fieldValuesAreEqual(lastValues, row, FieldNames))
            {
                newTable.Rows.Add(createRowClone(row, newTable.NewRow(), FieldNames));


                setLastValues(lastValues, row, FieldNames);
            }
        }
        return newTable;
    }


    private static bool fieldValuesAreEqual(object[] lastValues, DataRow currentRow, string[] fieldNames)
    {
        bool areEqual = true;


        for (int i = 0; i < fieldNames.Length; i++)
        {
            if (lastValues[i] == null || !lastValues[i].Equals(currentRow[fieldNames[i]]))
            {
                areEqual = false;
                break;
            }
        }
        return areEqual;
    }


    private static DataRow createRowClone(DataRow sourceRow, DataRow newRow, string[] fieldNames)
    {
        foreach (string field in fieldNames)
            newRow[field] = sourceRow[field];
        return newRow;
    }


    private static void setLastValues(object[] lastValues, DataRow sourceRow, string[] fieldNames)
    {
        for (int i = 0; i < fieldNames.Length; i++)
            lastValues[i] = sourceRow[fieldNames[i]];
    }

    protected void list_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater brandList = (Repeater)e.Item.FindControl("brandList");
            Label lblCategoryId = (Label)e.Item.FindControl("lblCategoryId");
            DataTable brandData = (DataTable)ViewState["mydata"];
            DataView dvBrand = brandData.DefaultView;
            dvBrand.RowFilter = " CategoryID = '" + lblCategoryId.Text + "'";
            brandList.DataSource = dvBrand;
            brandList.DataBind();

        }
    }
}