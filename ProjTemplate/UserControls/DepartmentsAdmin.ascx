<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DepartmentsAdmin.ascx.cs" Inherits="DepartmentsAdmin" %>
<div class="row-fluid">
    <div class="alert alert-success">
        <asp:Label ID="statusLabel" runat="server" Text="Departments Loaded"></asp:Label>
    </div>

    <p>
        <asp:Label ID="locationLabel" runat="server" Text="These are your departments:"></asp:Label>
    </p>
</div>
<asp:GridView ID="grid" CssClass="table table-bordered table-hover table-striped" runat="server" AutoGenerateColumns="False" DataKeyNames="DepartmentID" border="0" OnRowCancelingEdit="grid_RowCancelingEdit" OnRowDeleting="grid_RowDeleting" OnRowEditing="grid_RowEditing" OnRowUpdating="grid_RowUpdating">
    <Columns>
        <asp:BoundField DataField="Name" HeaderText="Department Name" SortExpression="Name" />
        <asp:TemplateField HeaderText="Department Description" SortExpression="Description">
            <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="descriptionTextBox" runat="server" Text='<%# Bind("Description") %>' Height="70px" TextMode="MultiLine" Width="350px"></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:HyperLinkField DataNavigateUrlFields="DepartmentID" DataNavigateUrlFormatString="../CatalogAdmin.aspx?DepartmentID={0}"
            Text="View Categories" />
        <asp:CommandField ShowEditButton="True" />
        <asp:ButtonField CommandName="Delete" Text="Delete" />
    </Columns>
</asp:GridView>
<br />
<div class="row-fluid">
    <h4>Create a new department:</h4>
    <table cellspacing="0">
        <tr>
            <td valign="top" width="100">Name:
            </td>
            <td>
                <asp:TextBox CssClass="AdminPageText" ID="newName" runat="server"
                    Width="400px" />
            </td>
        </tr>
        <tr>
            <td valign="top" width="100">Description:
            </td>
            <td>
                <asp:TextBox CssClass="AdminPageText" ID="newDescription" runat="server" Width="400px" Height="70px" TextMode="MultiLine" />
            </td>
        </tr>
    </table>
    <asp:Button ID="createDepartment" Text="Create Department" runat="server"
        CssClass="btn btn-primary" OnClick="createDepartment_Click" />
</div>



