<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CategoriesList.ascx.cs" Inherits="CategoriesList" %>
<div class="nav-category-header">
    <p>Categories</p>
</div>
<div class="nav-category-body">
    <ul>
        <asp:Repeater ID="list" runat="server">
            <ItemTemplate>
                <li>&nbsp;&raquo;
    <asp:HyperLink
        ID="HyperLink1"
        runat="server"
        NavigateUrl='<%# "../Catalog.aspx?CategoryID=" + Eval("CategoryID")  %>'
        Text='<%# Eval("Name") %>'
        CssClass='<%# Eval("CategoryID").ToString() == Request.QueryString["CategoryID"] ? "CategorySelected" : "CategoryUnselected" %>'>>
    </asp:HyperLink>
                </li>

    
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</div>
