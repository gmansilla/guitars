using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CategoriesList : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // don't reload data during postbacks
        if (!IsPostBack)
        {
            string categoryId = Request.QueryString["CategoryID"];
            list.DataSource = CatalogAccess.GetBrandsForACategory(categoryId);
            // Needed to bind the data bound controls to the data source
            list.DataBind();
        }
    }
}
