<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductsAdmin.ascx.cs"
    Inherits="ProductsAdmin" %>

<div class="row-fluid">
    <div class="alert alert-success">
        <asp:Label ID="statusLabel" runat="server" Text="Products Loaded"></asp:Label>
    </div>
    <p>
        <asp:Label ID="locationLabel" runat="server" Text="Displaying products for category..."></asp:Label>
    </p>
    <p>
        <asp:LinkButton ID="goBackLink" runat="server" CssClass="btn btn-primary" OnClick="goBackLink_Click">(go back to categories)</asp:LinkButton>
    </p>
</div>
<asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" DataKeyNames="ProductID" CssClass="table table-hover table-striped table-bordered"
    Width="100%" OnRowCancelingEdit="grid_RowCancelingEdit" OnRowEditing="grid_RowEditing" OnRowUpdating="grid_RowUpdating">
    <Columns>
        <asp:ImageField DataImageUrlField="Image" DataImageUrlFormatString="../ProductImages/{0}"
            HeaderText="Product Image" ReadOnly="True" ControlStyle-CssClass="edit-product-img">
        </asp:ImageField>
        <asp:TemplateField HeaderText="Product Name" SortExpression="Name">
            <ItemTemplate>
                <asp:Label ID="Label5" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="nameTextBox" runat="server" Width="97%" CssClass="GridEditingRow" Text='<%# Bind("Name") %>'></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Product Description" SortExpression="Description">
            <ItemTemplate>
                <asp:Literal ID="Label1" runat="server" Text='<%# System.Web.HttpUtility.HtmlEncode((string)Eval("Description")) %>'></asp:Literal>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="descriptionTextBox" runat="server" Text='<%# Bind("Description") %>'
                    Height="100px" Width="97%" CssClass="GridEditingRow" TextMode="MultiLine" />
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Price" SortExpression="Price">
            <ItemTemplate>
                <asp:Label ID="Label2" runat="server" Text='<%# String.Format("{0:0.00}", Eval("Price")) %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="priceTextBox" runat="server" Width="45px" Text='<%# String.Format("{0:0.00}", Eval("Price")) %>'></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Image File" SortExpression="Image2FileName">
            <ItemTemplate>
                <asp:Label ID="Label4" runat="server" Text='<%# Bind("Image") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="image1TextBox" Width="80px" runat="server" Text='<%# Bind("Image") %>'></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Stock" SortExpression="Stock">
            <ItemTemplate>
                <asp:Label ID="Label4" runat="server" Text='<%# Bind("Stock") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="stockTextBox" Width="80px" runat="server" Text='<%# Bind("Stock") %>'></asp:TextBox>
            </EditItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField>
            <ItemTemplate>
                <asp:HyperLink runat="server" Text="Select" NavigateUrl='<%# "../CatalogAdmin.aspx?CategoryID=" + Request.QueryString["CategoryID"] + "&amp;ProductID=" + Eval("ProductID") %>' ID="HyperLink1">
                </asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:CommandField ShowEditButton="True" />
    </Columns>
</asp:GridView>
<h4 class="AdminPageText">Create a new product and assign it to this category:</h4>
<table class="AdminPageText" cellspacing="0">
    <tr>
        <td width="100" valign="top">Name:</td>
        <td>
            <asp:TextBox CssClass="AdminPageText" ID="newName" runat="server" Width="400px" />
        </td>
    </tr>
    <tr>
        <td width="100" valign="top">Description:</td>
        <td>
            <asp:TextBox CssClass="AdminPageText" ID="newDescription" runat="server" Width="400px" Height="70px" TextMode="MultiLine" />
        </td>
    </tr>
    <tr>
        <td width="100" valign="top">Price:</td>
        <td>
            <asp:TextBox CssClass="AdminPageText" ID="newPrice" runat="server" Width="400px">0.00</asp:TextBox>
        </td>
    </tr>
    <tr>
        <td width="100" valign="top">Image1 File:</td>
        <td>
            <asp:TextBox CssClass="AdminPageText" ID="newImage1FileName" runat="server" Width="400px">Generic1.png</asp:TextBox>
        </td>
    </tr>
    <tr>
        <td width="150" valign="top">Stock:</td>
        <td>
            <asp:TextBox CssClass="AdminPageText" ID="newStock" runat="server" Width="400px">00</asp:TextBox>
        </td>
    </tr>
    
</table>
<asp:Button ID="createProduct" CssClass="btn btn-primary"
    runat="server" Text="Create Product" OnClick="createProduct_Click" />
