﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductRating.ascx.cs"
    Inherits="WebUserControl" %>
<div id="reviewsDiv" class="row-fluid">
    <strong class="review-heading">Customer Reviews <span></span></strong>
    <asp:HiddenField ID="UserIdHidden" runat="server" />
    <asp:LoginView ID="LoginView1" runat="server">
        <RoleGroups>
            <asp:RoleGroup Roles="User">
                <ContentTemplate>
                    <div class="accordion" id="reviewInput">
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">Write a product review
                                </a>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <ul class="order-admin-list">
                                        <li class="clearfix">
                                            <asp:Label ID="RatingLbl" AssociatedControlID="RatingId" runat="server" Text="Rating:"></asp:Label>
                                            <input name="star1" type="radio" class="star1" value="1" />
                                            <input name="star1" type="radio" class="star1" value="2" />
                                            <input name="star1" type="radio" class="star1" value="3" />
                                            <input name="star1" type="radio" class="star1" value="4" />
                                            <input name="star1" type="radio" class="star1" value="5" />
                                            <asp:HiddenField id="RatingId" runat="server"></asp:HiddenField> 
                                            
                                        </li>
                                        <li class="clearfix">
                                            <asp:Label ID="CommentLbl" AssociatedControlID="CommentId" runat="server" Text="Comment:"></asp:Label>
                                            <asp:TextBox ID="CommentId" Wrap="true" TextMode="MultiLine" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RatingValidation" runat="server" ControlToValidate="CommentId"
                                                ErrorMessage="Please supply a comment." ValidationGroup="RatingValidationGoup"></asp:RequiredFieldValidator>
                                             
                                        </li>
                                        <li class="clearfix">
                                            <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" OnClick="Button1_Click" Text="Submit Review" ValidationGroup="RatingValidationGoup" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:RoleGroup>
        </RoleGroups>
    </asp:LoginView>
    <div class="review-summary-div">
        <p>Review Summary</p>

    </div>
    <div class="review-summary-info-div clearfix">
        <div id="reviewSummaryStars" class="review-summary-stars-big pull-left" runat="server"></div>
        <div id="reviewSummaryScore" class="review-summary-score-big pull-left" runat="server"></div>
        <div id="reviewSummaryCount" class="review-summary-count-big pull-left" runat="server"></div>
    </div>
    <div class="review-div clearfix">
        <asp:Repeater ID="Repeater1" runat="server">
            <ItemTemplate>
                <div class="review-cell clearfix">
                    <div class="span4">
                        <p>Posted by <b><%# Eval("UserName") %></b></p>
                        <p>at <%# Eval("created") %></p>
                    </div>
                    <div class="span8">
                        <div class="clearfix">
                            <div class="review-summary-stars-small pull-left star<%# Eval("rate") %>-small"></div>
                            <div class="review-summary-score-big pull-left"><%# Eval("rate", "{0:0.0}") %></div>
                        </div>
                        <p class="comments-header"><b>Comments about <i><%# Eval("name") %></i></b></p>
                        <p><%# Eval("comment") %></p>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
