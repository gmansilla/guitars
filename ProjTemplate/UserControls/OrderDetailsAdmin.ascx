<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OrderDetailsAdmin.ascx.cs" Inherits="OrderDetailsAdmin" %>
<h4>
    <asp:Label ID="orderIdLabel" runat="server" Text="Order #000" /></h4>

<ul class="order-admin-list">
    <li class="clearfix">
        <label for="totalAmountLabel">Total Amount:</label>
        <asp:Label ID="totalAmountLabel" CssClass="order-admin-price" runat="server"></asp:Label>
    </li>
    <li class="clearfix">
        <asp:Label runat="server" AssociatedControlID="dateCreatedTextBox">Date Created:</asp:Label>
        <asp:TextBox ID="dateCreatedTextBox" runat="server" />
    </li>
    <li class="clearfix">
        <asp:Label runat="server" AssociatedControlID="dateShippedTextBox">Date Shipped:</asp:Label>
        <asp:TextBox ID="dateShippedTextBox" runat="server" />
    </li>
    <li class="clearfix">
        <asp:Label runat="server" AssociatedControlID="verifiedCheck">Verified:</asp:Label>
        <asp:CheckBox ID="verifiedCheck" runat="server" />
    </li>
    <li class="clearfix">
        <asp:Label runat="server" AssociatedControlID="completedCheck">Completed:</asp:Label>
        <asp:CheckBox ID="completedCheck" runat="server" />
    </li>
    <li class="clearfix">
        <asp:Label runat="server" AssociatedControlID="canceledCheck">Canceled:</asp:Label>
        <asp:CheckBox ID="canceledCheck" runat="server" />
    </li>
    <li class="clearfix">
        <asp:Label runat="server" AssociatedControlID="commentsTextBox">Comments:</asp:Label>
        <asp:TextBox ID="commentsTextBox" runat="server" />
    </li>
    <li class="clearfix">
        <asp:Label runat="server" AssociatedControlID="customerNameTextBox">Customer Name:</asp:Label>
        <asp:TextBox ID="customerNameTextBox" runat="server" />
    </li>
    <li class="clearfix">
        <asp:Label runat="server" AssociatedControlID="shippingAddressTextBox">Shipping Address:</asp:Label>
        <asp:TextBox ID="shippingAddressTextBox" runat="server" />
    </li>
    <li class="clearfix">
        <asp:Label runat="server" AssociatedControlID="customerEmailTextBox">Customer Email:</asp:Label>
        <asp:TextBox ID="customerEmailTextBox" runat="server" />
    </li>

</ul>
<div class="row-fluid order-admin-buttons">
    <p>
        <asp:Button ID="editButton" runat="server" CssClass="btn"
            Text="Edit" Width="100px" OnClick="editButton_Click" />
        <asp:Button ID="updateButton" runat="server" CssClass="btn"
            Text="Update" Width="100px" OnClick="updateButton_Click" />
        <asp:Button ID="cancelButton" runat="server" CssClass="btn"
            Text="Cancel" Width="100px" OnClick="cancelButton_Click" />
    </p>
    <p><asp:Button ID="markVerifiedButton" runat="server" CssClass="btn"
        Text="Mark Order as Verified" Width="310px" OnClick="markVerifiedButton_Click" /></p>
    <p><asp:Button ID="markCompletedButton" runat="server" CssClass="btn"
        Text="Mark Order as Completed" Width="310px" OnClick="markCompletedButton_Click" /></p>
    <p><asp:Button ID="markCanceledButton" runat="server" CssClass="btn"
        Text="Mark Order as Canceled" Width="310px" OnClick="markCanceledButton_Click" /></p>
</div>

<h4>
    <asp:Label ID="Label13" runat="server" Text="The order contains these items:" />
</h4>
<asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-hover table-striped" Width="100%">
    <Columns>
        <asp:BoundField DataField="ProductID" HeaderText="Product ID"
            ReadOnly="True" SortExpression="ProductID" />
        <asp:BoundField DataField="ProductName" HeaderText="Product Name"
            ReadOnly="True" SortExpression="ProductName" />
        <asp:BoundField DataField="Quantity" HeaderText="Quantity"
            ReadOnly="True" SortExpression="Quantity" />
        <asp:BoundField DataField="UnitCost" HeaderText="Unit Cost"
            ReadOnly="True" SortExpression="UnitCost" DataFormatString="{0:C}" />
        <asp:BoundField DataField="Subtotal" HeaderText="Subtotal"
            ReadOnly="True" SortExpression="Subtotal" DataFormatString="{0:C}" />
    </Columns>
</asp:GridView>
