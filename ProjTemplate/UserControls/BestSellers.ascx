<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BestSellers.ascx.cs"
    Inherits="ProductsList" %>
<asp:Repeater ID="list" runat="server" OnItemCommand="list_ItemCommand">
    <ItemTemplate>
        <li class="span4<%# (Container.ItemIndex == 0 || Container.ItemIndex == 3) ? " first" : ""%>">
            <div class="thumbnail">
                <a class="thumbnail-link" href='Product.aspx?ProductID=<%# Eval("ProductID")%>'>
                    <img src='ProductImages/<%# Eval("Image") %>' border="0" style="height: auto; width:auto; max-height:100%; max-width:100%;" alt="<%# Eval("Name") %>"></a>
                <div class="caption">
                    <h3>
                        <a class="ProductName" href='Product.aspx?ProductID=<%# Eval("ProductID")%>'>
                        <%# Eval("Name") %></a>
                    </h3>
                    <p class="bestsellers-price"><%# Eval("Price", "{0:c}") %></p>
                    <p><asp:Button ID="Button1" runat="server" Text="Add to Cart" CommandArgument='<%# Eval("ProductID") %>' CssClass="btn btn-primary" /></p>                
                </div>
            </div>
        </li>
        <%# (Container.ItemIndex == 2) ? "</ul><ul class=\"thumbnails row-second\">" : ""%>
    </ItemTemplate>
</asp:Repeater>
