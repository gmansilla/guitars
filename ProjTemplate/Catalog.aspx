<%@ Page Language="C#" MasterPageFile="~/GuitarStore.master"
    AutoEventWireup="true" CodeFile="Catalog.aspx.cs" Inherits="Catalog"
    Title="GuitarShop - The Product Catalog" %>

<%@ Register Src="UserControls/ProductsList.ascx" TagName="ProductsList" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/CategoriesList.ascx" TagPrefix="uc1" TagName="CategoriesList" %>
<%@ Register Src="~/UserControls/BrandList.ascx" TagPrefix="uc1" TagName="BrandList" %>


<asp:Content ID="content" ContentPlaceHolderID="contentPlaceHolder" runat="Server">
    <div class="container-fluid container-fluid-no-padding">
        <div class="row-fluid">
            <div class="span3">
                <!--Sidebar content-->
                <uc1:CategoriesList runat="server" id="CategoriesList" />
                <uc1:BrandList runat="server" ID="BrandList" />
            </div>
            <div class="span9">
                <!--Body content-->
                <uc1:ProductsList ID="ProductsList1" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
