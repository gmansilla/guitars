<%@ Page Language="C#" MasterPageFile="~/GuitarStore.master" AutoEventWireup="true" CodeFile="Oooops.aspx.cs" Inherits="Oooops" Title="BalloonShop - Oooops!" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolder" runat="Server">
    <div class="alert alert-error">
        <p>
            <b>Your request generated an internal error!</b>
        </p>
        <p>
            We apologize for the inconvenience! The error has been reported.
        </p>
    </div>
</asp:Content>

