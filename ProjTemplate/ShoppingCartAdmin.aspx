<%@ Page Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="ShoppingCartAdmin.aspx.cs" Inherits="ShoppingCartAdmin" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h4>Shopping Cart Admin</h4>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="alert alert-success">
        <asp:Label ID="countLabel" runat="server">
      Hello!
        </asp:Label>
    </div>
    <span>How many days?</span>
    <asp:DropDownList ID="daysList" runat="server">
        <asp:ListItem Value="0">All shopping carts</asp:ListItem>
        <asp:ListItem Value="1">One</asp:ListItem>
        <asp:ListItem Value="10" Selected="True">Ten</asp:ListItem>
        <asp:ListItem Value="20">Twenty</asp:ListItem>
        <asp:ListItem Value="30">Thirty</asp:ListItem>
        <asp:ListItem Value="90">Ninety</asp:ListItem>
    </asp:DropDownList><br />
    <br />
    <asp:Button ID="countButton" runat="server" Text="Count Old Shopping Carts" CssClass="btn btn-primary" OnClick="countButton_Click" />
    <asp:Button ID="deleteButton" runat="server" Text="Delete Old Shopping Carts" CssClass="btn" OnClick="deleteButton_Click" />
</asp:Content>

