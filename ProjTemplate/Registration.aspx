﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Registration.aspx.cs" MasterPageFile="~/GuitarStore.master" Inherits="Registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolder" runat="Server">

    <div class="row-fluid">
        <div class="section-heading rounded shopping-cart-header">
            <h2>Please Register</h2>
        </div>
        <div class="shopping-cart-total row-alternate text-center">

            <ul class="login-list">
                <li class="clearfix control-group">
                    <asp:Label ID="fName" runat="server" Text="First Name:"></asp:Label>
                    <asp:TextBox ID="fName_Input" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="fName_Val" runat="server"
                        ControlToValidate="fName_Input" ErrorMessage="First name is required" Display="Dynamic"></asp:RequiredFieldValidator>
                </li>
                <li class="clearfix control-group">
                    <asp:Label ID="lName" runat="server" Text="Last Name:"></asp:Label>
                    <asp:TextBox ID="lName_Input" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="lName_Val" runat="server"
                        ControlToValidate="lName_Input" ErrorMessage="Last name is required" Display="Dynamic"></asp:RequiredFieldValidator>
                </li>
                <li class="clearfix control-group">
                    <asp:Label ID="email" runat="server" Text="Email:"></asp:Label>
                    <asp:TextBox ID="email_Input" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="email_Req_Val" runat="server"
                        ControlToValidate="email_Input" Display="Dynamic"
                        ErrorMessage="Email is required"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="email_Valid_Val" runat="server"
                        ControlToValidate="email_Input" ErrorMessage="Email not valid"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"></asp:RegularExpressionValidator>
                </li>
                <li class="clearfix control-group">
                    <asp:Label ID="address" runat="server" Text="Address:"></asp:Label>
                    <asp:TextBox ID="address_Input" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="address_Val" runat="server"
                        ControlToValidate="address_Input" ErrorMessage="Address is required" Display="Dynamic"></asp:RequiredFieldValidator>
                </li>
                <li class="clearfix control-group">
                    <asp:Label ID="password" runat="server" Text="Password:"></asp:Label>
                    <asp:TextBox ID="password_Input" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="password_Req_Val" runat="server"
                        ControlToValidate="password_Input" Display="Dynamic"
                        ErrorMessage="Password is required"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="password_RegEx_Val" runat="server"
                        ControlToValidate="password_Input"
                        ErrorMessage="Password must be at least 6 characters long and contain one number"
                        ValidationExpression="^(?=.*\d).{6,}$" Display="Dynamic"></asp:RegularExpressionValidator>
                </li>
                <li class="clearfix control-group">
                    <asp:Label ID="confirmPassword" runat="server" Text="Confirm Password:"></asp:Label>
                    <asp:TextBox ID="confirmPassword_Input" runat="server"  TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="cPassword_Req_Val" runat="server"
                        ControlToValidate="confirmPassword_Input" Display="Dynamic"
                        ErrorMessage="Password is required"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cPassword_Compare_Val" runat="server"
                        ControlToCompare="password_Input" ControlToValidate="confirmPassword_Input"
                        ErrorMessage="Passwords do not match"></asp:CompareValidator>
                </li>
                <li class="clearfix form-btn-group">
                    <asp:Button ID="register_Btn" runat="server" Text="Register" CssClass="btn btn-primary" />
                    <asp:Button ID="Button1" runat="server" Text="Clear" CssClass="btn" />
                </li>
            </ul>
        </div>
    </div>
</asp:Content>
