$(document).ready(function () {
    // Handler for .ready() called.
    $('#myCarousel').carousel({
        interval: 10000,
        pause: "hover"
    });

    var inputRating = $('input.star1');

    if (inputRating.length) {
        inputRating.rating(
            {
                callback: function (a, b) {
                    $('input[id*="RatingId"]').val(a);
                }
            });
    }
});