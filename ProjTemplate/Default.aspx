<%@ Page Language="C#" MasterPageFile="~/GuitarStore.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" Title="Welcome to BalloonShop!" %>

<%@ Register Src="UserControls/ProductsList.ascx" TagName="ProductsList" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/BestSellers.ascx" TagPrefix="uc1" TagName="BestSellers" %>

<asp:Content ID="content" ContentPlaceHolderID="contentPlaceHolder" runat="server">
    <!-- Main hero unit for a primary marketing message or call to action -->
    <div class="hero-unit">
        <div id="myCarousel" class="carousel slide">

            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class=""></li>
                <li class="active" data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="item">
                    <a id="A1" href="~/Product.aspx?ProductID=9" runat="server">
                        <img src="img/promo01.jpg" alt="">
                        <div class="carousel-caption">
                            <h4>Brand NEW</h4>
                            <p>Gibson 2012 Les Paul Standard Premium AAA Electric Guitar. All-New design.</p>
                        </div>
                    </a>
                </div>
                <div class="item active">
                    <a href="~/Product.aspx?ProductID=16" runat="server">
                        <img src="img/promo02.jpg" alt="">
                        <div class="carousel-caption">
                            <h4>Ibanez JEM/UV Steve Vai Signature Electric Guitar</h4>
                            <p>An incredible combination of specifications designed by Steve Vai to break boundaries.</p>
                        </div>
                    </a>
                </div>
                <div class="item">
                    <img src="img/promo03.jpg" alt="">
                    <div class="carousel-caption">
                        <h4>Canada's most UNIQUE guitar selection</h4>
                        <p>We have a unique and continually expanding selection of acoustic and electric guitars, basses, amps and accessories.</p>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">�</a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">�</a>

        </div>
    </div>

    <!-- Example row of columns -->
    <div class="section-heading rounded">
        <h2>Our Bestselling Products</h2>
    </div>
    <div class="row-fluid row-alternate">
        <ul class="thumbnails">
            <uc1:BestSellers runat="server" ID="BestSellers" />
        </ul>
    </div>
</asp:Content>

