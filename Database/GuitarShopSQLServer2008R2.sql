USE [master]
GO
/****** Object:  Database [GuitarShop]    Script Date: 04/14/2013 12:55:27 ******/
CREATE DATABASE [GuitarShop] ON  PRIMARY 
( NAME = N'GuitarShop', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\GuitarShop.mdf' , SIZE = 3328KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'GuitarShop_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\GuitarShop_log.LDF' , SIZE = 768KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [GuitarShop] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GuitarShop].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [GuitarShop] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [GuitarShop] SET ANSI_NULLS OFF
GO
ALTER DATABASE [GuitarShop] SET ANSI_PADDING OFF
GO
ALTER DATABASE [GuitarShop] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [GuitarShop] SET ARITHABORT OFF
GO
ALTER DATABASE [GuitarShop] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [GuitarShop] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [GuitarShop] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [GuitarShop] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [GuitarShop] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [GuitarShop] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [GuitarShop] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [GuitarShop] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [GuitarShop] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [GuitarShop] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [GuitarShop] SET  ENABLE_BROKER
GO
ALTER DATABASE [GuitarShop] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [GuitarShop] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [GuitarShop] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [GuitarShop] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [GuitarShop] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [GuitarShop] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [GuitarShop] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [GuitarShop] SET  READ_WRITE
GO
ALTER DATABASE [GuitarShop] SET RECOVERY FULL
GO
ALTER DATABASE [GuitarShop] SET  MULTI_USER
GO
ALTER DATABASE [GuitarShop] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [GuitarShop] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'GuitarShop', N'ON'
GO
USE [GuitarShop]
GO
/****** Object:  Role [aspnet_Membership_BasicAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE ROLE [aspnet_Membership_BasicAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Membership_FullAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE ROLE [aspnet_Membership_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Membership_ReportingAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE ROLE [aspnet_Membership_ReportingAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Personalization_BasicAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE ROLE [aspnet_Personalization_BasicAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Personalization_FullAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE ROLE [aspnet_Personalization_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Personalization_ReportingAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE ROLE [aspnet_Personalization_ReportingAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Profile_BasicAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE ROLE [aspnet_Profile_BasicAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Profile_FullAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE ROLE [aspnet_Profile_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Profile_ReportingAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE ROLE [aspnet_Profile_ReportingAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Roles_BasicAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE ROLE [aspnet_Roles_BasicAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Roles_FullAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE ROLE [aspnet_Roles_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Roles_ReportingAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE ROLE [aspnet_Roles_ReportingAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_WebEvent_FullAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE ROLE [aspnet_WebEvent_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Schema [aspnet_WebEvent_FullAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE SCHEMA [aspnet_WebEvent_FullAccess] AUTHORIZATION [aspnet_WebEvent_FullAccess]
GO
/****** Object:  Schema [aspnet_Roles_ReportingAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE SCHEMA [aspnet_Roles_ReportingAccess] AUTHORIZATION [aspnet_Roles_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Roles_FullAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE SCHEMA [aspnet_Roles_FullAccess] AUTHORIZATION [aspnet_Roles_FullAccess]
GO
/****** Object:  Schema [aspnet_Roles_BasicAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE SCHEMA [aspnet_Roles_BasicAccess] AUTHORIZATION [aspnet_Roles_BasicAccess]
GO
/****** Object:  Schema [aspnet_Profile_ReportingAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE SCHEMA [aspnet_Profile_ReportingAccess] AUTHORIZATION [aspnet_Profile_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Profile_FullAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE SCHEMA [aspnet_Profile_FullAccess] AUTHORIZATION [aspnet_Profile_FullAccess]
GO
/****** Object:  Schema [aspnet_Profile_BasicAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE SCHEMA [aspnet_Profile_BasicAccess] AUTHORIZATION [aspnet_Profile_BasicAccess]
GO
/****** Object:  Schema [aspnet_Personalization_ReportingAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE SCHEMA [aspnet_Personalization_ReportingAccess] AUTHORIZATION [aspnet_Personalization_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Personalization_FullAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE SCHEMA [aspnet_Personalization_FullAccess] AUTHORIZATION [aspnet_Personalization_FullAccess]
GO
/****** Object:  Schema [aspnet_Personalization_BasicAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE SCHEMA [aspnet_Personalization_BasicAccess] AUTHORIZATION [aspnet_Personalization_BasicAccess]
GO
/****** Object:  Schema [aspnet_Membership_ReportingAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE SCHEMA [aspnet_Membership_ReportingAccess] AUTHORIZATION [aspnet_Membership_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Membership_FullAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE SCHEMA [aspnet_Membership_FullAccess] AUTHORIZATION [aspnet_Membership_FullAccess]
GO
/****** Object:  Schema [aspnet_Membership_BasicAccess]    Script Date: 04/14/2013 12:55:27 ******/
CREATE SCHEMA [aspnet_Membership_BasicAccess] AUTHORIZATION [aspnet_Membership_BasicAccess]
GO
/****** Object:  FullTextCatalog [BalloonShopFullText]    Script Date: 04/14/2013 12:55:27 ******/
CREATE FULLTEXT CATALOG [BalloonShopFullText]WITH ACCENT_SENSITIVITY = ON
AS DEFAULT
AUTHORIZATION [dbo]
GO
/****** Object:  Table [dbo].[aspnet_Applications]    Script Date: 04/14/2013 12:55:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Applications](
	[ApplicationName] [nvarchar](256) NOT NULL,
	[LoweredApplicationName] [nvarchar](256) NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [aspnet_Applications_Index] ON [dbo].[aspnet_Applications] 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[aspnet_Applications] ([ApplicationName], [LoweredApplicationName], [ApplicationId], [Description]) VALUES (N'/', N'/', N'a1fcdf22-40f5-4c33-a685-a2161508df75', NULL)
INSERT [dbo].[aspnet_Applications] ([ApplicationName], [LoweredApplicationName], [ApplicationId], [Description]) VALUES (N'MyAppName', N'myappname', N'0ee00cdd-475a-4c89-8b88-191118d6019e', NULL)
/****** Object:  Table [dbo].[aspnet_WebEvent_Events]    Script Date: 04/14/2013 12:55:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aspnet_WebEvent_Events](
	[EventId] [char](32) NOT NULL,
	[EventTimeUtc] [datetime] NOT NULL,
	[EventTime] [datetime] NOT NULL,
	[EventType] [nvarchar](256) NOT NULL,
	[EventSequence] [decimal](19, 0) NOT NULL,
	[EventOccurrence] [decimal](19, 0) NOT NULL,
	[EventCode] [int] NOT NULL,
	[EventDetailCode] [int] NOT NULL,
	[Message] [nvarchar](1024) NULL,
	[ApplicationPath] [nvarchar](256) NULL,
	[ApplicationVirtualPath] [nvarchar](256) NULL,
	[MachineName] [nvarchar](256) NOT NULL,
	[RequestUrl] [nvarchar](1024) NULL,
	[ExceptionType] [nvarchar](256) NULL,
	[Details] [ntext] NULL,
PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RestorePermissions]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Setup_RestorePermissions]
    @name   sysname
AS
BEGIN
    DECLARE @object sysname
    DECLARE @protectType char(10)
    DECLARE @action varchar(60)
    DECLARE @grantee sysname
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT Object, ProtectType, [Action], Grantee FROM #aspnet_Permissions where Object = @name

    OPEN c1

    FETCH c1 INTO @object, @protectType, @action, @grantee
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = @protectType + ' ' + @action + ' on ' + @object + ' TO [' + @grantee + ']'
        EXEC (@cmd)
        FETCH c1 INTO @object, @protectType, @action, @grantee
    END

    CLOSE c1
    DEALLOCATE c1
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RemoveAllRoleMembers]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Setup_RemoveAllRoleMembers]
    @name   sysname
AS
BEGIN
    CREATE TABLE #aspnet_RoleMembers
    (
        Group_name      sysname,
        Group_id        smallint,
        Users_in_group  sysname,
        User_id         smallint
    )

    INSERT INTO #aspnet_RoleMembers
    EXEC sp_helpuser @name

    DECLARE @user_id smallint
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT User_id FROM #aspnet_RoleMembers

    OPEN c1

    FETCH c1 INTO @user_id
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = 'EXEC sp_droprolemember ' + '''' + @name + ''', ''' + USER_NAME(@user_id) + ''''
        EXEC (@cmd)
        FETCH c1 INTO @user_id
    END

    CLOSE c1
    DEALLOCATE c1
END
GO
/****** Object:  Table [dbo].[aspnet_SchemaVersions]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_SchemaVersions](
	[Feature] [nvarchar](128) NOT NULL,
	[CompatibleSchemaVersion] [nvarchar](128) NOT NULL,
	[IsCurrentVersion] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Feature] ASC,
	[CompatibleSchemaVersion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'common', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'health monitoring', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'membership', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'personalization', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'profile', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'role manager', N'1', 1)
/****** Object:  Table [dbo].[Attribute]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attribute](
	[AttributeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AttributeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Attribute] ON
INSERT [dbo].[Attribute] ([AttributeID], [Name]) VALUES (1, N'Color')
SET IDENTITY_INSERT [dbo].[Attribute] OFF
/****** Object:  Table [dbo].[Brand]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Brand](
	[BrandID] [int] NOT NULL,
	[BName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NULL,
 CONSTRAINT [PK_Brands] PRIMARY KEY CLUSTERED 
(
	[BrandID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Brand] ([BrandID], [BName], [Description]) VALUES (1, N'Ephiphone', N'')
INSERT [dbo].[Brand] ([BrandID], [BName], [Description]) VALUES (2, N'Fender', N'')
INSERT [dbo].[Brand] ([BrandID], [BName], [Description]) VALUES (3, N'Gibson', N'')
INSERT [dbo].[Brand] ([BrandID], [BName], [Description]) VALUES (4, N'Ibanez', N'')
INSERT [dbo].[Brand] ([BrandID], [BName], [Description]) VALUES (5, N'Elixer', N'')
/****** Object:  Table [dbo].[Orders]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[DateShipped] [smalldatetime] NULL,
	[Verified] [bit] NOT NULL,
	[Completed] [bit] NOT NULL,
	[Canceled] [bit] NOT NULL,
	[Comments] [nvarchar](1000) NULL,
	[CustomerName] [nvarchar](50) NULL,
	[CustomerEmail] [nvarchar](50) NULL,
	[ShippingAddress] [nvarchar](500) NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Orders] ON
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (1, CAST(0x9E3903F0 AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (2, CAST(0x9E3A01A9 AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (3, CAST(0x9E3A0206 AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (4, CAST(0x9E3A02CF AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (5, CAST(0x9E3E01B3 AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (6, CAST(0x9E3E01B5 AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (7, CAST(0x9E3E01B9 AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (8, CAST(0x9E3E02A6 AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (9, CAST(0x9E3E02A8 AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (10, CAST(0x9E3E02BB AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (11, CAST(0x9EAA03D5 AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (12, CAST(0x9EAA03D7 AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (13, CAST(0xA01503E6 AS SmallDateTime), CAST(0x9EE40202 AS SmallDateTime), 0, 0, 1, N'', N'', N'', N'')
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (14, CAST(0xA01D02F7 AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (15, CAST(0xA01D02F7 AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (16, CAST(0xA01D02F8 AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (17, CAST(0xA01D02FB AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (18, CAST(0xA19A03B1 AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [DateCreated], [DateShipped], [Verified], [Completed], [Canceled], [Comments], [CustomerName], [CustomerEmail], [ShippingAddress]) VALUES (19, CAST(0xA19B033E AS SmallDateTime), NULL, 0, 0, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Orders] OFF
/****** Object:  Table [dbo].[Department]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Department](
	[DepartmentID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](1000) NULL,
 CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED 
(
	[DepartmentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Department] ON
INSERT [dbo].[Department] ([DepartmentID], [Name], [Description]) VALUES (1, N'Anniversary Balloons', N'These sweet balloons are the perfect gift for someone you love.')
INSERT [dbo].[Department] ([DepartmentID], [Name], [Description]) VALUES (2, N'Balloons for Children', N'The colorful and funny balloons will make any child smile!')
SET IDENTITY_INSERT [dbo].[Department] OFF
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[OrderID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[ProductName] [nvarchar](50) NOT NULL,
	[Quantity] [int] NOT NULL,
	[UnitCost] [money] NOT NULL,
	[Subtotal]  AS ([Quantity]*[UnitCost]),
 CONSTRAINT [PK_OrderDetail] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC,
	[ProductID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (1, 4, N'Today, Tomorrow & Forever', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (2, 1, N'I Love You (Simon Elvin)', 2, 12.4900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (2, 5, N'Smiley Heart Red Balloon', 5, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (3, 1, N'I Love You (Simon Elvin)', 2, 12.4900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (3, 24, N'Birthday Star Balloon', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (4, 1, N'I Love You (Simon Elvin)', 1, 12.4900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (4, 2, N'Elvis Hunka Burning Love', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (4, 4, N'Today, Tomorrow & Forever', 2, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (4, 5, N'Smiley Heart Red Balloon', 2, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (5, 1, N'I Love You (Simon Elvin)', 1, 12.4900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (5, 4, N'Today, Tomorrow & Forever', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (6, 1, N'I Love You (Simon Elvin)', 1, 12.4900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (6, 24, N'Birthday Star Balloon', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (7, 2, N'Elvis Hunka Burning Love', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (7, 25, N'Tweety Stars', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (7, 40, N'Rugrats Tommy & Chucky', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (8, 14, N'Love Rose', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (8, 22, N'I''m Younger Than You', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (9, 4, N'Today, Tomorrow & Forever', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (10, 1, N'I Love You (Simon Elvin)', 1, 12.4900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (10, 4, N'Today, Tomorrow & Forever', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (10, 10, N'I Can''t Get Enough of You Baby', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (11, 1, N'I Love You (Simon Elvin)', 1, 12.4900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (12, 3, N'Funny Love', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (12, 57, N'Crystal Rose Silver', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (12, 58, N'Crystal Rose Gold', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (13, 1, N'I Love You (Simon Elvin)', 1, 12.4900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (13, 23, N'Birthday Balloon', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (14, 2, N'Elvis Hunka Burning Love', 2, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (14, 5, N'Smiley Heart Red Balloon', 31, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (14, 63, N'Joanne''s birthday balloon', 1, 23.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (17, 1, N'I Love You (Simon Elvin)', 1, 12.4900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (17, 7, N'Smiley Kiss Red Balloon', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (17, 10, N'I Can''t Get Enough of You Baby', 1, 12.9900)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [ProductName], [Quantity], [UnitCost]) VALUES (19, 1, N'Epiphone Elitist 1965 Casino', 1, 1799.0000)
/****** Object:  StoredProcedure [dbo].[OrderMarkVerified]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderMarkVerified]
(@OrderID INT)
AS
UPDATE Orders
SET Verified = 1
WHERE OrderID = @OrderID
GO
/****** Object:  StoredProcedure [dbo].[OrderMarkCompleted]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderMarkCompleted]
(@OrderID INT)
AS
UPDATE Orders
SET Completed = 1,
    DateShipped = GETDATE()
WHERE OrderID = @OrderID
GO
/****** Object:  StoredProcedure [dbo].[OrderMarkCanceled]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderMarkCanceled]
(@OrderID INT)
AS
UPDATE Orders
SET Canceled = 1
WHERE OrderID = @OrderID
GO
/****** Object:  StoredProcedure [dbo].[CatalogDeleteDepartment]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogDeleteDepartment]
(@DepartmentID int)
AS
DELETE FROM Department
WHERE DepartmentID = @DepartmentID
GO
/****** Object:  StoredProcedure [dbo].[CatalogGetDepartments]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogGetDepartments] AS
SELECT DepartmentID, Name, Description
FROM Department
GO
/****** Object:  StoredProcedure [dbo].[CatalogGetDepartmentDetails]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogGetDepartmentDetails]
(@DepartmentID INT)
AS
SELECT Name, Description
FROM Department
WHERE DepartmentID = @DepartmentID
GO
/****** Object:  Table [dbo].[Category]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentID] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](1000) NULL,
 CONSTRAINT [PK_Category_1] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Category] ON
INSERT [dbo].[Category] ([CategoryID], [DepartmentID], [Name], [Description]) VALUES (1, NULL, N'Electric', NULL)
INSERT [dbo].[Category] ([CategoryID], [DepartmentID], [Name], [Description]) VALUES (2, NULL, N'Acoustic', NULL)
INSERT [dbo].[Category] ([CategoryID], [DepartmentID], [Name], [Description]) VALUES (3, NULL, N'Bass', NULL)
INSERT [dbo].[Category] ([CategoryID], [DepartmentID], [Name], [Description]) VALUES (4, NULL, N'Strings', NULL)
SET IDENTITY_INSERT [dbo].[Category] OFF
/****** Object:  Table [dbo].[AttributeValue]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AttributeValue](
	[AttributeValueID] [int] IDENTITY(1,1) NOT NULL,
	[AttributeID] [int] NOT NULL,
	[Value] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AttributeValueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AttributeValue] ON
INSERT [dbo].[AttributeValue] ([AttributeValueID], [AttributeID], [Value]) VALUES (1, 1, N'White')
INSERT [dbo].[AttributeValue] ([AttributeValueID], [AttributeID], [Value]) VALUES (2, 1, N'Black')
INSERT [dbo].[AttributeValue] ([AttributeValueID], [AttributeID], [Value]) VALUES (3, 1, N'Red')
INSERT [dbo].[AttributeValue] ([AttributeValueID], [AttributeID], [Value]) VALUES (4, 1, N'Orange')
INSERT [dbo].[AttributeValue] ([AttributeValueID], [AttributeID], [Value]) VALUES (5, 1, N'Yellow')
INSERT [dbo].[AttributeValue] ([AttributeValueID], [AttributeID], [Value]) VALUES (6, 1, N'Green')
INSERT [dbo].[AttributeValue] ([AttributeValueID], [AttributeID], [Value]) VALUES (7, 1, N'Blue')
INSERT [dbo].[AttributeValue] ([AttributeValueID], [AttributeID], [Value]) VALUES (8, 1, N'Indigo')
INSERT [dbo].[AttributeValue] ([AttributeValueID], [AttributeID], [Value]) VALUES (9, 1, N'Purple')
SET IDENTITY_INSERT [dbo].[AttributeValue] OFF
/****** Object:  StoredProcedure [dbo].[aspnet_WebEvent_LogEvent]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_WebEvent_LogEvent]
        @EventId         char(32),
        @EventTimeUtc    datetime,
        @EventTime       datetime,
        @EventType       nvarchar(256),
        @EventSequence   decimal(19,0),
        @EventOccurrence decimal(19,0),
        @EventCode       int,
        @EventDetailCode int,
        @Message         nvarchar(1024),
        @ApplicationPath nvarchar(256),
        @ApplicationVirtualPath nvarchar(256),
        @MachineName    nvarchar(256),
        @RequestUrl      nvarchar(1024),
        @ExceptionType   nvarchar(256),
        @Details         ntext
AS
BEGIN
    INSERT
        dbo.aspnet_WebEvent_Events
        (
            EventId,
            EventTimeUtc,
            EventTime,
            EventType,
            EventSequence,
            EventOccurrence,
            EventCode,
            EventDetailCode,
            Message,
            ApplicationPath,
            ApplicationVirtualPath,
            MachineName,
            RequestUrl,
            ExceptionType,
            Details
        )
    VALUES
    (
        @EventId,
        @EventTimeUtc,
        @EventTime,
        @EventType,
        @EventSequence,
        @EventOccurrence,
        @EventCode,
        @EventDetailCode,
        @Message,
        @ApplicationPath,
        @ApplicationVirtualPath,
        @MachineName,
        @RequestUrl,
        @ExceptionType,
        @Details
    )
END
GO
/****** Object:  Table [dbo].[aspnet_Users]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Users](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[LoweredUserName] [nvarchar](256) NOT NULL,
	[MobileAlias] [nvarchar](16) NULL,
	[IsAnonymous] [bit] NOT NULL,
	[LastActivityDate] [datetime] NOT NULL,
PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Users_Index] ON [dbo].[aspnet_Users] 
(
	[ApplicationId] ASC,
	[LoweredUserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [aspnet_Users_Index2] ON [dbo].[aspnet_Users] 
(
	[ApplicationId] ASC,
	[LastActivityDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'0ee00cdd-475a-4c89-8b88-191118d6019e', N'57df95b6-b3b2-4605-b2da-d48297f4b585', N'admin', N'admin', NULL, 0, CAST(0x0000A1A000FAAFCC AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'0ee00cdd-475a-4c89-8b88-191118d6019e', N'ab959136-cc0d-4d63-b921-1092677f7f64', N'normal', N'normal', NULL, 0, CAST(0x0000A1A000F7FEB1 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'a1fcdf22-40f5-4c33-a685-a2161508df75', N'bc707ac4-e84f-400f-9649-7164ecb7b31e', N'admin', N'admin', NULL, 0, CAST(0x0000A1960141B665 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'a1fcdf22-40f5-4c33-a685-a2161508df75', N'e4985154-3fa1-40ce-8140-3581b21ddf2a', N'normal', N'normal', NULL, 0, CAST(0x0000A19D00187DD4 AS DateTime))
/****** Object:  StoredProcedure [dbo].[aspnet_UnRegisterSchemaVersion]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UnRegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    DELETE FROM dbo.aspnet_SchemaVersions
        WHERE   Feature = LOWER(@Feature) AND @CompatibleSchemaVersion = CompatibleSchemaVersion
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_CheckSchemaVersion]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_CheckSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    IF (EXISTS( SELECT  *
                FROM    dbo.aspnet_SchemaVersions
                WHERE   Feature = LOWER( @Feature ) AND
                        CompatibleSchemaVersion = @CompatibleSchemaVersion ))
        RETURN 0

    RETURN 1
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Applications_CreateApplication]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Applications_CreateApplication]
    @ApplicationName      nvarchar(256),
    @ApplicationId        uniqueidentifier OUTPUT
AS
BEGIN
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName

    IF(@ApplicationId IS NULL)
    BEGIN
        DECLARE @TranStarted   bit
        SET @TranStarted = 0

        IF( @@TRANCOUNT = 0 )
        BEGIN
	        BEGIN TRANSACTION
	        SET @TranStarted = 1
        END
        ELSE
    	    SET @TranStarted = 0

        SELECT  @ApplicationId = ApplicationId
        FROM dbo.aspnet_Applications WITH (UPDLOCK, HOLDLOCK)
        WHERE LOWER(@ApplicationName) = LoweredApplicationName

        IF(@ApplicationId IS NULL)
        BEGIN
            SELECT  @ApplicationId = NEWID()
            INSERT  dbo.aspnet_Applications (ApplicationId, ApplicationName, LoweredApplicationName)
            VALUES  (@ApplicationId, @ApplicationName, LOWER(@ApplicationName))
        END


        IF( @TranStarted = 1 )
        BEGIN
            IF(@@ERROR = 0)
            BEGIN
	        SET @TranStarted = 0
	        COMMIT TRANSACTION
            END
            ELSE
            BEGIN
                SET @TranStarted = 0
                ROLLBACK TRANSACTION
            END
        END
    END
END
GO
/****** Object:  Table [dbo].[aspnet_Paths]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Paths](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[PathId] [uniqueidentifier] NOT NULL,
	[Path] [nvarchar](256) NOT NULL,
	[LoweredPath] [nvarchar](256) NOT NULL,
PRIMARY KEY NONCLUSTERED 
(
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Paths_index] ON [dbo].[aspnet_Paths] 
(
	[ApplicationId] ASC,
	[LoweredPath] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Personalization_GetApplicationId]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Personalization_GetApplicationId] (
    @ApplicationName NVARCHAR(256),
    @ApplicationId UNIQUEIDENTIFIER OUT)
AS
BEGIN
    SELECT @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
END
GO
/****** Object:  Table [dbo].[aspnet_Roles]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Roles](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
	[LoweredRoleName] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Roles_index1] ON [dbo].[aspnet_Roles] 
(
	[ApplicationId] ASC,
	[LoweredRoleName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[aspnet_Roles] ([ApplicationId], [RoleId], [RoleName], [LoweredRoleName], [Description]) VALUES (N'a1fcdf22-40f5-4c33-a685-a2161508df75', N'5055d286-50b4-4e69-9358-b913f45865e9', N'Administrators', N'administrators', NULL)
INSERT [dbo].[aspnet_Roles] ([ApplicationId], [RoleId], [RoleName], [LoweredRoleName], [Description]) VALUES (N'a1fcdf22-40f5-4c33-a685-a2161508df75', N'9bdb4136-0b58-4055-bcb0-3ff983b736b2', N'User', N'user', NULL)
/****** Object:  StoredProcedure [dbo].[aspnet_RegisterSchemaVersion]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_RegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128),
    @IsCurrentVersion          bit,
    @RemoveIncompatibleSchema  bit
AS
BEGIN
    IF( @RemoveIncompatibleSchema = 1 )
    BEGIN
        DELETE FROM dbo.aspnet_SchemaVersions WHERE Feature = LOWER( @Feature )
    END
    ELSE
    BEGIN
        IF( @IsCurrentVersion = 1 )
        BEGIN
            UPDATE dbo.aspnet_SchemaVersions
            SET IsCurrentVersion = 0
            WHERE Feature = LOWER( @Feature )
        END
    END

    INSERT  dbo.aspnet_SchemaVersions( Feature, CompatibleSchemaVersion, IsCurrentVersion )
    VALUES( LOWER( @Feature ), @CompatibleSchemaVersion, @IsCurrentVersion )
END
GO
/****** Object:  Table [dbo].[Product]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[BrandID] [int] NULL,
	[Name] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL,
	[Price] [money] NULL,
	[Thumbnail] [nvarchar](50) NULL,
	[Image] [nvarchar](50) NULL,
	[PromoFront] [bit] NULL,
	[PromoDept] [bit] NULL,
	[Rating] [float] NULL,
	[Features] [text] NULL,
	[Stock] [int] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE FULLTEXT INDEX ON [dbo].[Product](
[Description] LANGUAGE [English], 
[Name] LANGUAGE [English])
KEY INDEX [PK_Product]ON ([BalloonShopFullText], FILEGROUP [PRIMARY])
WITH (CHANGE_TRACKING = AUTO, STOPLIST = SYSTEM)
GO
SET IDENTITY_INSERT [dbo].[Product] ON
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (1, 1, N'Epiphone Elitist 1965 Casino', N'<h2 class=''heading''>
<p style=''''>A full hollowbody with P-90s generates the chimey tones from Revolver to Let it Be.</p>
</h2><div class=''details''>
<p class=''description''>You''ll love the light weight and comfortable neck of the Epiphone Elitist 1965 Casino. The Elite series are instruments that approach custom shop perfection. Crafted in Japan with premium woods, fitted with American pickups and circuitry—-even American-made toggle switches and Grover tuners--they''re made at a special factory devoted to their manufacture where they receive a high degree of hands-on luthier attention. You''ll find the Elite 1965 Casino true to the original made famous by The Beatles. A finely crafted, great-sounding, and easy-playing instrument that beautifully blurs the line between Epiphone and Gibson. Includes hardshell case.</p>
</div>', 1799.0000, NULL, N'Electric1.jpg', NULL, NULL, NULL, N'<ul>
<li>5-ply maple back, sides, and top</li>
<li>One-piece mahogany neck</li>
<li>24-3/4'' scale</li>
<li>Neck set at the 16th fret</li>
<li>Rosewood fretboard</li>
<li>Bone nut, 1-5/8''</li>
<li>22 frets</li>
<li>P90 USA single coil rhythm pickup</li>
<li>P90 USA single coil reverse treble pickup</li>
<li>Nickel hardware</li>
<li>2 volume, 2 tone, 3-way toggle</li>
<li>Grover tuners</li>
</ul>', 14)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (2, 1, N'Epiphone Marcus Henderson Apparition Electric Guitar', N'<h2 class=''heading''>
<p style=''''>Everything that a rock/metal guitar of the 21st century should be.</p>
</h2><div class=''details''>
<p class=''description''>Marcus Henderson, best known as the heavy-metal guitar player behind a majority of guitar parts in the highly successful video game Guitar Hero, has joined forces with Epiphone to design and create the Apparition signature electric guitar. Marcus says this dream axe is designed to be the very definition of what a 21st century guitar should be: substance with killer style, bulletproof construction, and above everything else, huge tone. <br><br>With a shape reminiscent of an Explorer or Futura, the Epiphone Apparition takes guitar design to the extreme and offers a patented All Access heel contour for maximum playability over the whole fretboard. The effortless speed-friendly neck is modeled after the ''60s slim-tapered Gibson Les Paul. The real Floyd Rose tremolo with a specially routed back cavity lets you pull-up screaming harmonics. Active EMG-81 and 85 pickups give you all the killer tones you''ve been dreaming of. A kill button chops the sound up any way your heart desires. <br><br>A hardshell case designed specifically for the Marcus Henderson Apparition guitar is also available.</p>
</div>', 583.9900, NULL, N'Electric2.jpg', NULL, NULL, NULL, N'<ul>
<li>Top: Quilted maple</li>
<li>Back: Solid mahogany</li>
<li>Neck: Set maple</li>
<li>Scale Length: 24-3/4''</li>
<li>Neck Binding: Single-ply white</li>
<li>Neck Profile: Speedtaper with satin finish</li>
<li>Fingerboard: Ebony with crossed scythe inlays</li>
<li>Fingerboard Radius: 14''</li>
<li>Frets: 24 jumbo nickel/silver alloy</li>
<li>Nut Width: 1-11/16''</li>
<li>Truss Rod: Double-action 2-way</li>
<li>Bridge Pickup: EMG-81</li>
<li>Neck Pickup: EMG-85</li>
<li>Power: 9-Volt Battery</li>
<li>Controls: Master volume, master tone, momentary ''kill'' button, 3-way selector switch</li>
<li>Bridge: Double-locking Floyd Rose Original</li>
<li>Hardware: Black</li>
<li>Strap buttons: Straplocks</li>
</ul>', 12)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (3, 1, N'Epiphone Limitied Edition Matt Heafy Les Paul Custom-7', N'<div class=''details''>
<p class=''description''>Epiphone and Trivium''s Matt Heafy have team up to create the Limited Edition Matt Heafy Les Paul Custom-7 seven-string guitar, a totally killer original take on the world-famous Les Paul. Designed in close collaboration with Matt Heafy, each Ltd. Ed. Matt Heafy Les Paul Custom reflects his distinctive approach to guitar, which has earned him and Trivium scores of dedicated fans around the world. Each of Trivium''s albums has sold over half a million copies worldwide. Check out a Trivium performance and you''ll see why. The rule books on how-to-play a Les Paul go right out the window when Heafy plugs in and his revolutionary use of an LP Custom seven-string is a breakthrough in contemporary metal and hard rock. Now, Matt Heafy joins an illustrious group of Epiphone artists with the introduction of this signature model.</p>
</div>', 799.9900, NULL, N'Electric3.jpg', NULL, NULL, NULL, N'<ul>
<li>Body Material: Mahogany</li>
<li>Top Material: Plain Maple Veneer</li>
<li>Neck Material: Mahogany</li>
<li>Neck Shape: 1960''s SlimTaper; D profile</li>
<li>Neck Joint: Glued In; Deep-Set Neck Joint with ''Axcess'' heel</li>
<li>Truss Rod: Adjustable</li>
<li>Truss Rod Cover: 2-ply (Black/White); ''MKH Les Paul Custom'' in white silkprint</li>
<li>Scale Length: 24.75''</li>
<li>Fingerboard Material: Ebony with pearloid Block inlays</li>
<li>Neck Pickup: EMG-707</li>
<li>Knobs: Black Speed Knobs</li>
<li>Bridge Pickup: EMG-81-7</li>
<li>Controls:</li>
<li>Epiphone All-metal 3-way Pickup Selector; White toggle cap</li>
<li>Neck Pickup Volume</li>
<li>Bridge Pickup Volume</li>
<li>Neck Pickup Tone</li>
<li>Bridge Pickup Tone</li>
<li>(Active) 9V battery compartment in back</li>
<li>Bridge: LockTone tune-o-matic/Stopbar</li>
<li>Binding Body Top - 7-ply (White/Black)</li>
<li>Headstock - 5-ply (White/Black)</li>
<li>Fingerboard - 1-ply (White)</li>
<li>Fingerboard Radius: 12''</li>
<li>Frets 22; medium-jumbo</li>
<li>Nut Width: 1-7/8''</li>
<li>Hardware: Black</li>
<li>Machine Heads: Deluxe Die-cast with metal Tulip Buttons 14:1 ratio</li>
<li>Output Jack: Epiphone Heavy-Duty with metal output jack plate</li>
<li>Color: Ebony (gloss)</li>
</ul>', 56)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (4, 1, N'Epiphone 50th Ann. 1962 Sheraton E212TV Outfit Tremotone', N'<div class=''details''>
<p class=''description''>The 50th Anniversary ''1962'' Sheraton E212TV Outfit features the original early ''60s Sheraton profile with a laminated maple body with period correct binding, a 5-layer bound tortoise-style pickguard with metal foil ''E'', and a mahogany neck with a 1960s SlimTaper™ profile. The rosewood fingerboard has 22 medium jumbo frets, pearloid and Abalone Block and Triangle inlays, and Grover™ ''kidney'' style machine heads with a 14:1 ratio on a dovewing 3-on-a-side headstock with pearloid vine inlay. The Sheraton was Epiphone''s original flagship, top-of-the-line semi-hollow archtop and the ''1962'' Anniversary puts the Sheraton back at the top where it belongs---in style.<br><br>The Sheraton''s electronics feature CTS potentiometers, Epiphone''s non-rotating ¼'' output jack, Top Hat knobs with metal inserts, and Gibson USA mini-humbuckers for superior sound. The Sheraton comes with the Tremotone™ tremolo tailpiece.<br><br>The reissue Epiphone Tremotone™ tremolo bar was carefully redesigned and retooled to function and look exactly like the original but with subtle improvements to ensure that your Tremotone™ will last much longer than vintage models. The E212TV Outfit comes with a period-correct hard case and a 1962 Collection Certificate of Authenticity.<br><br>All three of these Epiphone classics will be issued in limited quantities of 1,962 pieces.<br><br>All of the ''1962'' Collection instruments were designed by Epiphone luthiers who worked from the best examples of vintage originals found in Epiphone''s own collection in Nashville. Measure for measure, the ''1962'' Collection recreates the look, sound, and feel of the originals first produced at the Gibson &amp; Epiphone factory in Kalamazoo, Michigan in 1962, the same factory that produced the Les Paul Standard, the SG, and the 335.</p>
</div>', 849.0000, NULL, N'Electric4.jpg', NULL, NULL, NULL, N'<ul>
<li>Sheraton profile with a laminated maple body and period correct binding</li>
<li>5-layer bound tortoise-style pickguard with metal foil ''E''</li>
<li>Mahogany neck with a 1960s SlimTaper™ profile</li>
<li>Rosewood fingerboard with 22 medium jumbo frets</li>
<li>Pearloid and Abalone Block and Triangle inlays</li>
<li>Grover™ ''kidney'' style machine heads</li>
<li>3-on-a-side headstock with pearloid vine inlay.</li>
<li>CTS potentiometers</li>
<li>Top Hat knobs with metal inserts</li>
<li>Gibson USA mini-humbuckers for superior sound</li>
<li>Tremotone™ tremolo tailpiece</li>
</ul>', 36)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (5, 2, N'Fender 2012 American Standard Stratocaster Electric Guitar with Maple Fingerboard', N'<h2 class=''heading''>
<p>Tradition redesigned.</p>
</h2><div class=''details''>
<p class=''description''>The American Standard Stratocaster is the same great best-selling, go-to guitar it has always been, and now it’s upgraded with aged plastic parts and full-sounding Fender Custom Shop Fat ’50s pickups. The latest iteration of our time-honored classic, it is the very essence of Strat tone and remains a beauty to see, hear and feel. The American Standard Stratocaster Electric Guitar with Rosewood Fingerboard is a Fender icon. It''s a beauty to behold in sound, look and feel. Features include hand-rolled fingerboard edges, Custom Shop Fat ''50s pickups, staggered tuners, improved bridge with bent steel saddles and copper-infused high-mass block for increased resonance and sustain, tinted neck, high-gloss maple or rosewood fretboard, satin neck back for smooth playability, thin-finish undercoat that lets the body breathe and improves resonance, and Fender® exclusive SKB® molded case.</p>
</div>', 949.9900, NULL, N'Electric5.jpg', NULL, NULL, NULL, N'<ul>
<li>Solid alder body (urethane finish)</li>
<li>Modern C-shaped maple neck with rich, deep neck tint, glossed neck front with satin back</li>
<li>22 medium jumbo frets</li>
<li>3 full-sounding Fender Custom Shop Fat ’50s pickups</li>
<li>Delta-tone™ no-load circuit (includes high output bridge pickup and special no-load tone control for middle and bridge pickups)</li>
<li>Bent steel Fender saddles</li>
<li>Copper Infused Cast Strat® Bridge Block (steel with copper- 100% metal) for Increased Mass and Improved Tone</li>
<li>Thinner undercoat for improved body resonance</li>
<li>American 2-point trem</li>
<li>Rosewood or maple fretboard</li>
<li>Staggered machine heads</li>
<li>Aged plastic parts</li>
</ul>', 88)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (6, 2, N'Fender 2012 American Standard Telecaster Electric Guitar with Maple Fingerboard', N'<h2 class=''heading''>
<p>The best of yesterday and today in one amazing Tele.</p>
</h2><div class=''details''>
<p class=''description''>From the garage to the club to the stadium, the American Standard Telecaster is the same great best-selling go-to guitar it always has been, and now it’s upgraded with a comfortable new body contour and classic-sounding Fender Custom Shop Twisted Tele (neck) and Broadcaster (bridge) pickups. The latest incarnation of a truly timeless classic, it rings more fully, brightly and crisply than ever.<br><br>The look, sound and vibe of the Telecaster® resonates throughout the history of popular music. From orchestra pits to mosh pits, small clubs to stadiums and garages to recording studios, its signature sound is everywhere. The American Standard Telecaster Electric Guitar with Maple Fingerboard has hand-rolled fingerboard edges, Custom Shop pickups, staggered tuning machines, improved bridge with bent steel saddles mounted to a stamped-brass bridge plate for increased resonance and sustain, tinted neck, high-gloss maple fingerboard, satin-finished neck back, thinner finish undercoat that lets the body breathe and improves resonance, and Fender-exclusive SKB® molded case.<br><br></p>
</div>', 1199.9900, NULL, N'Electric6.jpg', NULL, NULL, NULL, N'<ul>
<li>Comfortable contour body</li>
<li>Fender Custom Shop Twisted Tele (neck) and Broadcaster (bridge) pickups</li>
<li>Deluxe hardware</li>
<li>Solid alder body (black, 3-color sunburst, candy cola, and blizzard pearl) or ash body (2-tone sunburst, natural, crimson red transparent)</li>
<li>Modern C-shaped maple neck with rich, deep neck tint, glossed neck fretboard with satin back and rolled edges</li>
<li>Delta-tone™ no-load circuit</li>
<li>Highly finished frets</li>
<li>Detailed nut work</li>
<li>Bent steel Fender saddles</li>
<li>American Standard Tele® Bridge Plate (steel saddles and brass plate) for improved resonance</li>
<li>Thinner undercoat for improved body resonance</li>
<li>Rosewood or maple fretboard</li>
<li>Staggered machine heads</li>
</ul>', 53)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (7, 2, N'Fender Standard Stratocaster Electric Guitar', N'<h2 class=''heading''>
<p>Setting the standard for electric guitars.</p>
</h2><div class=''details''>
<p class=''description''>The Fender Standard Stratocaster Electric Guitar is the guitar design that changed the world. Includes select alder body, 21-fret gloss maple neck with a maple fretboard, 3 single-coil pickups, a vintage-style tremolo, and die-cast tuning keys. At this low price, why play anything but the real thing?</p>
</div>', 399.9900, NULL, N'Electric7.jpg', NULL, NULL, NULL, N'<ul>
<li>Select alder body</li>
<li>Modern, C-shaped gloss maple neck</li>
<li>Maple fretboard</li>
<li>21 medium jumbo frets</li>
<li>Vintage-style tremolo</li>
<li>Die-cast tuning keys</li>
<li>3 Standard single-coil pickups with ceramic magnets</li>
</ul>', 96)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (8, 2, N'Fender Deluxe Player''s Stratocaster Electric Guitar', N'<h2 class=''heading''>
<p>High-end features at affordable price.</p>
</h2><div class=''details''>
<p class=''description''>The Fender Deluxe Player''s Stratocaster Electric Guitar gives you classic Strat sound and feel in a beautiful package. Upgraded with American-made Vintage Noiseless pickups, medium-jumbo frets, and a 12'' neck radius. As a result, it sounds fantastic and plays easy. It also is equipped with a push-button pickup switch (in addition to the usual toggle) that gives you 7 pickup combinations. Gold-plated hardware.</p>
</div>', 649.9900, NULL, N'Electric8.jpg', NULL, NULL, NULL, N'<ul>
<li>4-ply brown shell pickguard</li>
<li>12'' neck radius</li>
<li>Modern C-shaped maple neck</li>
<li>21 medium-jumbo frets</li>
<li>Gold-plated hardware</li>
<li>3 Vintage Noiseless single-coil pickups</li>
<li>Push-button switch activates bridge pickup in positions 4 and 5 of blade switch for 7 pickup configurations</li>
<li>Vintage-style synchronized tremolo</li>
</ul>', 35)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (9, 3, N'Gibson 2012 Les Paul Standard Premium AAA Electric Guitar', N'<h2 class=''heading''>
<p>Premium woods and modern electronics adorn the 2012 Les Paul Standard from Gibson.</p>
</h2><div class=''details''>
<p class=''description''>All the sumptuous ingredients that makes the Les Paul name great are in the 2012 Gibson Les Paul Standard Electric Guitar in spades: your choice of a skillfully carved plain or AAA figured maple top on a mahogany back perfectly joined to a mahogany neck, Burstbucker Pro pickups each fitted with coil-split capability, locking tuning machines, and neck and body binding, this is truly a full-featured, loaded Les Paul and now features a compound-radius rosewood fretboard. Also, new “modern” weight relieving is carefully applied inside the mahogany back to provide outstanding balance while optimizing the sonic capabilities of the guitar.<br><br><strong>Burstbucker Pro Humbuckers<br></strong>Gibson’s drive to recapture the magic of the original “Patent Applied For” humbucker pickups of the 1950s culminated with the introduction of the Burstbucker line in the early 1990s. In 2002, Gibson followed up this innovative accomplishment with yet another breakthrough in pickup design—the Burstbucker Pro, designed specifically for Les Paul Standards. The Burstbucker Pro features an Alnico V magnet (instead of the Alnico II), which offers slightly higher output and allows preamps to be driven a little harder to achieve a more natural break-up. Like all Burstbuckers, the Burstbucker Pro has asymmetrical coils—true to the original PAFs—which supply a more open sound. The Burstbucker Pro Neck is wound slightly less than the original PAFs, while the Burstbucker Pro Bridge is slightly overwound for increased output. The Burstbucker Pro pickups are also wax potted to allow loud volume pressures with minimal feedback. A high-quality Switchcraft® output jack ensures a perfect connection to your cable allowing the purest signal to leave the guitar.<br><br><strong>2 Push/Pull Tone controls</strong><br>In addition to controlling tone, one is a phase switch which allows for new tonal flavors, especially when used in combination with the coil-splitting feature. The other engages the “Pure Bypass” circuit which sends the bridge pickup signal straight to the output jack to deliver the most direct, pure and dynamic signal possible.<br><br><strong>Coil Tapping<br></strong>The bridge and neck pickups can each be split via their own push/pull switch on the volume knob to achieve a single-coil tone. This coil-tapping function allows you to instantly achieve brighter, snappier tones from this humbucker-loaded guitar, to replicate the sounds of other classic guitars equipped with single-coil pickups, and to return to the Les Paul Standard''s thick, warm, powerful, full-humbucking tone at the push of a knob. Frequency-tuned “Fat Tap” coil tapping creates a fatter single coil tone with great output and balance when switching from dual to single coils. Single coil configurations are specially designed to be “hum-reducing”.<br><br><strong>Neck Profile<br></strong>The 2012 Les Paul Standard is fitted with Gibson’s classic ’60s slim-taper neck profile, which is the more modern, slim-tapered neck most commonly associated with the Les Paul and SG models of the early 1960s. A compound radius rosewood fingerboard allows for comfort, speed and precision at all points up and down the neck. It accommodates lower string height while achieving unrestricted, smooth, upper-register string bending.<br><br><strong>Locking Grover Tuners<br></strong>The 2012 Les Paul Standard is outfitted with locking tuners from Grover, which deliver ease of use through a standard tuner and positive locking mechanism that securely locks each string in place. Simply insert each string through the string hole, turn the dial on the bottom of the tuner to lock the string, and begin tuning. Each string can be tuned to pitch in less than one complete revolution of the post. These Grover machine heads feature completely sealed components with an improved 18:1 tuning ratio.<br><br>Gibson Hardshell case included.</p>
</div>', 2999.0000, NULL, N'Electric9.jpg', NULL, NULL, NULL, N'<ul>
<li>Mahogany body</li>
<li>Carved AAA flame maple top (plain top on solid colors)</li>
<li>24.75'' Scale mahogany ''60s slim-taper set neck</li>
<li>Rosewood fretboard with compound radius</li>
<li>22 Medium frets</li>
<li>Trapezoid inlay</li>
<li>Gibson Burstbucker Pro humbucking neck pickup with coil-tap</li>
<li>Gibson Burstbucker Pro humbucking bridge pickup with coil-tap</li>
<li>Individual volume and tone controls</li>
<li>2 Push/Pull Tone controls</li>
<li>3-Way pickup selector</li>
<li>Tune-o-matic bridge</li>
<li>Stopbar tailpiece</li>
<li>Grover locking keystone tuning machines</li>
</ul>', 35)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (10, 3, N'Gibson SG Standard Electric Guitar', N'<h2 class=''heading''>
<p>The classic Gibson SG Standard revamped for 2013.</p>
</h2><div class=''details''>
<p class=''description''>The light bulb. Television and radio. The microwave oven. Gibson''s SG Standard — all inspirations of genius that transformed the way we live. Gibson''s redesign of the Les Paul in 1960 was a bold move. But it paid off big time. It announced a new, radical electric guitar design for Gibson, and the world. It also inspired a rebellious generation of ideas and music rarely experienced before, and certainly not since.<br><br>Today, that same radical design – and restless spirit – lives on in Gibson''s celebrated SG Standard. The redesigned 2013 SG Standard pays tribute to this revolutionary guitar. This guitar boasts all of the classic SG appointments including a double-cutaway mahogany body, slim mahogany neck, bound rosewood fretboard and Gibson’s ’57 Classic humbucking pickups for pure vintage-style bite. Plus, now all SG Standard models feature the original, smaller pickguard with tenon cover for the ultimate vintage-inspired look. It''s all there, in all its original glory. Transform your playing. Revolutionize your life. Buy the 2013 Gibson SG Standard today and experience your own uprising firsthand.</p>
</div>', 1299.0000, NULL, N'Electric10.jpg', NULL, NULL, NULL, N'<ul>
<li>Mahogany body</li>
<li>24.75'' Scale SG slim mahogany neck</li>
<li>Bound rosewood fretboard with trapezoid inlays</li>
<li>Chrome tune-o-matic bridge</li>
<li>Vintage-style tuning machines</li>
<li>57 Classic humbucking pickups</li>
<li>Small SG pickguard</li>
</ul>', 35)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (11, 3, N'Gibson Firebird 2010 Electric Guitar', N'<h2 class=''heading''>
<p>Based on the 1963 original Firebird updated with ceramic ''buckers and gearless tuners.</p>
</h2><div class=''details''>
<p class=''description''>The Gibson Firebird 2010 features the original 1963-style classic reverse body styling and neck-through body design for increased sustain and solid attack. Modern improvements include high-output ceramic mini-humbuckers and gearless Steinberger 40:1 ratio machine heads for extreme tuning precision. The Firebird 2010 has a mahogany body with walnut stringers and mahogany wings, bound rosewood fingerboard with trapezoid inlays, and chrome hardware.</p>
</div>', 1649.0000, NULL, N'Electric11.jpg', NULL, NULL, NULL, N'<ul>
<li>Classic reverse body styling</li>
<li>Neck-through body design for increased sustain and attack</li>
<li>Mahogany body with walnut stringers and mahogany wings</li>
<li>Bound rosewood fingerboard with trapezoid inlays</li>
<li>Steinberger gearless tuners</li>
<li>High-output creramic mini-humbuckers at the bridge and neck</li>
<li>2 volume and 2 tone controls</li>
<li>3-way switch</li>
</ul>', 87)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (12, 3, N'Gibson Flying V Government Series Electric Guitar', N'<h2 class=''heading''>
<p>A unique Flying V with a tall tale to tell.</p>
</h2><div class=''details''>
<p class=''description''>Gibson’s Flying V has been a radical instrument since it was first introduced back in 1958, but this one’s pedigree is even more notorious than most. When federal agents raided Gibson in 2011 to seize stocks of ebony and rosewood that they claimed were in violation of the Lacey Act, they also took away several completed necks and bodies made largely from wood that was uncontested.<br><br>Now that the Federal charges have been dropped, in a deal agreed between Gibson and the Justice Department, several partially completed neck and body blanks have been returned to the factory, ready to be incorporated into Gibson USA’s new Government Series, a wry celebration of an infamous moment in Gibson history.<br><br>The Government Series Flying V—a powerful and superbly playable guitar in and of itself—includes a number of completed necks, some still stamped with the 2011 serial numbers they were given before being taken away by Federal agents. Interspersed among the general production run of the Government Series, the confiscated and returned components will be “golden tickets” of a sort, rendering these particular guitars instantly collectible. Regardless of whether the entire guitar, or just the fingerboard was seized, each wears a commemorative Government Grey finish in vintage gloss nitrocellulose, with black hardware, and a five-ply pickguard that is hot-stamped in gold with the Government Series emblem. In addition, each guitar carries a pair of Gibson’s powerful Dirty Fingers+ humbucking pickups, and the full traditional Flying V mojo for unparalleled tone and playability.<br><br>The Government Series Flying V is built on the same foundation that has made so many Gibson solidbody electric guitars legendary for more than 60 years. Its solid-mahogany body offers superb depth and warmth, and the iconic “winged” design allows unparalleled upper-fret access. A solid, quarter-sawn mahogany neck is glued in and carved to a fast, slim profile that measures .800” at the 1st fret and .850” at the 12th. It is topped with an unbound fingerboard made from genuine, one-piece rosewood, with 22 medium-jumbo frets and acrylic dot inlays. Classic Gibson construction ensures optimum playability, too, along with traditional specs like a 24 ¾” scale length, a 1 11/16” width across the PLEK-cut Corian™ nut, and a 12-inch fingerboard radius for smooth, easy bending.</p>
</div>', 999.0000, NULL, N'Electric12.jpg', NULL, NULL, NULL, N'<ul>
<li>Mahogany body</li>
<li>Set mahogany neck</li>
<li>Nitrocellulose finish</li>
<li>Rounded neck shape</li>
<li>24-3/4'' scale length</li>
<li>Rosewood fingerboard</li>
<li>12'' Radius</li>
<li>Dot fingerboard inlays</li>
<li>22 frets</li>
<li>1-11/16'' nut width</li>
<li>Tune-o-matic/Stop bar bridge</li>
<li>Grover tuners</li>
<li>Dirty Fingers pickups</li>
<li>2 x Volume, 2 x Tone, 3-way Pickup Toggle</li>
<li>Certificate of Authenticity hand signed by Henry Juszkiewicz</li>
</ul>', 57)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (13, 4, N'Ibanez S570DXQM Electric Guitar', N'<h2 class=''heading''>
<p>A rockin'' electric guitar with luxurious looks and feel.</p>
</h2><div class=''details''>
<p class=''description''>Like its S series siblings, the Ibaneze S570DXQM Electric Guitar is one of easiest to play and most comfortable guitars you can buy. The S570DXQM Ibanez is crafted with a gorgeous carved quilted maple top and a mahogany body with a matching headstock.  <br><br>The original sleek archtop gives the S570DXQM guitar a feel that is luxurious and highly playable. The 3-piece bolt-on maple neck has the fast Ibanez Wizard II neck profile and is dressed with 24 jumbo frets for increased expressiveness. <br><br>The ZR (Zero Resistance) double-locking ball bearing bridge gives any guitarist amazing creative freedom. The tone of Ibanez INF pickups is unmatched. Other appointments include special designed fingerboard inlays and Cosmo black hardware. <br><br><strong>Infinity humbucking pickups</strong> <br>The versatile and responsive INF1 pickup in the neck position has a ceramic magnet and delivers warm classic tone with added output. A single-coil Infinity INF Single 1 pickup with an alnico magnet in the middle position is designed to work with humbuckers set in a split configuration. The INF2 humbucking bridge pickup offers enhanced midrange output. Its Alnico-V magnet enhances individual string clarity with excellent dynamics.<br><br><strong>Zero Point tremolo system</strong><br>The unique Ibanez Zero Point trem system stabilizes the bridge and insures tuning stability by consistently returning the tremolo to the center or zero point. In addition to the usual tremolo springs, the Zero Point System features 2 additional springs, which insure that the bridge returns to the center point. Zero Point system adjustment is quickly set by hand—there''s no need for a hex wrench. The ZR tremolo uses ball-bearings for smooth, precise, friction-free action.</p>
</div>', 649.9900, NULL, N'Electric13.jpg', NULL, NULL, NULL, N'<ul>
<li>S Series model</li>
<li>Solidbody double-cutaway style</li>
<li>Carved quilted maple top</li>
<li>Mahogany body</li>
<li>Matching headstock</li>
<li>3-piece bolt-on maple neck</li>
<li>25-1/2'' scale</li>
<li>Wizard II neck profile</li>
<li>Rosewood fretboard</li>
<li>15.75'' radius</li>
<li>1.69'' nut width</li>
<li>24 jumbo frets</li>
<li>Special designed fretboard inlays</li>
<li>Infinity INF2 humbucking bridge pickup</li>
<li>Infinity INF Single1 single-coil</li>
<li>Infinity INF humbucking 1 neck pickup</li>
<li>Master volume and tone knobs</li>
<li>5-way blade pickup selector</li>
<li>Die-cast locking tuners</li>
<li>Zero resistance (ZR) Double-locking tremolo bridge</li>
<li>Cosmo black hardware</li>
</ul>', 24)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (14, 4, N'Ibanez RG8 8-String Electric Guitar', N'<h2 class=''heading''>
<p>An exceptional value with its string-through design, fixed bridge and slim Wizard II-8 neck.</p>
</h2><div class=''details''>
<p class=''description''>The RG8 Electric Guitar brings Ibanez 8-String research and design to a price point that''s hard to resist. It features all of the advantages of the Ibanez RG series including the famous slim, fast, and ultra-playable Wizard neck. Its basswood body has through-body stringing and a fixed bridge for maximum sustain while specially-designed Ibanez AH-18 and AH-28 humbucking pickups evenly reproduce all of the wide frequencies of which this 8-string is capable. If you''ve been on the lookout for a reliable and affordable 8-string, here it is.</p>
</div>', 399.9900, NULL, N'Electric14.jpg', NULL, NULL, NULL, N'<ul>
<li>5pc Maple/Walnut Wizard II-8 neck</li>
<li>Basswood body</li>
<li>Rosewood fingerboard</li>
<li>Pickups: AH-18 (H) neck, AH-28 (H) bridge</li>
<li>Fixed bridge 8</li>
<li>Black hardware</li>
<li>String gauges :1st to 8th: .009/.011/.013/.016/.024/.032/.042/.054/.065)</li>
<li>Factory Tuning: 1st to 8th: (D#/A#/F#/C#/G#/D#/A#/F)</li>
</ul>', 98)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (15, 4, N'Ibanez Artcore AS53 Semi-Hollow Electric Guitar', N'<h2 class=''heading''>
<p>Transparent finishes let the glory of sapele shine through.</p>
</h2><div class=''details''>
<p class=''description''>The double-cutaway body with sapele top, back, and sides is strong and resonant while the 22-fret set mahogany neck with bound rosewood fretboard is satin-smooth and dishes out sweet sustain. A master volume and master tone control 2 ACH-ST open-coil humbuckers with ceramic magnets.</p>
</div>', 309.9900, NULL, N'Electric15.jpg', NULL, NULL, NULL, N'<ul>
<li>Sapele top, back, and sides</li>
<li>Mahogany neck</li>
<li>Bound rosewood fingerboard</li>
<li>22 frets</li>
<li>Two ACH-ST open-coil humbuckers with ceramic magnets</li>
</ul>', 64)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (16, 4, N'Ibanez JEM/UV Steve Vai Signature Electric Guitar', N'<h2 class=''heading''>
<p>An incredible combination of specifications designed by Steve Vai to break boundaries.</p>
</h2><div class=''details''>
<p class=''description''>Steve Vai is one of the revered few on that short list of players who have changed the way we all think about what a guitar can really do. His signature Ibanez guitars are no different: iconic, sublime, awe-inspiring, and outrageous. The Ibanez JEM/UV Series is a spectacular instrument that features a contoured basswood body and a 5-piece maple and walnut bolt-on neck that provides easy access to all registers. A 24-fret rosewood fretboard is adorned with a colorful ''Tree of Life'' pattern inlay and has the high-speed JEM Prestige profile that gives Steve Vai the playability he needs. The JEM/UV sports chrome hardware and includes Steve''s ''Monkey Grip'' handle cut on the body.</p>
</div>', 1399.9900, NULL, N'Electric16.jpg', NULL, NULL, NULL, N'<ul>
<li>Basswood body</li>
<li>5pc maple/walnut JEM premium neck</li>
<li>Rosewood fretboard with 24 jumbo frets</li>
<li>Tree of Life inlay</li>
<li>DiMarzio&reg; Evolution&reg; DP158 neck pickup</li>
<li>DiMarzio&reg; Evolution&reg; DPS1 middle pickup</li>
<li>DiMarzio&reg; Evolution&reg; DP159 bridge pickup</li>
<li>Ibanez Edge Bridge</li>
</ul>', 32)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (17, 1, N'Epiphone Limited Edition Hummingbird Artist Acoustic Guitar', N'<h2 class=''heading''>
<p>A no-frills, economy version of the legendary dreadnought.</p>
</h2><div class=''details''>
<p class=''description''>A true legend of acoustic instruments, the Gibson Hummingbird was first introduced in 1960 as Gibson''s earliest square-shoulder dreadnought. The Epiphone Limited Edition Hummingbird Artist is a no-frills edition of a classic acoustic guitar that features a solid spruce top, mahogany backs and sides for a warm, natural acoustic tone. The 25.5'' scale mahogany neck is topped with a rosewood fingerboard that facilitates fast picking styles. Appointments include a sculpted tortoise shell pickguard and split parallel fingerboard inlays. The Epiphone Hummingbird Artist Guitar possesses all the power you expect from a dreadnought and has strong bass tones that are favored by country and bluegrass players.<br><br>Check the drop-down menu to the right to select colors and/or other options.</p>
</div>', 249.9900, NULL, N'Acoustic1.jpg', NULL, NULL, NULL, N'<ul>
<li>Body Style: Square-shoulder Dreadnought</li>
<li>Top: Solid Spruce</li>
<li>Back: Mahogany</li>
<li>Sides: Mahogany</li>
<li>Neck: Set mahogany</li>
<li>Scale Length: 25.5</li>
<li>No. of frets: 20/14 open</li>
<li>Fingerboard: Rosewood</li>
<li>Fingerboard Inlays: Split parallelogram</li>
<li>Nut width: 1.68''</li>
<li>Binding: on body and neck</li>
<li>Rosette: Parallel stripes</li>
<li>Bridge: Rosewood</li>
<li>Pickguard: Sculpted Tortoise shell</li>
<li>Tuners: Tulip style</li>
</ul>', 68)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (18, 1, N'Epiphone DR-212 12-String Acoustic Guitar', N'<h2 class=''heading''>
<p>Big-toned, super-playable, and remarkably priced.</p>
</h2><div class=''details''>
<p class=''description''>The select spruce top Epiphone DR-212 12-String Acoustic Guitar is a dreadnought with mahogany body that delivers the tones and warmth you want. The set mahogany neck and rosewood fingerboard deliver great feel. Epiphone gives you chrome die-cast machine heads with your DR-212 guitar to provide great tuning stability.</p>
</div>', 169.0000, NULL, N'Acoustic2.jpg', NULL, NULL, NULL, N'<ul>
<li>Mahogany body</li>
<li>Select spruce top</li>
<li>Set mahogany neck</li>
<li>Rosewood fingerboard with dot inlays</li>
<li>Chrome hardware</li>
<li>25.5'' scale</li>
</ul>', 45)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (19, 1, N'Epiphone AJ-220S Acoustic Guitar', N'<h2 class=''heading''>
<p>Sitka spruce top and luxurious appointments in an Epiphone guitar offered at a steal.</p>
</h2><div class=''details''>
<p class=''description''>From Epiphone comes this beautiful entry-level guitar, priced for anyone to afford. Dressed in a beautiful finish, and featuring appointments you''d often find on higher-end guitars, the Epiphone AJ-220S guitar sounds just as good as it looks. Built on advanced jumbo body design, the depth of the body and the width of the shoulders create a large sound chamber, presenting tone that is full, deep, and loud.</p>
</div>', 199.0000, NULL, N'Acoustic3.jpg', NULL, NULL, NULL, N'<ul>
<li>6-string acoustic guitar</li>
<li>Advanced jumbo body style</li>
<li>Mahogany back and sides</li>
<li>Solid Sitka spruce top</li>
<li>25-1/2'' scale</li>
<li>Glued neck joint</li>
<li>Mahogany neck</li>
<li>Rosewood bridge and fretboard</li>
<li>Sealed die-cast tuners with 14:1 ratio</li>
<li>Nickel hardware</li>
</ul>', 26)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (20, 1, N'Epiphone Limited Edition DR-90 Acoustic Guitar', N'<h2 class=''heading''>
<p>Laminated spruce top, nato body, and full dreadnought size at a great price.</p>
</h2><div class=''details''>
<p class=''description''>The Epiphone Limited Edition DR 90 Acoustic Guitar is an excellent choice for a first steel string acoustic guitar. It has features that will continue to satisfy the student as he or she progresses and that are normally associated with more expensive instruments.</p>
</div>', 109.9900, NULL, N'Acoustic4.jpg', NULL, NULL, NULL, N'<ul>
<li>Body Type: Dreadnought</li>
<li>Body Wood: Nato</li>
<li>Top wood: Laminated Spruce</li>
<li>Scale Length: 25.5''</li>
<li>Neck Joint: Glued In</li>
<li>Neck Wood: Mahogany</li>
<li>Fretboard: Rosewood</li>
<li>Neck Shape: D-profile</li>
<li># frets: 20</li>
<li>Nut Width: 1-11/16''</li>
<li>Fretboard Radius: 12''</li>
<li>Bridge: Rosewood; Compensated</li>
<li>Tuners: Covered; 14:1 Diecast</li>
<li>Hardware color: Nickel</li>
</ul>', 75)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (21, 2, N'Fender CD60 All-Mahogany Acoustic Guitar', N'<h2 class=''heading''>
<p>A sweet-sounding dreadnought with features that belie its price.</p>
</h2><div class=''details''>
<p class=''description''>One of Fender''s best-selling acoustics is now available with the sweet mellow tone of an all-mahogany body. The CD60 All-Mahogany Acoustic Guitar has upgraded features that include a new black pickguard, mother-of-pearl acrylic rosette design, new compensated bridge design, white bridge pins with black dots and smaller (3mm) dot fingerboard inlays. Other features include scalloped X bracing, black body binding, 20-fret fingerboard, dual-action truss rod and die-cast tuners. Hardshell case included.</p>
</div>', 199.9900, NULL, N'Acoustic5.jpg', NULL, NULL, NULL, N'<ul>
<li>Dreadnought size</li>
<li>All-mahogany body and neck produces a warm, mellow tone</li>
<li>Scalloped X Bracing for increased resonance</li>
<li>Mother-of-pearl acrylic rosette design</li>
<li>Compensated bridge and die-cast tuners for better intonation</li>
</ul>', 37)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (22, 2, N'Fender Alkaline Trio Malibu Mahogany Acoustic Guitar', N'<h2 class=''heading''>
<p>A striking and unique signature Fender&reg; Malibu model inspired by Alkaline Trio.</p>
</h2><div class=''details''>
<p class=''description''>Chicago punks Alkaline Trio jumped in on the design of what must be one of Fender’s most distinctive acoustic guitars ever&mdash;the Alkaline Trio Malibu. The instrument takes the classic ’60s-era sun-and-fun Malibu folk-style acoustic and dresses it up with a graceful heart-shaped rosette designed by the band, as seen gracing the cover of 2011 acoustic album <em>Damnesia</em>.<br><br>Premium features include a richly resonant all-mahogany body with the aforementioned AT-designed heart-shaped rosette, scalloped X bracing, C-shape maple Stratocaster&reg; neck with a 20-fret rosewood fretboard, body and neck binding, rosewood bridge with synthetic bone compensated saddle, chrome hardware and gloss neck and body finish.</p>
</div>', 279.9900, NULL, N'Acoustic6.jpg', NULL, NULL, NULL, N'<ul>
<li>Laminate mahogany top</li>
<li>Laminate mahogany back and sides</li>
<li>Scalloped X-bracing</li>
<li>25.5'' Scale maple Stratocaster&reg; neck</li>
<li>Rosewood fingerboard</li>
<li>20 frets</li>
<li>Alkaline Trio-designed heart rosette</li>
</ul>', 74)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (23, 2, N'Fender Kozik Punk Age Acoustic Guitar', N'<h2 class=''heading''>
<p>Impressive graphics make this a work of art you can play.</p>
</h2><div class=''details''>
<p class=''description''>The Fender Kozik Punk Age Acoustic Guitar is a work of art, for which a Fender dreadnought guitar provides the perfect canvas. Frank Kozik is famous for his graphic artwork and has been described as ''one of the rock world''s top poster artists.'' The image on this guitar is a remarkable reminder of his talents.<br><br>The guitar itself features a laminated spruce top, laminated agathis back and sides, rosewood bridge and fretboard, and die-cast tuners. Combined with the inspired illustration, the result is a visually striking acoustic with plenty of balanced sound projection and attitude.</p>
</div>', 149.9900, NULL, N'Acoustic7.jpg', NULL, NULL, NULL, N'<ul>
<li>Body Style: Dreadnought</li>
<li>Color: Natural</li>
<li>Finish: Gloss Polyurethane</li>
<li>Top: Laminated Spruce</li>
<li>Back and Sides: Laminated Agathis</li>
<li>Neck: Laminated Agathis</li>
<li>Fretboard: Rosewood</li>
<li>No. of Frets: 20</li>
<li>Bridge: Rosewood with compensated saddle</li>
<li>Machine Heads: Die-cast</li>
<li>Scale Length: 25.3''</li>
<li>Width at Nut: 1.65</li>
<li>Dot position inlays,</li>
<li>Simple black/white body binding,</li>
<li>Silkscreened logo,</li>
<li>Urea nut and saddle</li>
</ul>', 35)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (24, 2, N'Fender CD 320AS Dreadnought Acoustic Guitar', N'<h2 class=''heading''>
<p>All solid woods, stylish looks, and full, resonant sound.</p>
</h2><div class=''details''>
<p class=''description''>Fender’s all-solid-wood CD 320AS Dreadnought Acoustic Guitar is crafted with the kind of stylish looks, smooth feel and full, resonant tone you’d expect from much more expensive instruments.<br><br>Features include a solid spruce top with scalloped X bracing, elegant abalone rosette and tortoiseshell pickguard; solid mahogany back and sides; gloss natural finish; mahogany neck with dual-action truss rod and gloss finish; 20-fret rosewood fingerboard with 3mm white pearloid dot position inlays; rosewood bridge with compensated bone saddle and black bridge pins with abalone dots; rosewood headstock veneer and mother-of-pearl headstock logo inlay; and gold hardware.</p>
</div>', 599.9900, NULL, N'Acoustic8.jpg', NULL, NULL, NULL, N'<ul>
<li>Dreadnought body</li>
<li>Solid mahogany back</li>
<li>Solid mahogany sides</li>
<li>Solid spruce top</li>
<li>Dovetail neck joint</li>
<li>Mahogany neck</li>
<li>Rosewood fretboard</li>
<li>C shape neck</li>
<li>Bone nut</li>
<li>Rosewood bridge with compensated bone saddle</li>
<li>Die-Cast tuners</li>
<li>Gold hardware</li>
</ul>', 76)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (25, 3, N'Gibson 1942 J-45 Legend Acoustic Guitar', N'<h2 class=''heading''>
<p>Gibson has painstakingly reproduced this knockout vintage acoustic down to the minute details.</p>
</h2><div class=''details''>
<p class=''description''>The Gibson Legends version of the J-45 acoustic guitar handles music from the blues to bluegrass to folk to pop and everything in-between. The Gibson J-45 is one of the most played and cherished acoustic guitars in history. This J-45 remake is hand-crafted by Gibson luthiers using techniques from the J-45 guitar''s heyday. The Legend J-45 boasts an Adirondack spruce top and solid mahogany back and sides to produce unmatched mellow, full-bodied tone.</p>
</div>', 7199.0000, NULL, N'Acoustic9.jpg', NULL, NULL, NULL, N'<ul>
<li>Body: Solid, premium grade Honduran mahogany</li>
<li>Top: Solid, hand-selected Adirondack spruce</li>
<li>Bracing: Exact replica of Gibson''s 1940s bracing dimensions, down to the saw-blade marks</li>
<li>Neck: Exact replica of Gibson''s 1942 large ''V'' neck. One-piece Honduran mahogany, 24.75'' scale</li>
<li>Fingerboard: Madagascar rosewood, with Mother-of-Pearl inlays</li>
<li>Nut: 1.725'' wide, bone</li>
<li>Bridge: Madagascar rosewood, bone saddle</li>
<li>Tuners: German ''Kluson'' nickel-plated tuning machines, custom made white ''button'' tuners</li>
<li>Pickguard: 1940''s style celluloid. ''Firestripe'' pattern</li>
<li>Finish: Ultra thin ''McFadden'' nitrocellulose lacquer</li>
</ul>', 21)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (26, 3, N'Gibson Hummingbird Acoustic-Electric Guitar', N'<h2 class=''heading''>
<p>Originally designed around the frequency range of the human voice for peerless vocal accompaniment.</p>
</h2><div class=''details''>
<p class=''description''>The Gibson Hummingbird is an acoustic-electric guitar that has lured celebrity players with its sweet, velvet tone and distinctive appearance since its release in 1960. The Hummingbird guitar features a Sitka spruce top, mahogany back and sides, 24-3/4'' scale length, dovetail neck-to-body joint, scalloped bracing, and an authentic Hummingbird pickguard. <br><br>Plug the Gibson Hummingbird in and jam with the integrated L.R. Baggs Electronics system.</p>
</div>', 3099.0000, NULL, N'Acoustic10.jpg', NULL, NULL, NULL, N'<ul>
<li>Grover Rotomatic Tuners</li>
<li>Bone nut and Tusq saddle</li>
<li>''Big Sky'' radiused fingerboard edge</li>
<li>Industry''s finest electronics</li>
<li>Premium hardshell case</li>
<li>Distinctive labels</li>
</ul>', 34)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (27, 3, N'Gibson L7-C Acoustic Archtop Guitar', N'<h2 class=''heading''>
<p>The only pure acoustic arch top ever built at Gibson Montana.</p>
</h2><div class=''details''>
<p class=''description''>Based on the original Gibson LC7, this acoustic archtop sports a hand-carved solid Sitka spruce top, hand-carved solid flamed maple back, and solid flamed maple sides. Parallelogram inlays on the bound rosewood fretboard join a bound headstock, fully multibound body, layered pickguard, tulip tuners, and classic trapeze tailpiece to provide elegant visual appeal. Very few of these guitars are made, all of them under the direct supervision of master luthier Ren Ferguson. 25-1/2'' scale and X bracing deliver surprising snap and volume with unsurpassed tonal integrity.</p>
</div>', 5349.0000, NULL, N'Acoustic11.jpg', NULL, NULL, NULL, N'<ul>
<li>Hand-carved solid Sitka spruce top </li>
<li>Hand-carved solid flamed maple back </li>
<li>Solid flamed maple sides </li>
<li>Bound rosewood fretboard with parallelogram inlays </li>
<li>Bound headstock </li>
<li>Fully bound body </li>
<li>Layered pickguard </li>
<li>Tulip tuners </li>
<li>Classic trapeze tailpiece </li>
<li>X-pattern bracing </li>
<li>25-1/2'' scale </li>
</ul>', 56)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (28, 3, N'Gibson J-185 Custom Vine Acoustic Guitar', N'<h2 class=''heading''>
<p>A fine-art masterpiece on your guitar neck!</p>
</h2><div class=''details''>
<p class=''description''>This Gibson Acoustic J-185 Custom Vine Acoustic Guitar features gorgeous abalone and mother-of-pearl fingerboard inlay that is mirrored in the floral engraving on the tortoise pickguard. A bit smaller and more contemporary than the classic J-200, it''s the natural choice for the modern troubadour. Full, bold tone generated by a solid Sitka spruce top is given snappy projection by solid Eastern maple back and sides. V-profile mahogany neck and narrow waist for long-term playing comfort.</p>
</div>', 4499.0000, NULL, N'Acoustic12.jpg', NULL, NULL, NULL, N'<ul>
<li>Jumbo cutaway body</li>
<li>Solid spruce top</li>
<li>Flamed maple sides and back</li>
<li>Ebony fretboard and bridge</li>
<li>Abalone rosette</li>
<li>Full body, neck, and headstock binding</li>
<li>Gold tuners</li>
<li>Multiple top and back binding</li>
</ul>', 78)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (29, 4, N'Ibanez Artwood AW300NT Solid Top Dreadnought Acoustic Guitar', N'<h2 class=''heading''>
<p>Enchanting mahogany and the sweet sound of solid spruce.</p>
</h2><div class=''details''>
<p class=''description''>Straight-ahead stage and studio acoustic dreadnought with a broad expanse of solid Engelmann spruce on top and a revolutionary thin-and-tall bracing system. The Ibanez AW300NT guitar belts out big volume and subtle, warm harmonics that get better with age. Elegant fretboard, headstock, and bridge inlays complement the mahagony back and sides. Gold Grover tuners keep you in key. Dovetail neck joint provides ultimate stability and resonance. Like all Artwood guitars it comes equipped with D''Addario EXP strings, Ivorex II nut and saddle, and Advantage bridge pins.<br></p>
</div>', 279.9900, NULL, N'Acoustic13.jpg', NULL, NULL, NULL, N'<ul>
<li>Solid spruce top</li>
<li>Mahogany back and sides</li>
<li>Innovative bracing system with narrower, taller braces to free up the sound board</li>
<li>Gold Grover tuners</li>
<li>Special fretboard, bridge, and headstock inlays</li>
<li>D''Addario EXP strings</li>
<li>Ivorex II nut and saddle</li>
</ul>', 54)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (30, 4, N'Ibanez AW250 Artwood Solid Top Dreadnought Acoustic Guitar', N'<h2 class=''heading''>
<p>Solid cedar top, mahogany back &amp; sides, and advanced construction.</p>
</h2><div class=''details''>
<p class=''description''>The Artwood series was crafted to produce a traditional as well as a modern guitar. Technology moves forward at a frantic pace and the world of guitar craftsmanship is no different. In producing the AW250 Artwood Dreadnought Acoustic Guitar, Ibanez respected the rich tradition of the acoustic guitar while adding modern interpretations in their continuing search for the ultimate in guitar tone.<br><br>With their full sound, strong bass, and clarity in chording and soloing, dreadnoughts have become the most popular acoustic body style. The AW250RTB dreadnought features a solid cedar top with mahogany back &amp; sides for flavorfully lush tone. Other refinements include a bone saddle on a distinctively contoured bridge and a headstock where tradition and modern style co-exist. This acoustic is your perfect go to guitar whether it''s on stage, in the studio, or relaxing with friends at the campfire</p>
</div>', 299.9900, NULL, N'Acoustic14.jpg', NULL, NULL, NULL, N'<ul>
<li>Body Shape: Dreadnought Body</li>
<li>Neck: Mahogany Neck, dovetail neck joint</li>
<li>Back/Sides: Mahogany Back and Sides</li>
<li>Top: Solid Cedar Top</li>
<li>Rosette: Wooden Rosette</li>
<li>Tuners: Chrome Grover Tuners</li>
<li>Fretboard: Rosewood Bridge and Fretboard</li>
<li>Saddle: Bone Nut and Saddle</li>
<li>BridgePins: Advantage Bridge Pins</li>
</ul>', 53)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (31, 4, N'Ibanez Sage Series SGT220 Dreadnought Acoustic Guitar', N'<h2 class=''heading''>
<p>A great guitar to learn on that doesn’t skimp on sound.</p>
</h2><div class=''details''>
<p class=''description''>The Sage Series SGT220 dreadnought acoustic guitar is a brilliant guitar for those just starting out. Its dreadnought body offers a full sound, strong bass and clarity. Mahogany back and sides with a spruce top gives the SGT220 a clear, woody tone with classic dreadnought punch.<br><br>Ivorex II technology further enhances not only the sound, but playing ease. Thanks to the strong and durable Ivorex II nut you get brilliant highs and more pronounced lows. The Ivorex II saddle is specially formulated to provide ultra-fast response and maximum sound transfer to the body.</p>
</div>', 165.9900, NULL, N'Acoustic15.jpg', NULL, NULL, NULL, N'<ul>
<li>Dreadnought body</li>
<li>Mahogany back and sides</li>
<li>Spruce top</li>
<li>Mahogany neck</li>
<li>Rosewood bridge and fretboard</li>
<li>Onboard tuner</li>
<li>Pearloid rosette </li>
<li>Ibanez Chrome Die-Cast Tuners</li>
<li>Ivorex II nut and saddle</li>
<li>Advantage bridge pins</li>
</ul>', 78)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (32, 4, N'Ibanez Artwood Series AC300 Grand Concert Acoustic Guitar', N'<h2 class=''heading''>
<p>The crossroads of traditional and modern.</p>
</h2><div class=''details''>
<p class=''description''>The Artwood Series AC300 acoustic guitar was crafted to offer the latest advancements without loosing the classic staples of acoustics. Technology moves forward at a frantic pace and the world of guitar craftsmanship is no different. In producing the AC300, Ibanez has respected the rich tradition of the acoustic guitar while adding modern interpretations in a continuing search for the ultimate in guitar tone.<br><br>Rich mahogany back and sides are topped with harmonic Engelmann spruce. It features bone saddles on a distinctive and stylishly contoured bridge. The abalone rosette, chrome Grover tuners and headstock combine classic style with modern aesthetics.<br></p>
</div>', 299.9900, NULL, N'Acoustic16.jpg', NULL, NULL, NULL, N'<ul>
<li>Grand concert body</li>
<li>Solid Engelmann spruce top</li>
<li>Mahogany back and sides</li>
<li>Mahogany neck</li>
<li>Rosewood fingerboard</li>
<li>Chrome Grover tuners</li>
<li>Bone nut and saddle</li>
<li>Abalone rosette</li>
</ul>', 52)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (33, 1, N'Epiphone Thunderbird Pro-IV Bass', N'<h2 class=''heading''>
<p>Distinctive design and amazing sustain.</p>
</h2><div class=''details''>
<p class=''description''>The Epiphone Thunderbird Pro-IV bass builds on the illustrious history of the Thunderbird bass. This distinctively shaped rock-and-roll bass combines traditional design elements with new technology at a price any bassist can afford. <br><br>Epiphone''s Thunderbird Pro-V Bass features a 7-piece walnut/maple neck with through-body construction for amazing sustain and tone. It also hass Epiphone T-Pro bass humbucking pickups with custom active electronics and EQ to dial in the bass sound you seek.<br><br>It features Epiphone''s SlimTaper design, which upholds a precise width-to-thickness ratio on the neck. This design provides added comfort and agility when racing through the frets, while not fatiguing your digits.</p>
</div>', 399.0000, NULL, N'Bass1.jpg', NULL, NULL, NULL, N'<ul>
<li>7-piece walnut/maple neck</li>
<li>Through-body construction</li>
<li>T-Pro humbucking pickups</li>
</ul>', 34)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (34, 1, N'Epiphone Thunderbird Classic-IV PRO Electric Bass Guitar', N'<h2 class=''heading''>
<p>A rock classic returns featuring Gibson TB-Plus humbuckers with ceramic magnets</p>
</h2><div class=''details''>
<p class=''description''>The Thunderbird IV was first introduced in 1963 and instantly became one of rock’s most recognizable bass guitar designs. For almost five decades, the Thunderbird has powered artists as varied as Nikki Sixx, The Who, Kings of Leon, Cheap Trick, Lynyrd Skynyrd, Steely Dan and The Silversun Pickups. Turn on your radio and you’ll hear a Thunderbird bass. Now, Epiphone takes all the vintage mojo of the original Thunderbird IV and sends it flying into the future with Gibson TB Plus humbuckers with ceramic magnets and all the ’Bird’s original styling and features intact. <br><br>The Thunderbird IV was one of the most radical designs to come out of the Gibson and Epiphone Kalamazoo factory in the early ’60s, thanks to legendary automotive designer Ray Dietrich, who was asked to put a new twist on solidbody guitars and basses. The sound of the Thunderbird IV was as cutting edge as its design and now the Thunderbird Classic-IV PRO returns with all of Epiphone’s first-class quality and a lifetime guarantee, but without the hassles of owning (or hiding) a vintage instrument.</p>
</div>', 499.0000, NULL, N'Bass2.jpg', NULL, NULL, NULL, N'<ul>
<li>Gibson TB Plus humbuckers</li>
<li>Historic 60’s era Thunderbird profile</li>
<li>Mahogany body wings</li>
<li>7-ply Mahogany/walnut neck</li>
<li>Through-neck neck joint</li>
<li>Rosewood fingerboard with pearloid dot inlays</li>
<li>20-Medium jumbo, nickel/silver alloy frets</li>
<li>Classic style 3-point adjustable Flush-Mount, fully adjustable bridge</li>
<li>Neck volume, Bridge volume and Master tone controls</li>
<li>Black die-cast tuners with 17:1 ratio</li>
<li>Black hardware</li>
<li>Black TopHats control knobs with chrome metal inserts</li>
</ul>', 25)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (35, 1, N'Epiphone Jack Casady Signature Bass Guitar', N'<h2 class=''heading''>
<p>A bass bound for glory.</p>
</h2><div class=''details''>
<p class=''description''>Deep, articulate tone and classic semi-hollowbody style are the hallmarks of the Epiphone Jack Casady Signature Bass. The maple body, mahogany center block, set maple neck, and rosewood fretboard conspire to create a sweet-playing bass you won''t want to put down. Has a single low-impedance JCB-1 humbucker with VariTone control, as well as traditional volume and tone knobs. Pearloid trapezoid fingerboard inlays, body binding, and Jack''s signature across the headstock set it apart.</p>
</div>', 699.0000, NULL, N'Bass3.jpg', NULL, NULL, NULL, N'<ul>
<li>Maple body</li>
<li>Mahogany block under bridge</li>
<li>Set maple neck</li>
<li>Rosewood fretboard</li>
<li>Low-impedance JCB-1 humbucker</li>
<li>VariTone control</li>
<li>Volume and tone knobs</li>
<li>Pearloid trapezoid inlays</li>
<li>Body binding</li>
</ul>', 78)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (36, 1, N'Epiphone Zenith Acoustic/Electric Bass', N'<h2 class=''heading''>
<p>Classic design and elegant style.</p>
</h2><div class=''details''>
<p class=''description''>Since 1873, Epiphone has been designing and making some of the most innovative and classic instruments including the Emperor, Broadway, Casino, Wilshire, Coronet and WildKat - just to name a few. The Epiphone Zenith Bass features classic design styling with an elegant profile that pays tribute to great Jazz guitars of the past. Its gypsy style cutaway provides easy access to all frets while its f-holes and upright-style tailpiece pay homage to Epiphone''s acoustic basses of the past.</p>
</div>', 799.0000, NULL, N'Bass4.jpg', NULL, NULL, NULL, N'<ul>
<li>NanoFlex and NanoMag pickups</li>
<li>Black hardware</li>
<li>34'' scale</li>
<li>1-11/16'' nut width</li>
<li>5-piece hard maple and walnut neck</li>
<li>14'' radius</li>
<li>Rosewood fingerboard with dot markers</li>
<li>Chambered mahogany body</li>
</ul>', 42)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (37, 2, N'Fender American Deluxe Jazz Bass V 5-String Electric', N'<h2 class=''heading''>
<p>Updated electronics, vintage bridge, and a rugged alder body enhance one of the best Fender Jazz basses yet.</p>
</h2><div class=''details''>
<p class=''description''>The 5-string Fender American Deluxe Jazz Bass V delivers the modern features that bassists demand in a sleek, familiar electric bass design. An updated preamp circuit brings deeply exhilarating active and passive tone. Powerful-yet-quiet N3 Noiseless pickups present more clean headroom, extended EQ range, and tighter low-end response. The tonal options are vast, thanks to the active/passive toggle switch and passive tone control. These amazing electronics effortlessly handle the low B string with impeccable tone and seismic power. The classic Jazz Bass block fretboard inlays and binding present the Fender American Deluxe V bass in a rightfully classy light.</p>
</div>', 1799.9900, NULL, N'Bass5.jpg', NULL, NULL, NULL, N'<ul>
<li>Body: Alder with contoured heel</li>
<li>Finish: Gloss urethane</li>
<li>Neck: Maple, modern ''C'' shape, tinted, gloss polyurethane finish</li>
<li>Fingerboard: Rosewood</li>
<li>Frets 21, medium-jumbo</li>
<li>Hardware: Chrome</li>
<li>Tuning keys: Fender lightweight vintage-style tuners</li>
<li>Bridge: High Mass Vintage (HMV)</li>
<li>Pickguard: 3-ply black/white/black</li>
<li>Pickups: N3 Noiseless Pickups</li>
<li>Pickup switching: Pan pot/blend control</li>
<li>Controls: Master volume, pan pot (pickup selector), treble boost/cut, midrange boost/cut, bass boost/cut, active/passive mini toggle</li>
<li>Strings: Fender NPS .045 to 0.105</li>
</ul>', 46)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (38, 2, N'Fender Deluxe P Bass Special 4-String Bass', N'<h2 class=''heading''>
<p>A deluxe version of Fender''s best-of-both-worlds bass.</p>
</h2><div class=''details''>
<p class=''description''>The Fender Deluxe P Bass Special 4-String Bass has a Jazz neck, a P Bass body, and a P/J combination pickup array with Vintage Noiseless pickups and active electronics. The P body is a little smaller and lighter, the J neck slimmer and faster, and the combined pickups give you just that much more to play with. A gold vinyl pickguard sets the Deluxe P Bass apart visually, and its satin-finish neck feels worn in from the get-go.</p>
</div>', 699.9900, NULL, N'Bass6.jpg', NULL, NULL, NULL, N'<ul>
<li>Jazz neck</li>
<li>P Bass body</li>
<li>Vintage Noiseless Pickups (J bridge pickup, P center pickup)</li>
<li>Active electronics</li>
</ul>', 27)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (39, 2, N'Fender American Vintage ''64 Jazz Bass', N'<h2 class=''heading''>
<p>A re-imagined version of a classic Fender bass restored to the way it was when it was introduced all those years ago.</p>
</h2><div class=''details''>
<p class=''description''>The American Vintage series introduces an all-new lineup of original-era model year guitars and basses that bring Fender history and heritage to authentic and exciting new life. With key features and pivotal design elements spanning the mid-1950s to the mid-1960s, new American Vintage series instruments delve deep into Fender''s roots--expertly preserving an innovative U.S. guitar-making legacy and vividly demonstrating like never before that Fender not only knows where it''s going, but also remembers where it came from.</p>
</div>', 2199.9900, NULL, N'Bass7.jpg', NULL, NULL, NULL, N'<ul>
<li>Alder body</li>
<li>Fender flash coat lacquer body and neck finish </li>
<li>Maple, ''C''-shape neck </li>
<li>Round-laminated rosewood fingerboard</li>
<li>20, Vintage-style frets </li>
<li>Bone string nut</li>
<li>Vintage-style heel adjust truss rod</li>
<li>American Vintage ''64 Jazz Bass single-coil (bridge and neck) pickups</li>
<li>American Vintage reverse open-gear tuning machines </li>
<li>Volume 1. (Middle Pickup), Volume 2. (Bridge Pickup), Master Tone controls</li>
<li>Black plastic control knobs </li>
<li>American Vintage Bass bridge with threaded steel ''barrel'' saddles</li>
<li>Nickel/Chrome hardware</li>
<li>4-Ply Tortoiseshell pickguard on 3-Color Sunburst and Olympic White; 3-Ply White/Black/White pickguard on Placid Blue with matching headstock and Black</li>
</ul>', 85)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (40, 2, N'Fender Standard Precision Bass Guitar', N'<h2 class=''heading''>
<p>Today''s take on the bass that started it all.</p>
</h2><div class=''details''>
<p class=''description''>The Fender Standard Precision Bass Guitar delivers the sound, look, and feel today''s bass players demand. The Fender Standard Precision features that classic P-Bass old-school design. Each Precision Bass boasts contemporary features and refinements that make it an excellent value. The alder body, rosewood fretboard with jumbo frets, and split single-coil pickup deliver the fat tone that has driven rhythm sections for decades.</p>
</div>', 579.9900, NULL, N'Bass8.jpg', NULL, NULL, NULL, N'<ul>
<li>Series: Standard series </li>
<li>Body: Alder </li>
<li>Neck: Maple, modern C shape, gloss urethane finish </li>
<li>Fingerboard: Rosewood </li>
<li>9-1/2'' Radius (241 mm) </li>
<li>Frets: 20 Medium-jumbo frets </li>
<li>Pickups: 1 Standard Precision Bass split single-coil pickup (Mid) </li>
<li>Controls: Volume, Tone </li>
<li>Bridge: Standard vintage style with single groove saddles </li>
<li>Machine heads: Standard </li>
<li>Hardware: Chrome </li>
<li>Pickguard: 3-Ply Parchment </li>
<li>Scale Length: 34'' (864 mm) </li>
<li>Width at Nut: 1-5/8'' (41.3 mm) </li>
<li>Unique features: Knurled chrome P Bass knobs, Fender transition logo </li>
<li>Strings: Fender Super Bass 7250ML, NPS </li>
</ul>', 64)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (41, 3, N'Gibson Limited Run Flying V Electric Bass Guitar', N'<h2 class=''heading''>
<p>A bass that finally brings the Flying V body form to the deep end where it belongs.</p>
</h2><div class=''details''>
<p class=''description''>Ever since its introduction in 1958 as part of Gibson''s Modernist Series, the Flying V guitar has been an icon of heavy rock tones and alternative looks. Not satisfied with letting the six-stringers have all the fun, Gibson USA has configured this rocket-age styling into a mean and meaty package for the bassist in search of a scorching low-end alternative: the Flying V Bass. With its 30-1/2'' scale length, which is still somewhat longer than the conventional guitar, this bass is perfect for players with shorter arms and smaller hands, beginners and students, or those who are more familiar with six-string guitar scale lengths. But it''s also a fully professional instrument for the highest-flying touring or studio musician. Two of the world''s most famous bassists, Paul McCartney and Jack Bruce, played short-scale basses at the most influential points of their careers, and countless others have used them to make legendary rock, pop and blues recordings. The Flying V Bass brings weighty tones and easy playability to a package styled for the most radical rock that the 21st century has to offer, and suitable for every variety of music.</p>
</div>', 899.0000, NULL, N'Bass9.jpg', NULL, NULL, NULL, N'<ul>
<li>Body Type: Flying V</li>
<li>Body Wood: Mahogany</li>
<li>Scale Length: 30-1/2''</li>
<li>Neck Joint: Set</li>
<li>Neck Wood: Mahogany</li>
<li>Fretboard: Rosewood</li>
<li>Neck Shape: Slim Taper</li>
<li># frets: 20</li>
<li>Nut Width: 1.695''</li>
<li>Fretboard Radius: 12</li>
<li>Bridge: Fixed</li>
<li>Pickup Bridge: TB Plus Ceramic Humbucker</li>
<li>Pickup Neck: TB Plus Ceramic Humbucker</li>
<li>Controls: 2 Volume, 1 Tone</li>
<li>Tuners: Grover</li>
<li>Hardware color: Chrome</li>
<li>Pickup selector: 3-Way</li>
<li>Limited run</li>
</ul>', 63)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (42, 3, N'Gibson Les Paul Electric Bass', N'<h2 class=''heading''>
<p>Classic style, dual TB pickups and a ''50s rounded taper neck.</p>
</h2><div class=''details''>
<p class=''description''>The Gibson Les Paul Electric Bass is the perfect weekend warrior with the right mix of features and value. The mahogany body comes in a choice of finishes and has Gibson''s traditional weight relief to maintain sustain while making the bass a little lighter. The set mahogany neck comes in a popular shape, the ''50s rounded taper, and has a rosewood fingerboard. Dual chrome covered TB Plus pickups and a tune-o-matic bridge complete the list of features on this road-worthy bass.<br></p>
</div>', 1599.0000, NULL, N'Bass10.jpg', NULL, NULL, NULL, N'<ul>
<li>Mahogany body with “Traditional” weight relief</li>
<li>Maple top</li>
<li>Mahogany neck with ‘50s Rounded taper and a larger headstock</li>
<li>Bound rosewood fingerboard with Trapezoid Inlays, 34” Scale and 20 Frets</li>
<li>Chrome Bass tune-o-matic bridge</li>
<li>Grover bass tuners</li>
<li>Dual chrome covered TB Plus pickups</li>
<li>Cream plastic, Amber Top Hat knobs</li>
<li>Includes Gibson hardshell case</li>
</ul>', 86)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (43, 3, N'Gibson Thunderbird IV Electric Bass Guitar', N'<h2 class=''heading''>
<p>Big, bold, bass from a stunning classic.</p>
</h2><div class=''details''>
<p class=''description''>The Gibson Thunderbird IV Electric Bass Guitar debuted in 1963 with the original Firebird guitar series. The most highly respected of all Gibson basses, it was reissued in 1987 with original reverse body styling and TB Plus ceramic magnet humbuckers. Its thunderous bass output is enhanced by a 9-ply mahogany-and-walnut neck that extends through the body. Mahogany wings, a rosewood fretboard, and black chrome hardware provide stunning dark looks. 34'' scale, black top-hat knobs with inserts, Schaller tuners, and 3-way adjustable tailpiece. Gibson includes their own hardshell case with the Thunderbird IV.</p>
</div>', 1499.0000, NULL, N'Bass11.jpg', NULL, NULL, NULL, N'<ul>
<li>9-ply mahogany and walnut neck</li>
<li>Through-body neck</li>
<li>Mahogany wings</li>
<li>Rosewood fretboard</li>
<li>Black chrome hardware</li>
<li>34'' scale</li>
<li>TB Plus ceramic magnet humbuckers</li>
<li>Black top-hat knobs with inserts</li>
<li>Schaller tuners</li>
<li>3-way adjustable tailpiece</li>
</ul>', 44)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (44, 3, N'Gibson Krist Novoselic RD Bass Guitar', N'<h2 class=''heading''>
<p>A bass monster for one of modern rock’s most distinctive players.</p>
</h2><div class=''details''>
<p class=''description''>The Krist Novoselic Signature RD Bass begins with a body and glued-in neck of solid maple, a tonewood known for its ability to add punch, clarity and sustain to the sound of any guitar. It is shaped in the image of the RD guitars and basses first introduced by Gibson USA in the 1970s, with a rounded, offset body style reminiscent of the iconic Gibson Firebird guitar and Thunderbird bass. The sustain and thundering resonance of the Krist Novoselic Signature RD Bass are further enhanced by a strings-through-body design with solid three-point bridge, and a gently back-angled headstock to maintain optimum string pressure in the PLEK-cut slots of a Corian nut. Meanwhile, formidable Grover&trade; ''Shamrock-key'' tuners keep your pitch tight and true, however hard you hit it. The comfortably rounded neck profile with a depth of .860'' at the 1st fret and .960'' at the 12th is paired with an exotic obeche fingerboard with 20 frets and a 12-inch radius, all crafted to enhance a playing experience that is every bit the equal of the superior tone and construction of this outstanding bass. <br><br>To optimize the power, punch and clarity of the Krist Novoselic Signature RD Bass'' natural resonance and sustain, Gibson USA uses a pair of Seymour Duncan&trade; Bass Lines STK-J2n and STK-J2b ''Hot Stack'' pickups. Designed to offer the precision and harmonic richness of single-coil pickups, with the noise-canceling properties of humbuckers, these Seymour Duncans provide unparalleled tones and impressive versatility. An independent volume control for each enables you to dial in your preferred balance of bridge and neck pickup for any playing situation, while a master tone control allows fine-tuning of your instrument''s voice. Add it all up, and the Krist Novoselic Signature RD Bass is a total monster of versatility and performance for today''s master of the low-end.</p>
</div>', 1399.0000, NULL, N'Bass12.jpg', NULL, NULL, NULL, N'<ul>
<li>Handcrafted in Nashville, TN USA</li>
<li>Distinctive body shape</li>
<li>All Maple tone woods for added punch</li>
<li>Set neck and string-through design offer maximum sustain</li>
<li>Angled headstock maintains optimum string pressure</li>
</ul>', 46)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (45, 4, N'Ibanez SR500 Soundgear 4-String Bass', N'<h2 class=''heading''>
<p>Sleek, sexy, and sonically versatile—the SR500 Bass is at home in the studio and on any stage.</p>
</h2>
<div class=''details''>
<p class=''description''>The Ibanez SR500 Bass is an incredibly well-crafted and equipped bass for its price. It features a slim, fast SR4 5-piece wenge and bubinga neck on a sculpted mahogany body. The rosewood fretboard is fitted with medium frets and has oval abalone markers, while the maple fretboard version boasts black dot inlays. The Accu-Cast B20 bridge offers easier string changes, single-screw saddle and height adjustment, and improved stability. For pickups it sports 2 Bartonlini MK1s which integrate perfectly with its active Bartolini MK1 3-band EQ. It''s a bass that will excel in any style of music.</p>
</div>', 599.9900, NULL, N'Bass13.jpg', NULL, NULL, NULL, N'<ul>
<li>Thin, fast, 5-piece SR4 neck crafted of wenge and bubinga</li>
<li>Sculpted mahogany body</li>
<li>Rosewood fretboard with oval abalone markers or maple fretboard with black dot inlays</li>
<li>24 medium frets</li>
<li>Accu-Cast B20 bridge</li>
<li>Bartolini MK1-4 neck pickup</li>
<li>Bartolini MK1-4 bridge pickup</li>
<li>Black chrome hardware with black pickups</li>
</ul>', 67)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (46, 4, N'Ibanez BTB776PB 6-String Bass Guitar', N'<h2 class=''heading''>
<p>Fat sound, precise response, and extended range.</p>
</h2>
<div class=''details''>
<p class=''description''>The 6-string BTB776PB bass guitar from Ibanez features a gorgeous mahogany body and poplar burl top. Its MK-2 pickups, neck-through construction, and extremely deep cutaways provide huge tones, sustain, and upper-fret accessibility that BTB players have come to love.</p>
</div>', 999.9900, NULL, N'Bass14.jpg', NULL, NULL, NULL, N'<ul>
<li>Neck: 5-piece Maple/Bubinga</li>
<li>Body: Mahogany body/ Poplar Burl top</li>
<li>Frets: Medium frets</li>
<li>Fingerboard: Rosewood</li>
<li>Inlay: Abalone dot inlay</li>
<li>Bridge: Mono-Rail II bridge</li>
<li>Neck pickup: Bartolini MK2-6-F passive humbucking</li>
<li>Bridge pickup: Bartolini MK2-6-R passive humbucking</li>
<li>EQ: Bartolini MK-2 (BTB)</li>
<li>Controls: Volume, Bass Boost/Cut, Balance, Mid Boost/Cut, Treble Boost/Cut</li>
</ul>', 67)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (47, 4, N'Ibanez GSR200 4-String Bass', N'<h2 class=''heading''>
<p>Highly recommended as a first bass.</p>
</h2>
<div class=''details''>
<p class=''description''>The Ibanez GSR200 4-String Bass is a hot little bass with nice balance and feel for a very friendly price. Features an agathis body, super comfy one-piece maple neck, rosewood fretboard, pearl dot inlay, split-coil and single-coil pickup array, active EQ with PHAT-II Bass Boost, chrome hardware, a fully adjustable bridge, and medium frets.</p>
</div>', 199.9900, NULL, N'Bass15.jpg', NULL, NULL, NULL, N'<ul>
<li>Agathis body</li>
<li>One-piece maple neck</li>
<li>Rosewood fretboard</li>
<li>Pearl dot inlay</li>
<li>Split-coil and single-coil pickup combination</li>
<li>Active EQ with PHAT-II Bass Boost</li>
<li>Chrome hardware</li>
<li>Fully adjustable bridge</li>
</ul>', 24)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (48, 4, N'Ibanez SR305 5-String Bass Guitar', N'<h2 class=''heading''>
<p>Pro features for a starter price!</p>
</h2>
<div class=''details''>
<p class=''description''>The 5-string version of the SR300, this Ibanez bass guitar from the SR Series is inexpensive enough for anyone who wants to get into playing bass, but doesn''t want an axe that sounds like a cheap starter instrument. First, the SR305 bass has a 5-piece SR5 maple/rosewood neck; light, agathis body with smooth rounded edges; and an extreme, lower cutaway for easier access to the bottom frets. The EXF-4 pickups and an upgraded 3-band Style Sweeper EQ give you a great overall sound no matter what style you''re into.</p>
</div>', 399.9900, NULL, N'Bass16.jpg', NULL, NULL, NULL, N'<ul>
<li>5 strings</li>
<li>5-piece SR5 maple/rosewood neck</li>
<li>Agathis body</li>
<li>Medium frets</li>
<li>B105 bridge</li>
<li>EXF-5 neck pickup</li>
<li>EXF-5 bridge pickup</li>
<li>Style Sweeper EQ</li>
</ul>', 75)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (49, 2, N'Fender 150L Original Pure Nickel Electric Strings - Light', N'<h2 class=''heading''>
<p>Reap the benefits of time-tested Fender quality.</p>
</h2>
<div class=''details''>
<p class=''description''>
Fender''s 150 series Pure Nickel strings deliver a smooth feel with reduced finger noise. The tone is rich and warm with abundant harmonics. These Fender electric guitar strings are perfect for blues, jazz, and classic rock.
<br>
<br>
Gauges: 09-11-16-24-32-42.
<br>
<br>
Fender has been an instrument of choice for many of the greatest musicians since 1946. Fender''s popularity through the generations is a result of their design innovations and commitment to quality. These light guitar strings reflect this legacy and are worthy of the Fender name. Attention to detail and the finest materials ensure that you will get the best feeling and greatest sounding strings, set after set.
</p>
</div>', 4.1900, NULL, N'String1.jpg', NULL, NULL, NULL, N'<ul>
<li>Warm, clear tone</li>
<li>Long-lasting</li>
</ul>', 87)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (50, 2, N'Fender 3250L Nickel-Plated Steel Bullet-End Electric Guitar Strings - Light', N'<h2 class=''heading''>
<p>Invest in precious metal to reap the benefits of dynamic musical tones.</p>
</h2>
<div class=''details''>
<p class=''description''>
Gauges: 09-11-16-24-32-42.
<br>
<br>
Fender''s Super Bullets Nickel-Plated Steel (NPS) electric guitar strings combine the high output and dynamic sound of steel with the smooth feel of nickel. The Fender 3250L strings are perfect for rock and other styles of music where the guitar needs to cut through. The patented bullet-end creates a sonic coupling between the string and bridge block, for rock-solid tuning, stability, and increased sustain.
<br>
<br>
Fender has been an instrument of choice for many of the greatest musicians since 1946. Fender''s popularity through the generations is a result of their design innovations and commitment to quality. These light guitar strings reflect this legacy and are worthy of the Fender name. Attention to detail and the finest materials ensure that you will get the best feeling and greatest sounding strings, set after set.
</p>
</div>', 4.4600, NULL, N'String2.jpg', NULL, NULL, NULL, N'<ul>
<li>Bullet-ended Strings</li>
<li>Long-lasting</li>
</ul>', 91)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (51, 2, N'Fender 350L Stainless Steel Electric Guitar Strings - Light', N'<h2 class=''heading''>
<p>Reap the benefits of time-tested Fender quality</p>
</h2>
<div class=''details''>
<p class=''description''>
Fender''s Stainless steel 350s electric guitar strings deliver the high output and aggressive tone that''s ideal fro heavier styles, such as hard rock and heavy metal. The stainless steel construction also creates a string with an increased lifespan.
<br>
<br>
Gauges: 09-11-16-24-32-42.
<br>
<br>
Fender has been an instrument of choice for many of the greatest musicians since 1946. Fender''s popularity through the generations is a result of their design innovations and commitment to quality. These steel strings reflect this legacy and are worthy of the Fender name. Attention to detail and the finest materials ensure that you will get the best feeling and greatest sounding guitar strings, set after set.
<br>
<br>
</p>
</div>', 3.9900, NULL, N'String3.jpg', NULL, NULL, NULL, N'<ul>
<li>Aggressive tone</li>
<li>Long-lasting</li>
</ul>', 28)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (52, 2, N'Fender 250M Super 250 Nickel-Plated Steel Electric Guitar Strings - Medium', N'<h2 class=''heading''>
<p>Reap the benefits of time-tested Fender quality</p>
</h2>
<div class=''details''>
<p class=''description''>
Fender''s Super 250 Nickel-Plated Steel (NPS) electric guitar strings combine the high output and dynamic sound of steel with the smooth feel of nickel; perfect for rock and other styles of music where the guitar needs to cut through.
<br>
<br>
Gauges: 11-14-18-28-38-49.
<br>
<br>
Fender has been an instrument of choice for many of the greatest musicians since 1946. Fender''s popularity through the generations is a result of their design innovations and commitment to quality. These guitar strings reflect this legacy and are worthy of the Fender name. Attention to detail and the finest materials ensure that you will get the best feeling and greatest sounding strings, set after set.
<br>
<br>
</p>
</div>', 4.5900, NULL, N'String4.jpg', NULL, NULL, NULL, N'<ul>
<li>High-output tone</li>
<li>Long-lasting</li>
</ul>', 36)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (53, 2, N'Fender 9120 Nylon Tapewound Bass Strings', N'<h2 class=''heading''>
<p>Nylon tapewound for an upright bass sound.</p>
</h2>
<div class=''details''>
<p class=''description''>
Gauges: 58-72-92-110.
<br>
<br>
Fender has been an instrument of choice for many of the greatest musicians since 1946. Fender''s popularity through the generations is a result of their design innovations and commitment to quality. These electric bass strings reflect this legacy and are worthy of the Fender name. Attention to detail and the finest materials ensure that you will get the best feeling and greatest sounding replacement strings, set after set.
<br>
<br>
Fender 9120s Nylon Tapewound bass strings with a nylon wrapping over the metal core deliver a warm, smooth tone like the thump of an upright bass.
</p>
</div>', 20.4700, NULL, N'String5.jpg', NULL, NULL, NULL, N'<ul>
<li>Tapewound</li>
<li>Long-lasting</li>
</ul>', 85)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (54, 2, N'Fender 9050L Stainless Steel Flatwound Long Scale Bass Strings - Light', N'<h2 class=''heading''>
<p>Warm tone and smooth fingering from a light-gauge, flatwound string.</p>
</h2>
<div class=''details''>
<p class=''description''>
Gauges: 45-60-80-100.
<br>
<br>
Fender has been an instrument of choice for many of the greatest musicians since 1946. Fender''s popularity through the generations is a result of their design innovations and commitment to quality. These flatwound bass strings reflect this legacy and are worthy of the Fender name. Attention to detail and the finest materials ensure that you will get the best feeling and greatest sounding replacement strings, set after set.
<br>
<br>
Fender Stainless 9050L bass strings are flatwound for a warm, rich tone.
</p>
</div>', 20.4900, NULL, N'String6.jpg', NULL, NULL, NULL, N'<ul>
<li>flatwound</li>
<li>Long-lasting</li>
</ul>', 26)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (55, 5, N'Elixir Nanoweb Medium Electric Bass Strings', N'<h2 class=''heading''>
<p>Combat finger grunge!</p>
</h2>
<div class=''details''>
<p class=''description''>
Elixir Strings are covered with an ultra-thin, space age polymer tube that contacts the string on the tops of the windings only. This leaves the all-important winding-to-winding-to-core space free from the fear of the enemies of tone!
<br>
<br>
Nanoweb coating permits strings with the feel, bright tone, and punch of traditional strings. Elixirs are the first major innovation in strings in over 40 years, and deliver what they promise: great tone and long life no matter what you put them on or how often you play. Most players report that Elixir Strings keep sounding great 3 to 5 times longer than ordinary strings.
<br>
<br>
Gauges: 45-65-85-105.
</p>
</div>', 38.9900, NULL, N'String7.jpg', NULL, NULL, NULL, N'<ul>
<li>Ultra-thin/li>
<li>Nanoweb coating</li>
</ul>', 43)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (56, 5, N'Elixir Nanoweb Heavy Electric Bass Strings', N'<h2 class=''heading''>
<p>Combat finger grunge!</p>
</h2>
<div class=''details''>
<p class=''description''>
Gauges: 50-70-85-105.
<br>
<br>
Elixirs are the first major innovation in strings in over 40 years, and they deliver what they promise: great tone and long life no matter what you put them on or how often you play. Nanoweb coating permits strings with the feel, bright tone, and punch of traditional strings.
<br>
<br>
Elixir Heavey Electric Bass Strings are covered with an ultra-thin, space-age polymer tube that contacts the string on the tops of the windings only. This leaves the all-important winding-to-winding-to-core space free from the fear of the enemies of tone!
<br>
<br>
Most players report that Elixir Strings keep sounding great 3 to 5 times longer than ordinary strings.
</p>
</div>', 38.9900, NULL, N'String8.jpg', NULL, NULL, NULL, N'<ul>
<li>Ultra-thin</li>
<li>Nanoweb coating</li>
</ul>', 68)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (57, 2, N'Fender 130 Clear/Silver Classical Nylon Guitar Strings - Ball End', N'<h2 class=''heading''>
<p>Reap the benefits of time-tested Fender quality</p>
</h2>
<div class=''details''>
<p class=''description''>
Gauges: 28-29-32-35-40-43.
<br>
<br>
Fender has been an instrument of choice for many of the greatest musicians since 1946. Fender''s popularity through the generations is a result of their design innovations and commitment to quality. These replacement guitar strings reflect this legacy and are worthy of the Fender name. Attention to detail and the finest materials ensure that you will get the best feeling and greatest sounding strings, set after set.
<br>
<br>
Fender nylon folk strings emit superior warmth, clarity, and sustain. The pure silver windings offer a smooth feel with reduced finger noise.
</p>
</div>', 6.7900, NULL, N'String9.jpg', NULL, NULL, NULL, N'<ul>
<li>Clear, warm tones</li>
<li>Pure silver windings</li>
</ul>', 42)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (58, 2, N'Fender 130 Clear/Silver Classical Nylon Guitar Strings - Ball End', N'<h2 class=''heading''>
<p>Reap the benefits of time-tested Fender quality</p>
</h2>
<div class=''details''>
<p class=''description''>
Gauges: 28-29-32-35-40-43.
<br>
<br>
Fender has been an instrument of choice for many of the greatest musicians since 1946. Fender''s popularity through the generations is a result of their design innovations and commitment to quality. These replacement guitar strings reflect this legacy and are worthy of the Fender name. Attention to detail and the finest materials ensure that you will get the best feeling and greatest sounding strings, set after set.
<br>
<br>
Fender nylon folk strings emit superior warmth, clarity, and sustain. The pure silver windings offer a smooth feel with reduced finger noise.
</p>
</div>', 4.9900, NULL, N'String10.jpg', NULL, NULL, NULL, N'<ul>
<li>Clear, warm tones</li>
<li>Superior warmth and clarity</li>
</ul>', 57)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (59, 5, N'Elixir Light Nanoweb Acoustic Guitar Strings', N'<h2 class=''heading''>
<p>Combat finger grunge!</p>
</h2>
<div class=''details''>
<p class=''description''>
Gauges: 12-16-24-32-42-53.
<br>
<br>
Elixir Acoustic Guitar Strings are covered with an ultrathin, space-age polymer tube that contacts the string on the tops of the windings only. This leaves the all-important winding-to-winding-to-core space free from the fear of the enemies of tone!
<br>
<br>
Elixir Nanoweb coating permits acoustic guitar strings with the feel, bright tone, and punch of traditional strings. Elixirs are the first major innovation in guitar strings in over forty years and they deliver what they promise: great tone and long life no matter what you put them on or how often you play.
<br>
<br>
Most players report that Elixir Strings keep sounding great 3 to 5 times longer than ordinary strings.
</p>
</div>', 11.9800, NULL, N'String11.jpg', NULL, NULL, NULL, N'<ul>
<li>Nanoweb coating</li>
<li>Untrathin polymer coating</li>
</ul>', 63)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (60, 5, N'Elixir Light Nanoweb Acoustic Guitar Strings', N'<h2 class=''heading''>
<p>Combat finger grunge!</p>
</h2>
<div class=''details''>
<p class=''description''>
Gauges: 12-16-24-32-42-53.
<br>
<br>
Elixir Acoustic Guitar Strings are covered with an ultrathin, space-age polymer tube that contacts the string on the tops of the windings only. This leaves the all-important winding-to-winding-to-core space free from the fear of the enemies of tone!
<br>
<br>
Elixir Nanoweb coating permits acoustic guitar strings with the feel, bright tone, and punch of traditional strings. Elixirs are the first major innovation in guitar strings in over forty years and they deliver what they promise: great tone and long life no matter what you put them on or how often you play.
<br>
<br>
Most players report that Elixir Strings keep sounding great 3 to 5 times longer than ordinary strings.
</p>
</div>', 12.5200, NULL, N'String12.jpg', NULL, NULL, NULL, N'<ul>
<li>Nanoweb coating</li>
<li>Untrathin polymer coating</li>
</ul>', 67)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (61, 5, N'Elixir Light Nanoweb Electric Guitar Strings', N'<h2 class=''heading''>
<p>Elixir Strings with Nanoweb coating last longer and sound great!</p>
</h2>
<div class=''details''>
<p class=''description''>
Gauges 10-13-17-26-36-46.
<br>
<br>
The engineers at Elixir took their original Polyweb coating and shrunk it down to Nanoweb size. The result? Brighter-sounding electric guitar strings that last 3 to 5 times longer than regular strings.
<br>
<br>
In addition to an ultrathin grunge-resistant tubular coating on the wound strings, Nanowebs now have a revolutionary Anti-Rust plating on the plain strings to extend their life too.
<br>
<br>
Elixirs are the first major innovation in guitar strings in over forty years, and they deliver what they promise: great tone and long life no matter what you put them on or how often you play.
<br>
</p>
</div>', 7.6300, NULL, N'String13.jpg', NULL, NULL, NULL, N'<ul>
<li>Nanoweb coating</li>
<li>Anti-rust plating</li>
</ul>', 15)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (62, 5, N'Elixir Nanoweb Heavy Electric Guitar Strings', N'<h2 class=''heading''>
<p>Combat finger grunge!</p>
</h2>
<div class=''details''>
<p class=''description''>
Gauges: 12-16-24*w*-32-42-52.
<br>
<br>
Nanoweb coating permits electric guitar strings with the feel, bright tone, and punch of traditional strings. Elixirs are the first major innovation in guitar strings in over forty years, and they deliver what they promise: great tone and long life no matter what you put them on or how often you play.
<br>
<br>
Elixir Plated Plain Steel Strings are covered with an ultra-thin, space-age polymer tube that contacts the string on the tops of the windings only. This leaves the all-important winding-to-winding-to-core space free from the fear of the enemies of tone!
<br>
<br>
In addition to an ultra-thin grunge-resistant tubular coating on the wound strings, Nanowebs now have a revolutionary Anti-Rust plating on the plain strings to extend their life too.
<br>
<br>
<br>
Most players report that Elixir Strings keep sounding great 3 to five times longer than ordinary strings.
</p>
</div>', 10.9900, NULL, N'String14.jpg', NULL, NULL, NULL, N'<ul>
<li>Nanoweb coating</li>
<li>Anti-rust plating</li>
</ul>', 75)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (63, 5, N'Elixir Light Polyweb Electric Guitar Strings', N'<h2 class=''heading''>
<p>Combat string grunge!</p>
</h2>
<div class=''details''>
<p class=''description''>
Gauges 10-13-17-26-36-46.
<br>
<br>
Elixir Electric Guitar Strings are covered with an ultrathin, space age polymer tube that contacts the string on the tops of the windings only. This leaves the all-important winding-to-winding-to-core space free from the fear of the enemies of tone! No more gunk and goo. No more DNA evidence. No kidding.
<br>
<br>
In addition to an ultrathin grunge-resistant tubular coating on the wound strings, Polywebs now have a revolutionary Anti-Rust plating on the plain strings to extend their life too.
<br>
<br>
Elixirs are the first major innovation in guitar strings in over 40 years, and deliver what they promise: great tone and long life no matter what you put them on or how often you play.
</p>
</div>', 10.9900, NULL, N'String15.jpg', NULL, NULL, NULL, N'<ul>
<li>Polyweb coating</li>
<li>Anti-rust plating</li>
</ul>', 67)
INSERT [dbo].[Product] ([ProductID], [BrandID], [Name], [Description], [Price], [Thumbnail], [Image], [PromoFront], [PromoDept], [Rating], [Features], [Stock]) VALUES (64, 5, N'Elixir Nanoweb Baritone Electric Guitar Strings', N'<h2 class=''heading''>
<p>Combat finger grunge!</p>
</h2>
<div class=''details''>
<p class=''description''>
Gauges: 12-16-22-38-52-68.
<br>
<br>
Elixir Electric Guitar Strings are the first major innovation in guitar strings in over forty years, and they deliver what they promise: great tone and long life no matter what you put them on or how often you play. Elixir''s Nanoweb coating permits strings with the feel, bright tone, and punch of traditional strings.
<br>
<br>
Elixir Strings are covered with an ultra-thin, space-age polymer tube that contacts the string on the tops of the windings only. This leaves the all-important winding-to-winding-to-core space free from the fear of the enemies of tone!
<br>
<br>
In addition to an ultra-thin grunge-resistant tubular coating on the wound strings, Nanowebs now have a revolutionary Anti-Rust plating on the plain strings to extend their life too.
<br>
<br>
Most players report that Elixir Strings keep sounding great 3 to five times longer than ordinary strings.
</p>
</div>', 10.9900, NULL, N'String16.jpg', NULL, NULL, NULL, N'<ul>
<li>Nanoweb coating</li>
<li>Anti-rust plating</li>
</ul>', 87)
SET IDENTITY_INSERT [dbo].[Product] OFF
/****** Object:  StoredProcedure [dbo].[OrderUpdate]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderUpdate]
(@OrderID INT,
 @DateCreated SMALLDATETIME,
 @DateShipped SMALLDATETIME = NULL,
 @Verified BIT,
 @Completed BIT,
 @Canceled BIT,
 @Comments VARCHAR(200),
 @CustomerName VARCHAR(50),
 @ShippingAddress VARCHAR(200),
 @CustomerEmail VARCHAR(50))
AS
UPDATE Orders
SET DateCreated=@DateCreated,
    DateShipped=@DateShipped,
    Verified=@Verified,
    Completed=@Completed,
    Canceled=@Canceled,
    Comments=@Comments,
    CustomerName=@CustomerName,
    ShippingAddress=@ShippingAddress,
    CustomerEmail=@CustomerEmail
WHERE OrderID = @OrderID
GO
/****** Object:  StoredProcedure [dbo].[OrdersGetVerifiedUncompleted]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrdersGetVerifiedUncompleted]
AS
SELECT OrderID, DateCreated, DateShipped, 
       Verified, Completed, Canceled, CustomerName
FROM Orders
WHERE Verified=1 AND Completed=0
ORDER BY DateCreated DESC
GO
/****** Object:  StoredProcedure [dbo].[OrdersGetUnverifiedUncanceled]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrdersGetUnverifiedUncanceled]
AS
SELECT OrderID, DateCreated, DateShipped, 
       Verified, Completed, Canceled, CustomerName
FROM Orders
WHERE Verified=0 AND Canceled=0
ORDER BY DateCreated DESC
GO
/****** Object:  StoredProcedure [dbo].[OrdersGetByRecent]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrdersGetByRecent] 
(@Count smallINT)
AS
-- Set the number of rows to be returned
SET ROWCOUNT @Count
-- Get list of orders
SELECT OrderID, DateCreated, DateShipped, 
       Verified, Completed, Canceled, CustomerName
FROM Orders
ORDER BY DateCreated DESC
-- Reset rowcount value
SET ROWCOUNT 0
GO
/****** Object:  StoredProcedure [dbo].[OrdersGetByDate]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrdersGetByDate] 
(@StartDate SMALLDATETIME,
 @EndDate SMALLDATETIME)
AS
SELECT OrderID, DateCreated, DateShipped, 
       Verified, Completed, Canceled, CustomerName
FROM Orders
WHERE DateCreated BETWEEN @StartDate AND @EndDate
ORDER BY DateCreated DESC
GO
/****** Object:  StoredProcedure [dbo].[CatalogUpdateDepartment]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogUpdateDepartment]
(@DepartmentID int,
@DepartmentName nvarchar(50),
@DepartmentDescription nvarchar(1000))
AS
UPDATE Department
SET Name = @DepartmentName, Description = @DepartmentDescription
WHERE DepartmentID = @DepartmentID
GO
/****** Object:  StoredProcedure [dbo].[CatalogAddDepartment]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogAddDepartment]
(@DepartmentName nvarchar(50),
@DepartmentDescription nvarchar(1000))
AS
INSERT INTO Department (Name, Description)
VALUES (@DepartmentName, @DepartmentDescription)
GO
/****** Object:  View [dbo].[vw_aspnet_Applications]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_Applications]
  AS SELECT [dbo].[aspnet_Applications].[ApplicationName], [dbo].[aspnet_Applications].[LoweredApplicationName], [dbo].[aspnet_Applications].[ApplicationId], [dbo].[aspnet_Applications].[Description]
  FROM [dbo].[aspnet_Applications]
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Paths]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_WebPartState_Paths]
  AS SELECT [dbo].[aspnet_Paths].[ApplicationId], [dbo].[aspnet_Paths].[PathId], [dbo].[aspnet_Paths].[Path], [dbo].[aspnet_Paths].[LoweredPath]
  FROM [dbo].[aspnet_Paths]
GO
/****** Object:  View [dbo].[vw_aspnet_Users]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_Users]
  AS SELECT [dbo].[aspnet_Users].[ApplicationId], [dbo].[aspnet_Users].[UserId], [dbo].[aspnet_Users].[UserName], [dbo].[aspnet_Users].[LoweredUserName], [dbo].[aspnet_Users].[MobileAlias], [dbo].[aspnet_Users].[IsAnonymous], [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Users]
GO
/****** Object:  View [dbo].[vw_aspnet_Roles]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_Roles]
  AS SELECT [dbo].[aspnet_Roles].[ApplicationId], [dbo].[aspnet_Roles].[RoleId], [dbo].[aspnet_Roles].[RoleName], [dbo].[aspnet_Roles].[LoweredRoleName], [dbo].[aspnet_Roles].[Description]
  FROM [dbo].[aspnet_Roles]
GO
/****** Object:  StoredProcedure [dbo].[CatalogUpdateCategory]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogUpdateCategory]
(@CategoryID int,
@CategoryName nvarchar(50),
@CategoryDescription nvarchar(1000))
AS
UPDATE Category
SET Name = @CategoryName, Description = @CategoryDescription
WHERE CategoryID = @CategoryID
GO
/****** Object:  Table [dbo].[ShoppingCart]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ShoppingCart](
	[CartID] [char](36) NOT NULL,
	[ProductID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Attributes] [nvarchar](1000) NULL,
	[DateAdded] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_ShoppingCart] PRIMARY KEY CLUSTERED 
(
	[CartID] ASC,
	[ProductID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[SearchWord]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SearchWord] (@Word NVARCHAR(50))
AS

SET @Word = 'FORMSOF(INFLECTIONAL, "' + @Word + '")'

SELECT COALESCE(NameResults.[KEY], DescriptionResults.[KEY]) AS [KEY],
       ISNULL(NameResults.Rank, 0) * 3 + 
       ISNULL(DescriptionResults.Rank, 0) AS Rank 
FROM 
  CONTAINSTABLE(Product, Name, @Word, 
                LANGUAGE 'English') AS NameResults
  FULL OUTER JOIN
  CONTAINSTABLE(Product, Description, @Word, 
                LANGUAGE 'English') AS DescriptionResults
  ON NameResults.[KEY] = DescriptionResults.[KEY]
GO
/****** Object:  Table [dbo].[ProductRating]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductRating](
	[UserID] [uniqueidentifier] NULL,
	[ProductRatingID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NOT NULL,
	[rate] [int] NOT NULL,
	[comment] [nvarchar](1000) NOT NULL,
	[created] [datetime] NULL,
 CONSTRAINT [PK_ProductRating] PRIMARY KEY CLUSTERED 
(
	[ProductRatingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ProductRating] ON
INSERT [dbo].[ProductRating] ([UserID], [ProductRatingID], [ProductID], [rate], [comment], [created]) VALUES (N'e4985154-3fa1-40ce-8140-3581b21ddf2a', 1, 1, 1, N'this is a nice comment', CAST(0x0000A19F00BECFB1 AS DateTime))
INSERT [dbo].[ProductRating] ([UserID], [ProductRatingID], [ProductID], [rate], [comment], [created]) VALUES (N'ab959136-cc0d-4d63-b921-1092677f7f64', 2, 1, 5, N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus gravida venenatis quam eu vehicula. Aliquam vitae erat tellus. Sed aliquam quam non massa euismod nec ultrices purus vestibulum. Nunc id arcu sed erat tempus fringilla. Donec tincidunt tristique tellus non laoreet. Donec vel dui mauris. Etiam mi augue, feugiat a scelerisque et, tristique ut ligula. Proin consectetur semper nulla, a pellentesque sem porta in. Nullam a adipiscing leo. ', CAST(0x0000A19F016444FC AS DateTime))
INSERT [dbo].[ProductRating] ([UserID], [ProductRatingID], [ProductID], [rate], [comment], [created]) VALUES (N'ab959136-cc0d-4d63-b921-1092677f7f64', 3, 2, 5, N'Testing inserting comment', CAST(0x0000A19F01843EC3 AS DateTime))
INSERT [dbo].[ProductRating] ([UserID], [ProductRatingID], [ProductID], [rate], [comment], [created]) VALUES (N'ab959136-cc0d-4d63-b921-1092677f7f64', 4, 2, 3, N'Testing comment 2', CAST(0x0000A19F01846E8A AS DateTime))
INSERT [dbo].[ProductRating] ([UserID], [ProductRatingID], [ProductID], [rate], [comment], [created]) VALUES (N'ab959136-cc0d-4d63-b921-1092677f7f64', 5, 47, 3, N'Testing rating comment', CAST(0x0000A1A000B62950 AS DateTime))
INSERT [dbo].[ProductRating] ([UserID], [ProductRatingID], [ProductID], [rate], [comment], [created]) VALUES (N'ab959136-cc0d-4d63-b921-1092677f7f64', 6, 47, 3, N'Testing rating comment 2', CAST(0x0000A1A000B6332C AS DateTime))
INSERT [dbo].[ProductRating] ([UserID], [ProductRatingID], [ProductID], [rate], [comment], [created]) VALUES (N'ab959136-cc0d-4d63-b921-1092677f7f64', 7, 47, 4, N'Testing rating comment 3', CAST(0x0000A1A000B63CD4 AS DateTime))
SET IDENTITY_INSERT [dbo].[ProductRating] OFF
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductCategory](
	[ProductID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
 CONSTRAINT [PK_ProductCategory] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (1, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (2, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (3, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (4, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (5, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (6, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (7, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (8, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (9, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (10, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (11, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (12, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (13, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (14, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (15, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (16, 1)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (17, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (18, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (19, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (20, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (21, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (22, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (23, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (24, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (25, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (26, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (27, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (28, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (29, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (30, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (31, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (32, 2)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (33, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (34, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (35, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (36, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (37, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (38, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (39, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (40, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (41, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (42, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (43, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (44, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (45, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (46, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (47, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (48, 3)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (49, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (50, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (51, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (52, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (53, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (54, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (55, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (56, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (57, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (58, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (59, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (60, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (61, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (62, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (63, 4)
INSERT [dbo].[ProductCategory] ([ProductID], [CategoryID]) VALUES (64, 4)
/****** Object:  Table [dbo].[ProductAttributeValue]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductAttributeValue](
	[ProductID] [int] NOT NULL,
	[AttributeValueID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC,
	[AttributeValueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (1, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (1, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (1, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (1, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (1, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (1, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (1, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (1, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (1, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (2, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (2, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (2, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (2, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (2, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (2, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (2, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (2, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (2, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (3, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (3, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (3, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (3, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (3, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (3, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (3, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (3, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (3, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (4, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (4, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (4, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (4, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (4, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (4, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (4, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (4, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (4, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (5, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (5, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (5, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (5, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (5, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (5, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (5, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (5, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (5, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (6, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (6, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (6, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (6, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (6, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (6, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (6, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (6, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (6, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (7, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (7, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (7, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (7, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (7, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (7, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (7, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (7, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (7, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (8, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (8, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (8, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (8, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (8, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (8, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (8, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (8, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (8, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (9, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (9, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (9, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (9, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (9, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (9, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (9, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (9, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (9, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (10, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (10, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (10, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (10, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (10, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (10, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (10, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (10, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (10, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (11, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (11, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (11, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (11, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (11, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (11, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (11, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (11, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (11, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (12, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (12, 2)
GO
print 'Processed 100 total records'
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (12, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (12, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (12, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (12, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (12, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (12, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (12, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (13, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (13, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (13, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (13, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (13, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (13, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (13, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (13, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (13, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (14, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (14, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (14, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (14, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (14, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (14, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (14, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (14, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (14, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (15, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (15, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (15, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (15, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (15, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (15, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (15, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (15, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (15, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (16, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (16, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (16, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (16, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (16, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (16, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (16, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (16, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (16, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (17, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (17, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (17, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (17, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (17, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (17, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (17, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (17, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (17, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (18, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (18, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (18, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (18, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (18, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (18, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (18, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (18, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (18, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (19, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (19, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (19, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (19, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (19, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (19, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (19, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (19, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (19, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (20, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (20, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (20, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (20, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (20, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (20, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (20, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (20, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (20, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (21, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (21, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (21, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (21, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (21, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (21, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (21, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (21, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (21, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (22, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (22, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (22, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (22, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (22, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (22, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (22, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (22, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (22, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (23, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (23, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (23, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (23, 4)
GO
print 'Processed 200 total records'
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (23, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (23, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (23, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (23, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (23, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (24, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (24, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (24, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (24, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (24, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (24, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (24, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (24, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (24, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (25, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (25, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (25, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (25, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (25, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (25, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (25, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (25, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (25, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (26, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (26, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (26, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (26, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (26, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (26, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (26, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (26, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (26, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (27, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (27, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (27, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (27, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (27, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (27, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (27, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (27, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (27, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (28, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (28, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (28, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (28, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (28, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (28, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (28, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (28, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (28, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (29, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (29, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (29, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (29, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (29, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (29, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (29, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (29, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (29, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (30, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (30, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (30, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (30, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (30, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (30, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (30, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (30, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (30, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (31, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (31, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (31, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (31, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (31, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (31, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (31, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (31, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (31, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (32, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (32, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (32, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (32, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (32, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (32, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (32, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (32, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (32, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (33, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (33, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (33, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (33, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (33, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (33, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (33, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (33, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (33, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (34, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (34, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (34, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (34, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (34, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (34, 6)
GO
print 'Processed 300 total records'
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (34, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (34, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (34, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (35, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (35, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (35, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (35, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (35, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (35, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (35, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (35, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (35, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (36, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (36, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (36, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (36, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (36, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (36, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (36, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (36, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (36, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (37, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (37, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (37, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (37, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (37, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (37, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (37, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (37, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (37, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (38, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (38, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (38, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (38, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (38, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (38, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (38, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (38, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (38, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (39, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (39, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (39, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (39, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (39, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (39, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (39, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (39, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (39, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (40, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (40, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (40, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (40, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (40, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (40, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (40, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (40, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (40, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (41, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (41, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (41, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (41, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (41, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (41, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (41, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (41, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (41, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (42, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (42, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (42, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (42, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (42, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (42, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (42, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (42, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (42, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (43, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (43, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (43, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (43, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (43, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (43, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (43, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (43, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (43, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (44, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (44, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (44, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (44, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (44, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (44, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (44, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (44, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (44, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (45, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (45, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (45, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (45, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (45, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (45, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (45, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (45, 8)
GO
print 'Processed 400 total records'
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (45, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (46, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (46, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (46, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (46, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (46, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (46, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (46, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (46, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (46, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (47, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (47, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (47, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (47, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (47, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (47, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (47, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (47, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (47, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (48, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (48, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (48, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (48, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (48, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (48, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (48, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (48, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (48, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (49, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (49, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (49, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (49, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (49, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (49, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (49, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (49, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (49, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (50, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (50, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (50, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (50, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (50, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (50, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (50, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (50, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (50, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (51, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (51, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (51, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (51, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (51, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (51, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (51, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (51, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (51, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (52, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (52, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (52, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (52, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (52, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (52, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (52, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (52, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (52, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (53, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (53, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (53, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (53, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (53, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (53, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (53, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (53, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (53, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (54, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (54, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (54, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (54, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (54, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (54, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (54, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (54, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (54, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (55, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (55, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (55, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (55, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (55, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (55, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (55, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (55, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (55, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (56, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (56, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (56, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (56, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (56, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (56, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (56, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (56, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (56, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (57, 1)
GO
print 'Processed 500 total records'
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (57, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (57, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (57, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (57, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (57, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (57, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (57, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (57, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (58, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (58, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (58, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (58, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (58, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (58, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (58, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (58, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (58, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (59, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (59, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (59, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (59, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (59, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (59, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (59, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (59, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (59, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (60, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (60, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (60, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (60, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (60, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (60, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (60, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (60, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (60, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (61, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (61, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (61, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (61, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (61, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (61, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (61, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (61, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (61, 9)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (62, 1)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (62, 2)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (62, 3)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (62, 4)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (62, 5)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (62, 6)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (62, 7)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (62, 8)
INSERT [dbo].[ProductAttributeValue] ([ProductID], [AttributeValueID]) VALUES (62, 9)
/****** Object:  Table [dbo].[aspnet_PersonalizationPerUser]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_PersonalizationPerUser](
	[Id] [uniqueidentifier] NOT NULL,
	[PathId] [uniqueidentifier] NULL,
	[UserId] [uniqueidentifier] NULL,
	[PageSettings] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_PersonalizationPerUser_index1] ON [dbo].[aspnet_PersonalizationPerUser] 
(
	[PathId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [aspnet_PersonalizationPerUser_ncindex2] ON [dbo].[aspnet_PersonalizationPerUser] 
(
	[UserId] ASC,
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aspnet_Profile]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Profile](
	[UserId] [uniqueidentifier] NOT NULL,
	[PropertyNames] [ntext] NOT NULL,
	[PropertyValuesString] [ntext] NOT NULL,
	[PropertyValuesBinary] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aspnet_Membership]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Membership](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[MobilePIN] [nvarchar](16) NULL,
	[Email] [nvarchar](256) NULL,
	[LoweredEmail] [nvarchar](256) NULL,
	[PasswordQuestion] [nvarchar](256) NULL,
	[PasswordAnswer] [nvarchar](128) NULL,
	[IsApproved] [bit] NOT NULL,
	[IsLockedOut] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
	[LastPasswordChangedDate] [datetime] NOT NULL,
	[LastLockoutDate] [datetime] NOT NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAttemptWindowStart] [datetime] NOT NULL,
	[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttemptWindowStart] [datetime] NOT NULL,
	[Comment] [ntext] NULL,
PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [aspnet_Membership_index] ON [dbo].[aspnet_Membership] 
(
	[ApplicationId] ASC,
	[LoweredEmail] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'0ee00cdd-475a-4c89-8b88-191118d6019e', N'57df95b6-b3b2-4605-b2da-d48297f4b585', N'ywq/Ro8r+ll3J2nndD3Y0GxJ/hk=', 1, N'iAN62Djq+J6Xb3Yb6nCjvw==', NULL, N'test@test.com', N'test@test.com', N'test', N'mhnyG2j1SphoPNaFuO6qNctRmEc=', 1, 0, CAST(0x0000A19D01557480 AS DateTime), CAST(0x0000A1A000FAAFCC AS DateTime), CAST(0x0000A19D01557480 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'0ee00cdd-475a-4c89-8b88-191118d6019e', N'ab959136-cc0d-4d63-b921-1092677f7f64', N'ejlGydSNId9ZJNu6i6nJESqiRnU=', 1, N'T4uNtLZdWtyveaob7/MIiA==', NULL, N'test1@test.com', N'test1@test.com', N'test', N'eg0lNY/iJGgOCxOYSv6RXweDYBo=', 1, 0, CAST(0x0000A19D0155A810 AS DateTime), CAST(0x0000A1A000F7FEB1 AS DateTime), CAST(0x0000A19D0155A810 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'a1fcdf22-40f5-4c33-a685-a2161508df75', N'bc707ac4-e84f-400f-9649-7164ecb7b31e', N'A6+tUqd31oBoFtgfXvPPel2TF60=', 1, N'LGexuPCSOLthFXdz065OCQ==', NULL, N'mansillag@gmail.com', N'mansillag@gmail.com', N'1 plus 1', N'gaOOT1xmFxTGKSg8vuoEGYmR52k=', 1, 0, CAST(0x0000A19601205994 AS DateTime), CAST(0x0000A1960141B665 AS DateTime), CAST(0x0000A19601205994 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
/****** Object:  StoredProcedure [dbo].[aspnet_Paths_CreatePath]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Paths_CreatePath]
    @ApplicationId UNIQUEIDENTIFIER,
    @Path           NVARCHAR(256),
    @PathId         UNIQUEIDENTIFIER OUTPUT
AS
BEGIN
    BEGIN TRANSACTION
    IF (NOT EXISTS(SELECT * FROM dbo.aspnet_Paths WHERE LoweredPath = LOWER(@Path) AND ApplicationId = @ApplicationId))
    BEGIN
        INSERT dbo.aspnet_Paths (ApplicationId, Path, LoweredPath) VALUES (@ApplicationId, @Path, LOWER(@Path))
    END
    COMMIT TRANSACTION
    SELECT @PathId = PathId FROM dbo.aspnet_Paths WHERE LOWER(@Path) = LoweredPath AND ApplicationId = @ApplicationId
END
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationAllUsers]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_PersonalizationAllUsers](
	[PathId] [uniqueidentifier] NOT NULL,
	[PageSettings] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_CreateUser]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Users_CreateUser]
    @ApplicationId    uniqueidentifier,
    @UserName         nvarchar(256),
    @IsUserAnonymous  bit,
    @LastActivityDate DATETIME,
    @UserId           uniqueidentifier OUTPUT
AS
BEGIN
    IF( @UserId IS NULL )
        SELECT @UserId = NEWID()
    ELSE
    BEGIN
        IF( EXISTS( SELECT UserId FROM dbo.aspnet_Users
                    WHERE @UserId = UserId ) )
            RETURN -1
    END

    INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
    VALUES (@ApplicationId, @UserId, @UserName, LOWER(@UserName), @IsUserAnonymous, @LastActivityDate)

    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_RoleExists]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Roles_RoleExists]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(0)
    IF (EXISTS (SELECT RoleName FROM dbo.aspnet_Roles WHERE LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId ))
        RETURN(1)
    ELSE
        RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_GetAllRoles]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Roles_GetAllRoles] (
    @ApplicationName           nvarchar(256))
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN
    SELECT RoleName
    FROM   dbo.aspnet_Roles WHERE ApplicationId = @ApplicationId
    ORDER BY RoleName
END
GO
/****** Object:  Table [dbo].[aspnet_UsersInRoles]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_UsersInRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [aspnet_UsersInRoles_index] ON [dbo].[aspnet_UsersInRoles] 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[aspnet_UsersInRoles] ([UserId], [RoleId]) VALUES (N'e4985154-3fa1-40ce-8140-3581b21ddf2a', N'9bdb4136-0b58-4055-bcb0-3ff983b736b2')
INSERT [dbo].[aspnet_UsersInRoles] ([UserId], [RoleId]) VALUES (N'bc707ac4-e84f-400f-9649-7164ecb7b31e', N'5055d286-50b4-4e69-9358-b913f45865e9')
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_CreateRole]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Roles_CreateRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS(SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId))
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    INSERT INTO dbo.aspnet_Roles
                (ApplicationId, RoleName, LoweredRoleName)
         VALUES (@ApplicationId, @RoleName, LOWER(@RoleName))

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[CatalogCreateCategory]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogCreateCategory]
(@DepartmentID int,
@CategoryName nvarchar(50),
@CategoryDescription nvarchar(50))
AS
INSERT INTO Category (DepartmentID, Name, Description)
VALUES (@DepartmentID, @CategoryName, @CategoryDescription)
GO
/****** Object:  StoredProcedure [dbo].[CatalogGetCategoriesInDepartment]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--missing procedures from 05-07


CREATE PROCEDURE [dbo].[CatalogGetCategoriesInDepartment]
(@DepartmentID INT)
AS
SELECT CategoryID, Name, Description
FROM Category
WHERE DepartmentID = @DepartmentID
GO
/****** Object:  StoredProcedure [dbo].[CatalogGetCategories]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogGetCategories]
AS
SELECT CategoryID, Name
FROM Category
GO
/****** Object:  StoredProcedure [dbo].[CatalogGetBestSellingProducts]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogGetBestSellingProducts]
AS
SELECT TOP(6) ProductID, Name, Description, Price,
       Image
FROM Product
WHERE Stock IS NOT NULL
ORDER BY Stock ASC;
GO
/****** Object:  StoredProcedure [dbo].[CatalogUpdateProduct]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogUpdateProduct]
(@ProductID INT,
 @ProductName VARCHAR(50),
 @ProductDescription VARCHAR(5000),
 @Price MONEY,
 @Image VARCHAR(50),
 @Stock INT)
AS
UPDATE Product
SET Name = @ProductName,
    Description = @ProductDescription,
    Price = @Price,
    Image = @Image,
	Stock = @Stock
WHERE ProductID = @ProductID
GO
/****** Object:  StoredProcedure [dbo].[CatalogGetCategoryDetails]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogGetCategoryDetails]
(@CategoryID INT)
AS
SELECT DepartmentID, Name, Description
FROM Category
WHERE CategoryID = @CategoryID
GO
/****** Object:  StoredProcedure [dbo].[CatalogDeleteCategory]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogDeleteCategory]
(@CategoryID int)
AS
DELETE FROM Category
WHERE CategoryID = @CategoryID
GO
/****** Object:  StoredProcedure [dbo].[CatalogGetProductRecommendations]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CatalogGetProductRecommendations]
(@ProductID INT,
@DescriptionLength INT)
AS
SELECT ProductID, Price, Image, 
CASE WHEN LEN(Name) <= @DescriptionLength THEN Name
ELSE SUBSTRING(Name, 1, @DescriptionLength) + '...' END
AS Name
FROM Product
WHERE ProductID IN
(
SELECT TOP 5 od2.ProductID
FROM OrderDetail od1
JOIN OrderDetail od2 ON od1.OrderID = od2.OrderID
WHERE od1.ProductID = @ProductID AND od2.ProductID != @ProductID
GROUP BY od2.ProductID
ORDER BY COUNT(od2.ProductID) DESC
)
GO
/****** Object:  StoredProcedure [dbo].[CatalogGetProductDetails]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogGetProductDetails]
(@ProductID INT)
AS
SELECT Name, Description, Price, Thumbnail, Image, PromoFront, PromoDept, Features, Stock
FROM Product
WHERE ProductID = @ProductID
GO
/****** Object:  StoredProcedure [dbo].[OrderGetInfo]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderGetInfo]
(@OrderID INT)
AS
SELECT OrderID, 
      (SELECT ISNULL(SUM(Subtotal), 0) FROM OrderDetail WHERE OrderID = @OrderID)        
       AS TotalAmount, 
       DateCreated, 
       DateShipped, 
       Verified, 
       Completed, 
       Canceled, 
       Comments, 
       CustomerName, 
       ShippingAddress, 
       CustomerEmail
FROM Orders
WHERE OrderID = @OrderID
GO
/****** Object:  StoredProcedure [dbo].[OrderGetDetails]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderGetDetails]
(@OrderID INT)
AS
SELECT Orders.OrderID, 
       ProductID, 
       ProductName, 
       Quantity, 
       UnitCost, 
       Subtotal
FROM OrderDetail JOIN Orders
ON Orders.OrderID = OrderDetail.OrderID
WHERE Orders.OrderID = @OrderID
GO
/****** Object:  StoredProcedure [dbo].[GetShoppingCartRecommendations]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetShoppingCartRecommendations]
(@CartID CHAR(36),
 @DescriptionLength INT)
AS
--- Returns the product recommendations
SELECT ProductID,
       Name, Image, Price,
       SUBSTRING(Description, 1, @DescriptionLength) + '...' AS Description
FROM Product
WHERE ProductID IN
   (
   -- Returns the products that exist in a list of orders
   SELECT TOP 5 od1.ProductID AS Rank
   FROM OrderDetail od1 
     JOIN OrderDetail od2
       ON od1.OrderID=od2.OrderID
     JOIN ShoppingCart sp
       ON od2.ProductID = sp.ProductID
   WHERE sp.CartID = @CartID
        -- Must not include products that already exist in the visitor''s cart
      AND od1.ProductID NOT IN
      (
      -- Returns the products in the specified shopping cart
      SELECT ProductID 
      FROM ShoppingCart
      WHERE CartID = @CartID
      )
   -- Group the ProductID so we can calculate the rank
   GROUP BY od1.ProductID
   -- Order descending by rank
   ORDER BY COUNT(od1.ProductID) DESC
   )
GO
/****** Object:  StoredProcedure [dbo].[GetProductsInBrandAndCategory]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetProductsInBrandAndCategory]
(@brandId INT,
@CategoryID INT,
@PageNumber INT,
@ProductsPerPage INT,
@HowManyProducts INT OUTPUT)
AS

-- declare a new TABLE variable
DECLARE @Products TABLE
(RowNumber INT,
 ProductID INT,
 BrandID INT,
 Name NVARCHAR(100),
 Price MONEY,
 Stock INT,
 Image NVARCHAR(50))


-- populate the table variable with the complete list of products
INSERT INTO @Products
SELECT ROW_NUMBER() OVER (ORDER BY Product.ProductID),
       Product.ProductID, Product.BrandID, Name, 
       Price, Stock, Image
FROM Product INNER JOIN ProductCategory
  ON Product.ProductID = ProductCategory.ProductID
WHERE ProductCategory.CategoryID = @CategoryID AND Product.BrandID = @brandId

-- return the total number of products using an OUTPUT variable
SELECT @HowManyProducts = COUNT(ProductID) FROM @Products

-- extract the requested page of products
SELECT ProductID, BrandID, Name,  Price, Stock,
       Image
FROM @Products
WHERE RowNumber > (@PageNumber - 1) * @ProductsPerPage
  AND RowNumber <= @PageNumber * @ProductsPerPage
GO
/****** Object:  StoredProcedure [dbo].[GetProductRating]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetProductRating] 
	@productId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select count(rate) as numberOfRates, avg(1.0*rate) as rate from ProductRating
	where ProductRating.ProductId = @productId
END
GO
/****** Object:  StoredProcedure [dbo].[GetProductComments]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetProductComments]
	-- Add the parameters for the stored procedure here
	@productId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select PR.ProductRatingID, PR.rate, P.Name, PR.comment, PR.created, Usr.UserName, Usr.UserId 
    from ProductRating PR
	inner join aspnet_Users Usr on PR.UserID = Usr.UserId
	inner join Product P on PR.ProductID = P.ProductID
	where PR.ProductId = @productId
END
GO
/****** Object:  StoredProcedure [dbo].[GetBrandsForCategory]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetBrandsForCategory]
	-- Add the parameters for the stored procedure here
	--@categoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Distinct Brand.BrandId, BName, Category.CategoryID, Category.Name  
		from ProductCategory
	inner join Product on ProductCategory.ProductID = Product.ProductID
	inner join Brand on Product.BrandID = Brand.BrandID
	inner join Category on ProductCategory.CategoryID = Category.CategoryID
	--where ProductCategory.CategoryId = @categoryId
	
END
GO
/****** Object:  StoredProcedure [dbo].[GetBrandsForACategory]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetBrandsForACategory]
	-- Add the parameters for the stored procedure here
	@categoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Distinct Brand.BrandId, BName
		from ProductCategory
	inner join Product on ProductCategory.ProductID = Product.ProductID
	inner join Brand on Product.BrandID = Brand.BrandID
	inner join Category on ProductCategory.CategoryID = Category.CategoryID
	where ProductCategory.CategoryId = @categoryId
	
END
GO
/****** Object:  StoredProcedure [dbo].[CreateOrder]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateOrder] 
(@CartID char(36))
AS
/* Insert a new record INTo Orders */
DECLARE @OrderID INT
INSERT INTO Orders DEFAULT VALUES
/* Save the new Order ID */
SET @OrderID = @@IDENTITY
/* Add the order details to OrderDetail */
INSERT INTO OrderDetail 
     (OrderID, ProductID, ProductName, Quantity, UnitCost)
SELECT 
     @OrderID, Product.ProductID, Product.Name, 
     ShoppingCart.Quantity, Product.Price
FROM Product JOIN ShoppingCart
ON Product.ProductID = ShoppingCart.ProductID
WHERE ShoppingCart.CartID = @CartID
/* Clear the shopping cart */
DELETE FROM ShoppingCart
WHERE CartID = @CartID
/* Return the Order ID */
SELECT @OrderID
GO
/****** Object:  StoredProcedure [dbo].[CatalogGetProductAttributeValues]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create CatalogGetProductAttributeValues stored procedure
CREATE PROCEDURE [dbo].[CatalogGetProductAttributeValues]
(@ProductId INT)
AS
SELECT a.Name AS AttributeName,
       av.AttributeValueID, 
       av.Value AS AttributeValue
FROM AttributeValue av
INNER JOIN attribute a ON av.AttributeID = a.AttributeID
WHERE av.AttributeValueID IN
  (SELECT AttributeValueID
   FROM ProductAttributeValue
   WHERE ProductID = @ProductID)
ORDER BY a.Name;
GO
/****** Object:  StoredProcedure [dbo].[CatalogCreateProduct]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogCreateProduct]
(@CategoryID INT,
 @ProductName NVARCHAR(50),
 @ProductDescription NVARCHAR(MAX),
 @Price MONEY,
 @Image NVARCHAR(50),
 @Stock INT)
AS
-- Declare a variable to hold the generated product ID
DECLARE @ProductID int
-- Create the new product entry

INSERT INTO Product 
    (Name, 
     Description, 
     Price, 
     Image,
     Stock)
VALUES 
    (@ProductName, 
     @ProductDescription, 
     @Price, 
     @Image,
     @Stock)
-- Save the generated product ID to a variable
SELECT @ProductID = @@Identity
-- Associate the product with a category
INSERT INTO ProductCategory (ProductID, CategoryID)
VALUES (@ProductID, @CategoryID)
GO
/****** Object:  StoredProcedure [dbo].[CatalogGetCategoriesWithProduct]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogGetCategoriesWithProduct]
(@ProductID int)
AS
SELECT Category.CategoryID, Name
FROM Category INNER JOIN ProductCategory
ON Category.CategoryID = ProductCategory.CategoryID
WHERE ProductCategory.ProductID = @ProductID
GO
/****** Object:  StoredProcedure [dbo].[CatalogGetCategoriesWithoutProduct]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogGetCategoriesWithoutProduct]
(@ProductID int)
AS
SELECT CategoryID, Name
FROM Category
WHERE CategoryID NOT IN
   (SELECT Category.CategoryID
    FROM Category INNER JOIN ProductCategory
    ON Category.CategoryID = ProductCategory.CategoryID
    WHERE ProductCategory.ProductID = @ProductID)
GO
/****** Object:  StoredProcedure [dbo].[CatalogGetAllProductsInCategory]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogGetAllProductsInCategory]
(@CategoryID INT)
AS
SELECT Product.ProductID, Name, Description, Price, 
       Image, Stock
FROM Product INNER JOIN ProductCategory
  ON Product.ProductID = ProductCategory.ProductID
WHERE ProductCategory.CategoryID = @CategoryID
GO
/****** Object:  StoredProcedure [dbo].[CatalogDeleteProduct]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogDeleteProduct]
(@ProductID int)
AS
DELETE FROM ShoppingCart WHERE ProductID=@ProductID
DELETE FROM ProductCategory WHERE ProductID=@ProductID
DELETE FROM Product where ProductID=@ProductID
GO
/****** Object:  StoredProcedure [dbo].[CatalogAssignProductToCategory]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogAssignProductToCategory]
(@ProductID int, @CategoryID int)
AS
INSERT INTO ProductCategory (ProductID, CategoryID)
VALUES (@ProductID, @CategoryID)
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteInactiveProfiles]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_DeleteInactiveProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @InactiveSinceDate      datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
    BEGIN
        SELECT  0
        RETURN
    END

    DELETE
    FROM    dbo.aspnet_Profile
    WHERE   UserId IN
            (   SELECT  UserId
                FROM    dbo.aspnet_Users u
                WHERE   ApplicationId = @ApplicationId
                        AND (LastActivityDate <= @InactiveSinceDate)
                        AND (
                                (@ProfileAuthOptions = 2)
                             OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                             OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
                            )
            )

    SELECT  @@ROWCOUNT
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]
	@ApplicationName  nvarchar(256),
	@UserNames		  nvarchar(4000),
	@RoleNames		  nvarchar(4000)
AS
BEGIN
	DECLARE @AppId uniqueidentifier
	SELECT  @AppId = NULL
	SELECT  @AppId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
	IF (@AppId IS NULL)
		RETURN(2)


	DECLARE @TranStarted   bit
	SET @TranStarted = 0

	IF( @@TRANCOUNT = 0 )
	BEGIN
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END

	DECLARE @tbNames  table(Name nvarchar(256) NOT NULL PRIMARY KEY)
	DECLARE @tbRoles  table(RoleId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @tbUsers  table(UserId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @Num	  int
	DECLARE @Pos	  int
	DECLARE @NextPos  int
	DECLARE @Name	  nvarchar(256)
	DECLARE @CountAll int
	DECLARE @CountU	  int
	DECLARE @CountR	  int


	SET @Num = 0
	SET @Pos = 1
	WHILE(@Pos <= LEN(@RoleNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N',', @RoleNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@RoleNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@RoleNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbRoles
	  SELECT RoleId
	  FROM   dbo.aspnet_Roles ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredRoleName AND ar.ApplicationId = @AppId
	SELECT @CountR = @@ROWCOUNT

	IF (@CountR <> @Num)
	BEGIN
		SELECT TOP 1 N'', Name
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT ar.LoweredRoleName FROM dbo.aspnet_Roles ar,  @tbRoles r WHERE r.RoleId = ar.RoleId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(2)
	END


	DELETE FROM @tbNames WHERE 1=1
	SET @Num = 0
	SET @Pos = 1


	WHILE(@Pos <= LEN(@UserNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N',', @UserNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@UserNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@UserNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbUsers
	  SELECT UserId
	  FROM   dbo.aspnet_Users ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredUserName AND ar.ApplicationId = @AppId

	SELECT @CountU = @@ROWCOUNT
	IF (@CountU <> @Num)
	BEGIN
		SELECT TOP 1 Name, N''
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT au.LoweredUserName FROM dbo.aspnet_Users au,  @tbUsers u WHERE u.UserId = au.UserId)

		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(1)
	END

	SELECT  @CountAll = COUNT(*)
	FROM	dbo.aspnet_UsersInRoles ur, @tbUsers u, @tbRoles r
	WHERE   ur.UserId = u.UserId AND ur.RoleId = r.RoleId

	IF (@CountAll <> @CountU * @CountR)
	BEGIN
		SELECT TOP 1 UserName, RoleName
		FROM		 @tbUsers tu, @tbRoles tr, dbo.aspnet_Users u, dbo.aspnet_Roles r
		WHERE		 u.UserId = tu.UserId AND r.RoleId = tr.RoleId AND
					 tu.UserId NOT IN (SELECT ur.UserId FROM dbo.aspnet_UsersInRoles ur WHERE ur.RoleId = tr.RoleId) AND
					 tr.RoleId NOT IN (SELECT ur.RoleId FROM dbo.aspnet_UsersInRoles ur WHERE ur.UserId = tu.UserId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(3)
	END

	DELETE FROM dbo.aspnet_UsersInRoles
	WHERE UserId IN (SELECT UserId FROM @tbUsers)
	  AND RoleId IN (SELECT RoleId FROM @tbRoles)
	IF( @TranStarted = 1 )
		COMMIT TRANSACTION
	RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_IsUserInRole]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_IsUserInRole]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(2)
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    DECLARE @RoleId uniqueidentifier
    SELECT  @RoleId = NULL

    SELECT  @UserId = UserId
    FROM    dbo.aspnet_Users
    WHERE   LoweredUserName = LOWER(@UserName) AND ApplicationId = @ApplicationId

    IF (@UserId IS NULL)
        RETURN(2)

    SELECT  @RoleId = RoleId
    FROM    dbo.aspnet_Roles
    WHERE   LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId

    IF (@RoleId IS NULL)
        RETURN(3)

    IF (EXISTS( SELECT * FROM dbo.aspnet_UsersInRoles WHERE  UserId = @UserId AND RoleId = @RoleId))
        RETURN(1)
    ELSE
        RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetUsersInRoles]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_GetUsersInRoles]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
     DECLARE @RoleId uniqueidentifier
     SELECT  @RoleId = NULL

     SELECT  @RoleId = RoleId
     FROM    dbo.aspnet_Roles
     WHERE   LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId

     IF (@RoleId IS NULL)
         RETURN(1)

    SELECT u.UserName
    FROM   dbo.aspnet_Users u, dbo.aspnet_UsersInRoles ur
    WHERE  u.UserId = ur.UserId AND @RoleId = ur.RoleId AND u.ApplicationId = @ApplicationId
    ORDER BY u.UserName
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetRolesForUser]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_GetRolesForUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL

    SELECT  @UserId = UserId
    FROM    dbo.aspnet_Users
    WHERE   LoweredUserName = LOWER(@UserName) AND ApplicationId = @ApplicationId

    IF (@UserId IS NULL)
        RETURN(1)

    SELECT r.RoleName
    FROM   dbo.aspnet_Roles r, dbo.aspnet_UsersInRoles ur
    WHERE  r.RoleId = ur.RoleId AND r.ApplicationId = @ApplicationId AND ur.UserId = @UserId
    ORDER BY r.RoleName
    RETURN (0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_FindUsersInRole]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_FindUsersInRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256),
    @UserNameToMatch  nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
     DECLARE @RoleId uniqueidentifier
     SELECT  @RoleId = NULL

     SELECT  @RoleId = RoleId
     FROM    dbo.aspnet_Roles
     WHERE   LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId

     IF (@RoleId IS NULL)
         RETURN(1)

    SELECT u.UserName
    FROM   dbo.aspnet_Users u, dbo.aspnet_UsersInRoles ur
    WHERE  u.UserId = ur.UserId AND @RoleId = ur.RoleId AND u.ApplicationId = @ApplicationId AND LoweredUserName LIKE LOWER(@UserNameToMatch)
    ORDER BY u.UserName
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_AddUsersToRoles]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_AddUsersToRoles]
	@ApplicationName  nvarchar(256),
	@UserNames		  nvarchar(4000),
	@RoleNames		  nvarchar(4000),
	@CurrentTimeUtc   datetime
AS
BEGIN
	DECLARE @AppId uniqueidentifier
	SELECT  @AppId = NULL
	SELECT  @AppId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
	IF (@AppId IS NULL)
		RETURN(2)
	DECLARE @TranStarted   bit
	SET @TranStarted = 0

	IF( @@TRANCOUNT = 0 )
	BEGIN
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END

	DECLARE @tbNames	table(Name nvarchar(256) NOT NULL PRIMARY KEY)
	DECLARE @tbRoles	table(RoleId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @tbUsers	table(UserId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @Num		int
	DECLARE @Pos		int
	DECLARE @NextPos	int
	DECLARE @Name		nvarchar(256)

	SET @Num = 0
	SET @Pos = 1
	WHILE(@Pos <= LEN(@RoleNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N',', @RoleNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@RoleNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@RoleNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbRoles
	  SELECT RoleId
	  FROM   dbo.aspnet_Roles ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredRoleName AND ar.ApplicationId = @AppId

	IF (@@ROWCOUNT <> @Num)
	BEGIN
		SELECT TOP 1 Name
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT ar.LoweredRoleName FROM dbo.aspnet_Roles ar,  @tbRoles r WHERE r.RoleId = ar.RoleId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(2)
	END

	DELETE FROM @tbNames WHERE 1=1
	SET @Num = 0
	SET @Pos = 1

	WHILE(@Pos <= LEN(@UserNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N',', @UserNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@UserNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@UserNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbUsers
	  SELECT UserId
	  FROM   dbo.aspnet_Users ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredUserName AND ar.ApplicationId = @AppId

	IF (@@ROWCOUNT <> @Num)
	BEGIN
		DELETE FROM @tbNames
		WHERE LOWER(Name) IN (SELECT LoweredUserName FROM dbo.aspnet_Users au,  @tbUsers u WHERE au.UserId = u.UserId)

		INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
		  SELECT @AppId, NEWID(), Name, LOWER(Name), 0, @CurrentTimeUtc
		  FROM   @tbNames

		INSERT INTO @tbUsers
		  SELECT  UserId
		  FROM	dbo.aspnet_Users au, @tbNames t
		  WHERE   LOWER(t.Name) = au.LoweredUserName AND au.ApplicationId = @AppId
	END

	IF (EXISTS (SELECT * FROM dbo.aspnet_UsersInRoles ur, @tbUsers tu, @tbRoles tr WHERE tu.UserId = ur.UserId AND tr.RoleId = ur.RoleId))
	BEGIN
		SELECT TOP 1 UserName, RoleName
		FROM		 dbo.aspnet_UsersInRoles ur, @tbUsers tu, @tbRoles tr, aspnet_Users u, aspnet_Roles r
		WHERE		u.UserId = tu.UserId AND r.RoleId = tr.RoleId AND tu.UserId = ur.UserId AND tr.RoleId = ur.RoleId

		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(3)
	END

	INSERT INTO dbo.aspnet_UsersInRoles (UserId, RoleId)
	SELECT UserId, RoleId
	FROM @tbUsers, @tbRoles

	IF( @TranStarted = 1 )
		COMMIT TRANSACTION
	RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_DeleteUser]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Users_DeleteUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @TablesToDeleteFrom int,
    @NumTablesDeletedFrom int OUTPUT
AS
BEGIN
    DECLARE @UserId               uniqueidentifier
    SELECT  @UserId               = NULL
    SELECT  @NumTablesDeletedFrom = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    DECLARE @ErrorCode   int
    DECLARE @RowCount    int

    SET @ErrorCode = 0
    SET @RowCount  = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   u.LoweredUserName       = LOWER(@UserName)
        AND u.ApplicationId         = a.ApplicationId
        AND LOWER(@ApplicationName) = a.LoweredApplicationName

    IF (@UserId IS NULL)
    BEGIN
        GOTO Cleanup
    END

    -- Delete from Membership table if (@TablesToDeleteFrom & 1) is set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_MembershipUsers') AND (type = 'V'))))
    BEGIN
        DELETE FROM dbo.aspnet_Membership WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
               @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_UsersInRoles table if (@TablesToDeleteFrom & 2) is set
    IF ((@TablesToDeleteFrom & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_UsersInRoles') AND (type = 'V'))) )
    BEGIN
        DELETE FROM dbo.aspnet_UsersInRoles WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Profile table if (@TablesToDeleteFrom & 4) is set
    IF ((@TablesToDeleteFrom & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_Profiles') AND (type = 'V'))) )
    BEGIN
        DELETE FROM dbo.aspnet_Profile WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_PersonalizationPerUser table if (@TablesToDeleteFrom & 8) is set
    IF ((@TablesToDeleteFrom & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_WebPartState_User') AND (type = 'V'))) )
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Users table if (@TablesToDeleteFrom & 1,2,4 & 8) are all set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (@TablesToDeleteFrom & 2) <> 0 AND
        (@TablesToDeleteFrom & 4) <> 0 AND
        (@TablesToDeleteFrom & 8) <> 0 AND
        (EXISTS (SELECT UserId FROM dbo.aspnet_Users WHERE @UserId = UserId)))
    BEGIN
        DELETE FROM dbo.aspnet_Users WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:
    SET @NumTablesDeletedFrom = 0

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
	    ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_DeleteRole]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Roles_DeleteRole]
    @ApplicationName            nvarchar(256),
    @RoleName                   nvarchar(256),
    @DeleteOnlyIfRoleIsEmpty    bit
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

    DECLARE @RoleId   uniqueidentifier
    SELECT  @RoleId = NULL
    SELECT  @RoleId = RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId

    IF (@RoleId IS NULL)
    BEGIN
        SELECT @ErrorCode = 1
        GOTO Cleanup
    END
    IF (@DeleteOnlyIfRoleIsEmpty <> 0)
    BEGIN
        IF (EXISTS (SELECT RoleId FROM dbo.aspnet_UsersInRoles  WHERE @RoleId = RoleId))
        BEGIN
            SELECT @ErrorCode = 2
            GOTO Cleanup
        END
    END


    DELETE FROM dbo.aspnet_UsersInRoles  WHERE @RoleId = RoleId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    DELETE FROM dbo.aspnet_Roles WHERE @RoleId = RoleId  AND ApplicationId = @ApplicationId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUserInfo]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUserInfo]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @IsPasswordCorrect              bit,
    @UpdateLastLoginActivityDate    bit,
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @LastLoginDate                  datetime,
    @LastActivityDate               datetime
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @IsApproved                             bit
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @IsApproved = m.IsApproved,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        GOTO Cleanup
    END

    IF( @IsPasswordCorrect = 0 )
    BEGIN
        IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAttemptWindowStart ) )
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = 1
        END
        ELSE
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = @FailedPasswordAttemptCount + 1
        END

        BEGIN
            IF( @FailedPasswordAttemptCount >= @MaxInvalidPasswordAttempts )
            BEGIN
                SET @IsLockedOut = 1
                SET @LastLockoutDate = @CurrentTimeUtc
            END
        END
    END
    ELSE
    BEGIN
        IF( @FailedPasswordAttemptCount > 0 OR @FailedPasswordAnswerAttemptCount > 0 )
        BEGIN
            SET @FailedPasswordAttemptCount = 0
            SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            SET @FailedPasswordAnswerAttemptCount = 0
            SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            SET @LastLockoutDate = CONVERT( datetime, '17540101', 112 )
        END
    END

    IF( @UpdateLastLoginActivityDate = 1 )
    BEGIN
        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @LastActivityDate
        WHERE   @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END

        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @LastLoginDate
        WHERE   UserId = @UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END


    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
        FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
        FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
        FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
        FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
    WHERE @UserId = UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUser]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUser]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @Email                nvarchar(256),
    @Comment              ntext,
    @IsApproved           bit,
    @LastLoginDate        datetime,
    @LastActivityDate     datetime,
    @UniqueEmail          int,
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId, @ApplicationId = a.ApplicationId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership WITH (UPDLOCK, HOLDLOCK)
                    WHERE ApplicationId = @ApplicationId  AND @UserId <> UserId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            RETURN(7)
        END
    END

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    UPDATE dbo.aspnet_Users WITH (ROWLOCK)
    SET
         LastActivityDate = @LastActivityDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    UPDATE dbo.aspnet_Membership WITH (ROWLOCK)
    SET
         Email            = @Email,
         LoweredEmail     = LOWER(@Email),
         Comment          = @Comment,
         IsApproved       = @IsApproved,
         LastLoginDate    = @LastLoginDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN -1
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UnlockUser]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_UnlockUser]
    @ApplicationName                         nvarchar(256),
    @UserName                                nvarchar(256)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
        RETURN 1

    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = 0,
        FailedPasswordAttemptCount = 0,
        FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 ),
        FailedPasswordAnswerAttemptCount = 0,
        FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 ),
        LastLockoutDate = CONVERT( datetime, '17540101', 112 )
    WHERE @UserId = UserId

    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_SetPassword]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_SetPassword]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @NewPassword      nvarchar(128),
    @PasswordSalt     nvarchar(128),
    @CurrentTimeUtc   datetime,
    @PasswordFormat   int = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    UPDATE dbo.aspnet_Membership
    SET Password = @NewPassword, PasswordFormat = @PasswordFormat, PasswordSalt = @PasswordSalt,
        LastPasswordChangedDate = @CurrentTimeUtc
    WHERE @UserId = UserId
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ResetPassword]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_ResetPassword]
    @ApplicationName             nvarchar(256),
    @UserName                    nvarchar(256),
    @NewPassword                 nvarchar(128),
    @MaxInvalidPasswordAttempts  int,
    @PasswordAttemptWindow       int,
    @PasswordSalt                nvarchar(128),
    @CurrentTimeUtc              datetime,
    @PasswordFormat              int = 0,
    @PasswordAnswer              nvarchar(128) = NULL
AS
BEGIN
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @UserId                                 uniqueidentifier
    SET     @UserId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    SELECT @IsLockedOut = IsLockedOut,
           @LastLockoutDate = LastLockoutDate,
           @FailedPasswordAttemptCount = FailedPasswordAttemptCount,
           @FailedPasswordAttemptWindowStart = FailedPasswordAttemptWindowStart,
           @FailedPasswordAnswerAttemptCount = FailedPasswordAnswerAttemptCount,
           @FailedPasswordAnswerAttemptWindowStart = FailedPasswordAnswerAttemptWindowStart
    FROM dbo.aspnet_Membership WITH ( UPDLOCK )
    WHERE @UserId = UserId

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Membership
    SET    Password = @NewPassword,
           LastPasswordChangedDate = @CurrentTimeUtc,
           PasswordFormat = @PasswordFormat,
           PasswordSalt = @PasswordSalt
    WHERE  @UserId = UserId AND
           ( ( @PasswordAnswer IS NULL ) OR ( LOWER( PasswordAnswer ) = LOWER( @PasswordAnswer ) ) )

    IF ( @@ROWCOUNT = 0 )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
    ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            END
        END

    IF( NOT ( @PasswordAnswer IS NULL ) )
    BEGIN
        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByUserId]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByUserId]
    @UserId               uniqueidentifier,
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    IF ( @UpdateLastActivity = 1 )
    BEGIN
        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        FROM     dbo.aspnet_Users
        WHERE    @UserId = UserId

        IF ( @@ROWCOUNT = 0 ) -- User ID not found
            RETURN -1
    END

    SELECT  m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate, m.LastLoginDate, u.LastActivityDate,
            m.LastPasswordChangedDate, u.UserName, m.IsLockedOut,
            m.LastLockoutDate
    FROM    dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   @UserId = u.UserId AND u.UserId = m.UserId

    IF ( @@ROWCOUNT = 0 ) -- User ID not found
       RETURN -1

    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByName]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByName]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier

    IF (@UpdateLastActivity = 1)
    BEGIN
        -- select user ID from aspnet_users table
        SELECT TOP 1 @UserId = u.UserId
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1

        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        WHERE    @UserId = UserId

        SELECT m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut, m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  @UserId = u.UserId AND u.UserId = m.UserId 
    END
    ELSE
    BEGIN
        SELECT TOP 1 m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut,m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1
    END

    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByEmail]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByEmail]
    @ApplicationName  nvarchar(256),
    @Email            nvarchar(256)
AS
BEGIN
    IF( @Email IS NULL )
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                m.ApplicationId = a.ApplicationId AND
                m.LoweredEmail IS NULL
    ELSE
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                m.ApplicationId = a.ApplicationId AND
                LOWER(@Email) = m.LoweredEmail

    IF (@@rowcount = 0)
        RETURN(1)
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPasswordWithFormat]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetPasswordWithFormat]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @UpdateLastLoginActivityDate    bit,
    @CurrentTimeUtc                 datetime
AS
BEGIN
    DECLARE @IsLockedOut                        bit
    DECLARE @UserId                             uniqueidentifier
    DECLARE @Password                           nvarchar(128)
    DECLARE @PasswordSalt                       nvarchar(128)
    DECLARE @PasswordFormat                     int
    DECLARE @FailedPasswordAttemptCount         int
    DECLARE @FailedPasswordAnswerAttemptCount   int
    DECLARE @IsApproved                         bit
    DECLARE @LastActivityDate                   datetime
    DECLARE @LastLoginDate                      datetime

    SELECT  @UserId          = NULL

    SELECT  @UserId = u.UserId, @IsLockedOut = m.IsLockedOut, @Password=Password, @PasswordFormat=PasswordFormat,
            @PasswordSalt=PasswordSalt, @FailedPasswordAttemptCount=FailedPasswordAttemptCount,
		    @FailedPasswordAnswerAttemptCount=FailedPasswordAnswerAttemptCount, @IsApproved=IsApproved,
            @LastActivityDate = LastActivityDate, @LastLoginDate = LastLoginDate
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF (@UserId IS NULL)
        RETURN 1

    IF (@IsLockedOut = 1)
        RETURN 99

    SELECT   @Password, @PasswordFormat, @PasswordSalt, @FailedPasswordAttemptCount,
             @FailedPasswordAnswerAttemptCount, @IsApproved, @LastLoginDate, @LastActivityDate

    IF (@UpdateLastLoginActivityDate = 1 AND @IsApproved = 1)
    BEGIN
        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @CurrentTimeUtc
        WHERE   UserId = @UserId

        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @CurrentTimeUtc
        WHERE   @UserId = UserId
    END


    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPassword]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetPassword]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @PasswordAnswer                 nvarchar(128) = NULL
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @PasswordFormat                         int
    DECLARE @Password                               nvarchar(128)
    DECLARE @passAns                                nvarchar(128)
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @Password = m.Password,
            @passAns = m.PasswordAnswer,
            @PasswordFormat = m.PasswordFormat,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    IF ( NOT( @PasswordAnswer IS NULL ) )
    BEGIN
        IF( ( @passAns IS NULL ) OR ( LOWER( @passAns ) <> LOWER( @PasswordAnswer ) ) )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
        ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            END
        END

        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    IF( @ErrorCode = 0 )
        SELECT @Password, @PasswordFormat

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetNumberOfUsersOnline]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetNumberOfUsersOnline]
    @ApplicationName            nvarchar(256),
    @MinutesSinceLastInActive   int,
    @CurrentTimeUtc             datetime
AS
BEGIN
    DECLARE @DateActive datetime
    SELECT  @DateActive = DATEADD(minute,  -(@MinutesSinceLastInActive), @CurrentTimeUtc)

    DECLARE @NumOnline int
    SELECT  @NumOnline = COUNT(*)
    FROM    dbo.aspnet_Users u(NOLOCK),
            dbo.aspnet_Applications a(NOLOCK),
            dbo.aspnet_Membership m(NOLOCK)
    WHERE   u.ApplicationId = a.ApplicationId                  AND
            LastActivityDate > @DateActive                     AND
            a.LoweredApplicationName = LOWER(@ApplicationName) AND
            u.UserId = m.UserId
    RETURN(@NumOnline)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetAllUsers]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetAllUsers]
    @ApplicationName       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0


    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
    SELECT u.UserId
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u
    WHERE  u.ApplicationId = @ApplicationId AND u.UserId = m.UserId
    ORDER BY u.UserName

    SELECT @TotalRecords = @@ROWCOUNT

    SELECT u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName
    RETURN @TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByName]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByName]
    @ApplicationName       nvarchar(256),
    @UserNameToMatch       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT u.UserId
        FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND u.LoweredUserName LIKE LOWER(@UserNameToMatch)
        ORDER BY u.UserName


    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByEmail]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByEmail]
    @ApplicationName       nvarchar(256),
    @EmailToMatch          nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    IF( @EmailToMatch IS NULL )
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.Email IS NULL
            ORDER BY m.LoweredEmail
    ELSE
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.LoweredEmail LIKE LOWER(@EmailToMatch)
            ORDER BY m.LoweredEmail

    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY m.LoweredEmail

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_CreateUser]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_CreateUser]
    @ApplicationName                        nvarchar(256),
    @UserName                               nvarchar(256),
    @Password                               nvarchar(128),
    @PasswordSalt                           nvarchar(128),
    @Email                                  nvarchar(256),
    @PasswordQuestion                       nvarchar(256),
    @PasswordAnswer                         nvarchar(128),
    @IsApproved                             bit,
    @CurrentTimeUtc                         datetime,
    @CreateDate                             datetime = NULL,
    @UniqueEmail                            int      = 0,
    @PasswordFormat                         int      = 0,
    @UserId                                 uniqueidentifier OUTPUT
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @NewUserId uniqueidentifier
    SELECT @NewUserId = NULL

    DECLARE @IsLockedOut bit
    SET @IsLockedOut = 0

    DECLARE @LastLockoutDate  datetime
    SET @LastLockoutDate = CONVERT( datetime, '17540101', 112 )

    DECLARE @FailedPasswordAttemptCount int
    SET @FailedPasswordAttemptCount = 0

    DECLARE @FailedPasswordAttemptWindowStart  datetime
    SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 )

    DECLARE @FailedPasswordAnswerAttemptCount int
    SET @FailedPasswordAnswerAttemptCount = 0

    DECLARE @FailedPasswordAnswerAttemptWindowStart  datetime
    SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )

    DECLARE @NewUserCreated bit
    DECLARE @ReturnValue   int
    SET @ReturnValue = 0

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    SET @CreateDate = @CurrentTimeUtc

    SELECT  @NewUserId = UserId FROM dbo.aspnet_Users WHERE LOWER(@UserName) = LoweredUserName AND @ApplicationId = ApplicationId
    IF ( @NewUserId IS NULL )
    BEGIN
        SET @NewUserId = @UserId
        EXEC @ReturnValue = dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CreateDate, @NewUserId OUTPUT
        SET @NewUserCreated = 1
    END
    ELSE
    BEGIN
        SET @NewUserCreated = 0
        IF( @NewUserId <> @UserId AND @UserId IS NOT NULL )
        BEGIN
            SET @ErrorCode = 6
            GOTO Cleanup
        END
    END

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @ReturnValue = -1 )
    BEGIN
        SET @ErrorCode = 10
        GOTO Cleanup
    END

    IF ( EXISTS ( SELECT UserId
                  FROM   dbo.aspnet_Membership
                  WHERE  @NewUserId = UserId ) )
    BEGIN
        SET @ErrorCode = 6
        GOTO Cleanup
    END

    SET @UserId = @NewUserId

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership m WITH ( UPDLOCK, HOLDLOCK )
                    WHERE ApplicationId = @ApplicationId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            SET @ErrorCode = 7
            GOTO Cleanup
        END
    END

    IF (@NewUserCreated = 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate = @CreateDate
        WHERE  @UserId = UserId
        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    INSERT INTO dbo.aspnet_Membership
                ( ApplicationId,
                  UserId,
                  Password,
                  PasswordSalt,
                  Email,
                  LoweredEmail,
                  PasswordQuestion,
                  PasswordAnswer,
                  PasswordFormat,
                  IsApproved,
                  IsLockedOut,
                  CreateDate,
                  LastLoginDate,
                  LastPasswordChangedDate,
                  LastLockoutDate,
                  FailedPasswordAttemptCount,
                  FailedPasswordAttemptWindowStart,
                  FailedPasswordAnswerAttemptCount,
                  FailedPasswordAnswerAttemptWindowStart )
         VALUES ( @ApplicationId,
                  @UserId,
                  @Password,
                  @PasswordSalt,
                  @Email,
                  LOWER(@Email),
                  @PasswordQuestion,
                  @PasswordAnswer,
                  @PasswordFormat,
                  @IsApproved,
                  @IsLockedOut,
                  @CreateDate,
                  @CreateDate,
                  @CreateDate,
                  @LastLockoutDate,
                  @FailedPasswordAttemptCount,
                  @FailedPasswordAttemptWindowStart,
                  @FailedPasswordAnswerAttemptCount,
                  @FailedPasswordAnswerAttemptWindowStart )

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]
    @ApplicationName       nvarchar(256),
    @UserName              nvarchar(256),
    @NewPasswordQuestion   nvarchar(256),
    @NewPasswordAnswer     nvarchar(128)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Membership m, dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId
    IF (@UserId IS NULL)
    BEGIN
        RETURN(1)
    END

    UPDATE dbo.aspnet_Membership
    SET    PasswordQuestion = @NewPasswordQuestion, PasswordAnswer = @NewPasswordAnswer
    WHERE  UserId=@UserId
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_AnyDataInTables]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_AnyDataInTables]
    @TablesToCheck int
AS
BEGIN
    -- Check Membership table if (@TablesToCheck & 1) is set
    IF ((@TablesToCheck & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_MembershipUsers') AND (type = 'V'))))
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Membership))
        BEGIN
            SELECT N'aspnet_Membership'
            RETURN
        END
    END

    -- Check aspnet_Roles table if (@TablesToCheck & 2) is set
    IF ((@TablesToCheck & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_Roles') AND (type = 'V'))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 RoleId FROM dbo.aspnet_Roles))
        BEGIN
            SELECT N'aspnet_Roles'
            RETURN
        END
    END

    -- Check aspnet_Profile table if (@TablesToCheck & 4) is set
    IF ((@TablesToCheck & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_Profiles') AND (type = 'V'))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Profile))
        BEGIN
            SELECT N'aspnet_Profile'
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 8) is set
    IF ((@TablesToCheck & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_WebPartState_User') AND (type = 'V'))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_PersonalizationPerUser))
        BEGIN
            SELECT N'aspnet_PersonalizationPerUser'
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 16) is set
    IF ((@TablesToCheck & 16) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'aspnet_WebEvent_LogEvent') AND (type = 'P'))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 * FROM dbo.aspnet_WebEvent_Events))
        BEGIN
            SELECT N'aspnet_WebEvent_Events'
            RETURN
        END
    END

    -- Check aspnet_Users table if (@TablesToCheck & 1,2,4 & 8) are all set
    IF ((@TablesToCheck & 1) <> 0 AND
        (@TablesToCheck & 2) <> 0 AND
        (@TablesToCheck & 4) <> 0 AND
        (@TablesToCheck & 8) <> 0 AND
        (@TablesToCheck & 32) <> 0 AND
        (@TablesToCheck & 128) <> 0 AND
        (@TablesToCheck & 256) <> 0 AND
        (@TablesToCheck & 512) <> 0 AND
        (@TablesToCheck & 1024) <> 0)
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Users))
        BEGIN
            SELECT N'aspnet_Users'
            RETURN
        END
        IF (EXISTS(SELECT TOP 1 ApplicationId FROM dbo.aspnet_Applications))
        BEGIN
            SELECT N'aspnet_Applications'
            RETURN
        END
    END
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetUserState]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetUserState] (
    @Count                  int                 OUT,
    @ApplicationName        NVARCHAR(256),
    @InactiveSinceDate      DATETIME            = NULL,
    @UserName               NVARCHAR(256)       = NULL,
    @Path                   NVARCHAR(256)       = NULL)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser
        WHERE Id IN (SELECT PerUser.Id
                     FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
                     WHERE Paths.ApplicationId = @ApplicationId
                           AND PerUser.UserId = Users.UserId
                           AND PerUser.PathId = Paths.PathId
                           AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
                           AND (@UserName IS NULL OR Users.LoweredUserName = LOWER(@UserName))
                           AND (@Path IS NULL OR Paths.LoweredPath = LOWER(@Path)))

        SELECT @Count = @@ROWCOUNT
    END
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetSharedState]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetSharedState] (
    @Count int OUT,
    @ApplicationName NVARCHAR(256),
    @Path NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationAllUsers
        WHERE PathId IN
            (SELECT AllUsers.PathId
             FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
             WHERE Paths.ApplicationId = @ApplicationId
                   AND AllUsers.PathId = Paths.PathId
                   AND Paths.LoweredPath = LOWER(@Path))

        SELECT @Count = @@ROWCOUNT
    END
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_GetCountOfState]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_GetCountOfState] (
    @Count int OUT,
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @Path NVARCHAR(256) = NULL,
    @UserName NVARCHAR(256) = NULL,
    @InactiveSinceDate DATETIME = NULL)
AS
BEGIN

    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
        IF (@AllUsersScope = 1)
            SELECT @Count = COUNT(*)
            FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
            WHERE Paths.ApplicationId = @ApplicationId
                  AND AllUsers.PathId = Paths.PathId
                  AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
        ELSE
            SELECT @Count = COUNT(*)
            FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
            WHERE Paths.ApplicationId = @ApplicationId
                  AND PerUser.UserId = Users.UserId
                  AND PerUser.PathId = Paths.PathId
                  AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
                  AND (@UserName IS NULL OR Users.LoweredUserName LIKE LOWER(@UserName))
                  AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_FindState]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_FindState] (
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @PageIndex              INT,
    @PageSize               INT,
    @Path NVARCHAR(256) = NULL,
    @UserName NVARCHAR(256) = NULL,
    @InactiveSinceDate DATETIME = NULL)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        RETURN

    -- Set the page bounds
    DECLARE @PageLowerBound INT
    DECLARE @PageUpperBound INT
    DECLARE @TotalRecords   INT
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table to store the selected results
    CREATE TABLE #PageIndex (
        IndexId int IDENTITY (0, 1) NOT NULL,
        ItemId UNIQUEIDENTIFIER
    )

    IF (@AllUsersScope = 1)
    BEGIN
        -- Insert into our temp table
        INSERT INTO #PageIndex (ItemId)
        SELECT Paths.PathId
        FROM dbo.aspnet_Paths Paths,
             ((SELECT Paths.PathId
               FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
               WHERE Paths.ApplicationId = @ApplicationId
                      AND AllUsers.PathId = Paths.PathId
                      AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              ) AS SharedDataPerPath
              FULL OUTER JOIN
              (SELECT DISTINCT Paths.PathId
               FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Paths Paths
               WHERE Paths.ApplicationId = @ApplicationId
                      AND PerUser.PathId = Paths.PathId
                      AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              ) AS UserDataPerPath
              ON SharedDataPerPath.PathId = UserDataPerPath.PathId
             )
        WHERE Paths.PathId = SharedDataPerPath.PathId OR Paths.PathId = UserDataPerPath.PathId
        ORDER BY Paths.Path ASC

        SELECT @TotalRecords = @@ROWCOUNT

        SELECT Paths.Path,
               SharedDataPerPath.LastUpdatedDate,
               SharedDataPerPath.SharedDataLength,
               UserDataPerPath.UserDataLength,
               UserDataPerPath.UserCount
        FROM dbo.aspnet_Paths Paths,
             ((SELECT PageIndex.ItemId AS PathId,
                      AllUsers.LastUpdatedDate AS LastUpdatedDate,
                      DATALENGTH(AllUsers.PageSettings) AS SharedDataLength
               FROM dbo.aspnet_PersonalizationAllUsers AllUsers, #PageIndex PageIndex
               WHERE AllUsers.PathId = PageIndex.ItemId
                     AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
              ) AS SharedDataPerPath
              FULL OUTER JOIN
              (SELECT PageIndex.ItemId AS PathId,
                      SUM(DATALENGTH(PerUser.PageSettings)) AS UserDataLength,
                      COUNT(*) AS UserCount
               FROM aspnet_PersonalizationPerUser PerUser, #PageIndex PageIndex
               WHERE PerUser.PathId = PageIndex.ItemId
                     AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
               GROUP BY PageIndex.ItemId
              ) AS UserDataPerPath
              ON SharedDataPerPath.PathId = UserDataPerPath.PathId
             )
        WHERE Paths.PathId = SharedDataPerPath.PathId OR Paths.PathId = UserDataPerPath.PathId
        ORDER BY Paths.Path ASC
    END
    ELSE
    BEGIN
        -- Insert into our temp table
        INSERT INTO #PageIndex (ItemId)
        SELECT PerUser.Id
        FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
        WHERE Paths.ApplicationId = @ApplicationId
              AND PerUser.UserId = Users.UserId
              AND PerUser.PathId = Paths.PathId
              AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              AND (@UserName IS NULL OR Users.LoweredUserName LIKE LOWER(@UserName))
              AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
        ORDER BY Paths.Path ASC, Users.UserName ASC

        SELECT @TotalRecords = @@ROWCOUNT

        SELECT Paths.Path, PerUser.LastUpdatedDate, DATALENGTH(PerUser.PageSettings), Users.UserName, Users.LastActivityDate
        FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths, #PageIndex PageIndex
        WHERE PerUser.Id = PageIndex.ItemId
              AND PerUser.UserId = Users.UserId
              AND PerUser.PathId = Paths.PathId
              AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
        ORDER BY Paths.Path ASC, Users.UserName ASC
    END

    RETURN @TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_DeleteAllState]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_DeleteAllState] (
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @Count int OUT)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        IF (@AllUsersScope = 1)
            DELETE FROM aspnet_PersonalizationAllUsers
            WHERE PathId IN
               (SELECT Paths.PathId
                FROM dbo.aspnet_Paths Paths
                WHERE Paths.ApplicationId = @ApplicationId)
        ELSE
            DELETE FROM aspnet_PersonalizationPerUser
            WHERE PathId IN
               (SELECT Paths.PathId
                FROM dbo.aspnet_Paths Paths
                WHERE Paths.ApplicationId = @ApplicationId)

        SELECT @Count = @@ROWCOUNT
    END
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_SetPageSettings]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_SetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @PageSettings     IMAGE,
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Paths_CreatePath @ApplicationId, @Path, @PathId OUTPUT
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CurrentTimeUtc, @UserId OUTPUT
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    IF (EXISTS(SELECT PathId FROM dbo.aspnet_PersonalizationPerUser WHERE UserId = @UserId AND PathId = @PathId))
        UPDATE dbo.aspnet_PersonalizationPerUser SET PageSettings = @PageSettings, LastUpdatedDate = @CurrentTimeUtc WHERE UserId = @UserId AND PathId = @PathId
    ELSE
        INSERT INTO dbo.aspnet_PersonalizationPerUser(UserId, PathId, PageSettings, LastUpdatedDate) VALUES (@UserId, @PathId, @PageSettings, @CurrentTimeUtc)
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        RETURN
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE PathId = @PathId AND UserId = @UserId
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_GetPageSettings]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_GetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        RETURN
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    SELECT p.PageSettings FROM dbo.aspnet_PersonalizationPerUser p WHERE p.PathId = @PathId AND p.UserId = @UserId
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path             NVARCHAR(256),
    @PageSettings     IMAGE,
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Paths_CreatePath @ApplicationId, @Path, @PathId OUTPUT
    END

    IF (EXISTS(SELECT PathId FROM dbo.aspnet_PersonalizationAllUsers WHERE PathId = @PathId))
        UPDATE dbo.aspnet_PersonalizationAllUsers SET PageSettings = @PageSettings, LastUpdatedDate = @CurrentTimeUtc WHERE PathId = @PathId
    ELSE
        INSERT INTO dbo.aspnet_PersonalizationAllUsers(PathId, PageSettings, LastUpdatedDate) VALUES (@PathId, @PageSettings, @CurrentTimeUtc)
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path              NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    DELETE FROM dbo.aspnet_PersonalizationAllUsers WHERE PathId = @PathId
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path              NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT p.PageSettings FROM dbo.aspnet_PersonalizationAllUsers p WHERE p.PathId = @PathId
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_SetProperties]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_SetProperties]
    @ApplicationName        nvarchar(256),
    @PropertyNames          ntext,
    @PropertyValuesString   ntext,
    @PropertyValuesBinary   image,
    @UserName               nvarchar(256),
    @IsUserAnonymous        bit,
    @CurrentTimeUtc         datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
       BEGIN TRANSACTION
       SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    DECLARE @UserId uniqueidentifier
    DECLARE @LastActivityDate datetime
    SELECT  @UserId = NULL
    SELECT  @LastActivityDate = @CurrentTimeUtc

    SELECT @UserId = UserId
    FROM   dbo.aspnet_Users
    WHERE  ApplicationId = @ApplicationId AND LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
        EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, @IsUserAnonymous, @LastActivityDate, @UserId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Users
    SET    LastActivityDate=@CurrentTimeUtc
    WHERE  UserId = @UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS( SELECT *
               FROM   dbo.aspnet_Profile
               WHERE  UserId = @UserId))
        UPDATE dbo.aspnet_Profile
        SET    PropertyNames=@PropertyNames, PropertyValuesString = @PropertyValuesString,
               PropertyValuesBinary = @PropertyValuesBinary, LastUpdatedDate=@CurrentTimeUtc
        WHERE  UserId = @UserId
    ELSE
        INSERT INTO dbo.aspnet_Profile(UserId, PropertyNames, PropertyValuesString, PropertyValuesBinary, LastUpdatedDate)
             VALUES (@UserId, @PropertyNames, @PropertyValuesString, @PropertyValuesBinary, @CurrentTimeUtc)

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
    	SET @TranStarted = 0
    	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProperties]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_GetProperties]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL

    SELECT @UserId = UserId
    FROM   dbo.aspnet_Users
    WHERE  ApplicationId = @ApplicationId AND LoweredUserName = LOWER(@UserName)

    IF (@UserId IS NULL)
        RETURN
    SELECT TOP 1 PropertyNames, PropertyValuesString, PropertyValuesBinary
    FROM         dbo.aspnet_Profile
    WHERE        UserId = @UserId

    IF (@@ROWCOUNT > 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate=@CurrentTimeUtc
        WHERE  UserId = @UserId
    END
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProfiles]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_GetProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @PageIndex              int,
    @PageSize               int,
    @UserNameToMatch        nvarchar(256) = NULL,
    @InactiveSinceDate      datetime      = NULL
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT  u.UserId
        FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p
        WHERE   ApplicationId = @ApplicationId
            AND u.UserId = p.UserId
            AND (@InactiveSinceDate IS NULL OR LastActivityDate <= @InactiveSinceDate)
            AND (     (@ProfileAuthOptions = 2)
                   OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                   OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
                 )
            AND (@UserNameToMatch IS NULL OR LoweredUserName LIKE LOWER(@UserNameToMatch))
        ORDER BY UserName

    SELECT  u.UserName, u.IsAnonymous, u.LastActivityDate, p.LastUpdatedDate,
            DATALENGTH(p.PropertyNames) + DATALENGTH(p.PropertyValuesString) + DATALENGTH(p.PropertyValuesBinary)
    FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p, #PageIndexForUsers i
    WHERE   u.UserId = p.UserId AND p.UserId = i.UserId AND i.IndexId >= @PageLowerBound AND i.IndexId <= @PageUpperBound

    SELECT COUNT(*)
    FROM   #PageIndexForUsers

    DROP TABLE #PageIndexForUsers
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @InactiveSinceDate      datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
    BEGIN
        SELECT 0
        RETURN
    END

    SELECT  COUNT(*)
    FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p
    WHERE   ApplicationId = @ApplicationId
        AND u.UserId = p.UserId
        AND (LastActivityDate <= @InactiveSinceDate)
        AND (
                (@ProfileAuthOptions = 2)
                OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
            )
END
GO
/****** Object:  View [dbo].[ProdsInCats]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ProdsInCats]
AS
SELECT dbo.Product.ProductID, dbo.Product.Name, dbo.Product.Description, dbo.Product.Price, dbo.Product.Thumbnail, dbo.ProductCategory.CategoryID
FROM   dbo.Product INNER JOIN
            dbo.ProductCategory ON dbo.Product.ProductID = dbo.ProductCategory.ProductID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Product"
            Begin Extent = 
               Top = 9
               Left = 57
               Bottom = 156
               Right = 250
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "ProductCategory"
            Begin Extent = 
               Top = 9
               Left = 307
               Bottom = 114
               Right = 500
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ProdsInCats'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ProdsInCats'
GO
/****** Object:  StoredProcedure [dbo].[SearchCatalog]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SearchCatalog] 
(@DescriptionLength INT,
 @PageNumber TINYINT,
 @ProductsPerPage TINYINT,
 @HowManyResults INT OUTPUT,
 @AllWords BIT,
 @Word1 NVARCHAR(15) = NULL,
 @Word2 NVARCHAR(15) = NULL,
 @Word3 NVARCHAR(15) = NULL,
 @Word4 NVARCHAR(15) = NULL,
 @Word5 NVARCHAR(15) = NULL)
AS

/* @NecessaryMatches needs to be 1 for any-word searches and
   the number of words for all-words searches */
DECLARE @NecessaryMatches INT
SET @NecessaryMatches = 1
IF @AllWords = 1 
  SET @NecessaryMatches =
    CASE WHEN @Word1 IS NULL THEN 0 ELSE 1 END + 
    CASE WHEN @Word2 IS NULL THEN 0 ELSE 1 END + 
    CASE WHEN @Word3 IS NULL THEN 0 ELSE 1 END +
    CASE WHEN @Word4 IS NULL THEN 0 ELSE 1 END +
    CASE WHEN @Word5 IS NULL THEN 0 ELSE 1 END;

/* Create the table variable that will contain the search results */
DECLARE @Matches TABLE
([Key] INT NOT NULL,
 Rank INT NOT NULL)

-- Save matches for the first word
IF @Word1 IS NOT NULL
  INSERT INTO @Matches
  EXEC SearchWord @Word1

-- Save the matches for the second word
IF @Word2 IS NOT NULL
  INSERT INTO @Matches
  EXEC SearchWord @Word2

-- Save the matches for the third word
IF @Word3 IS NOT NULL
  INSERT INTO @Matches
  EXEC SearchWord @Word3

-- Save the matches for the fourth word
IF @Word4 IS NOT NULL
  INSERT INTO @Matches
  EXEC SearchWord @Word4

-- Save the matches for the fifth word
IF @Word5 IS NOT NULL
  INSERT INTO @Matches
  EXEC SearchWord @Word5

-- Calculate the IDs of the matching products
DECLARE @Results TABLE
(RowNumber INT,
 [KEY] INT NOT NULL,
 Rank INT NOT NULL)

-- Obtain the matching products 
INSERT INTO @Results
SELECT ROW_NUMBER() OVER (ORDER BY COUNT(M.Rank) DESC),
       M.[KEY], SUM(M.Rank) AS TotalRank
FROM @Matches M
GROUP BY M.[KEY]
HAVING COUNT(M.Rank) >= @NecessaryMatches

-- return the total number of results using an OUTPUT variable
SELECT @HowManyResults = COUNT(*) FROM @Results

-- populate the table variable with the complete list of products
SELECT Product.ProductID, Name,
       CASE WHEN LEN(Description) <= @DescriptionLength THEN Description 
            ELSE SUBSTRING(Description, 1, @DescriptionLength) + '...' END 
       AS Description, Price, Image, Stock 
FROM Product 
INNER JOIN @Results R
ON Product.ProductID = R.[KEY]
WHERE R.RowNumber > (@PageNumber - 1) * @ProductsPerPage
  AND R.RowNumber <= @PageNumber * @ProductsPerPage
ORDER BY R.Rank DESC
GO
/****** Object:  StoredProcedure [dbo].[RateAndCommentProduct]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RateAndCommentProduct]
	(@userId char(36),
	@productId int,
	@rate int,
	@comment nvarchar(1000))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	INSERT INTO ProductRating(UserID, ProductID, rate, comment, created) 
	values (@userId, @productId, @rate, @comment, getdate());
    
	
END
GO
/****** Object:  StoredProcedure [dbo].[CatalogRemoveProductFromCategory]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogRemoveProductFromCategory]
(@ProductID int, @CategoryID int)
AS
DELETE FROM ProductCategory
WHERE CategoryID = @CategoryID AND ProductID = @ProductID
GO
/****** Object:  StoredProcedure [dbo].[CatalogMoveProductToCategory]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogMoveProductToCategory]
(@ProductID int, @OldCategoryID int, @NewCategoryID int)
AS
UPDATE ProductCategory
SET CategoryID = @NewCategoryID
WHERE CategoryID = @OldCategoryID
  AND ProductID = @ProductID
GO
/****** Object:  StoredProcedure [dbo].[CatalogGetProductsInCategory]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CatalogGetProductsInCategory]
(@CategoryID INT,
@DescriptionLength INT,
@PageNumber INT,
@ProductsPerPage INT,
@HowManyProducts INT OUTPUT)
AS

-- declare a new TABLE variable
DECLARE @Products TABLE
(RowNumber INT,
 ProductID INT,
 Name NVARCHAR(100),
 Description NVARCHAR(MAX),
 Price MONEY,
 Stock INT,
 Image NVARCHAR(50))


-- populate the table variable with the complete list of products
INSERT INTO @Products
SELECT ROW_NUMBER() OVER (ORDER BY Product.ProductID),
       Product.ProductID, Name,
       CASE WHEN LEN(Description) <= @DescriptionLength THEN Description 
            ELSE SUBSTRING(Description, 1, @DescriptionLength) + '...' END 
       AS Description, Price, Stock, Image
FROM Product INNER JOIN ProductCategory
  ON Product.ProductID = ProductCategory.ProductID
WHERE ProductCategory.CategoryID = @CategoryID

-- return the total number of products using an OUTPUT variable
SELECT @HowManyProducts = COUNT(ProductID) FROM @Products

-- extract the requested page of products
SELECT ProductID, Name, Description, Price, Stock,
       Image
FROM @Products
WHERE RowNumber > (@PageNumber - 1) * @ProductsPerPage
  AND RowNumber <= @PageNumber * @ProductsPerPage
GO
/****** Object:  View [dbo].[vw_aspnet_Profiles]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_Profiles]
  AS SELECT [dbo].[aspnet_Profile].[UserId], [dbo].[aspnet_Profile].[LastUpdatedDate],
      [DataSize]=  DATALENGTH([dbo].[aspnet_Profile].[PropertyNames])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesString])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesBinary])
  FROM [dbo].[aspnet_Profile]
GO
/****** Object:  View [dbo].[vw_aspnet_MembershipUsers]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_MembershipUsers]
  AS SELECT [dbo].[aspnet_Membership].[UserId],
            [dbo].[aspnet_Membership].[PasswordFormat],
            [dbo].[aspnet_Membership].[MobilePIN],
            [dbo].[aspnet_Membership].[Email],
            [dbo].[aspnet_Membership].[LoweredEmail],
            [dbo].[aspnet_Membership].[PasswordQuestion],
            [dbo].[aspnet_Membership].[PasswordAnswer],
            [dbo].[aspnet_Membership].[IsApproved],
            [dbo].[aspnet_Membership].[IsLockedOut],
            [dbo].[aspnet_Membership].[CreateDate],
            [dbo].[aspnet_Membership].[LastLoginDate],
            [dbo].[aspnet_Membership].[LastPasswordChangedDate],
            [dbo].[aspnet_Membership].[LastLockoutDate],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptWindowStart],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptWindowStart],
            [dbo].[aspnet_Membership].[Comment],
            [dbo].[aspnet_Users].[ApplicationId],
            [dbo].[aspnet_Users].[UserName],
            [dbo].[aspnet_Users].[MobileAlias],
            [dbo].[aspnet_Users].[IsAnonymous],
            [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Membership] INNER JOIN [dbo].[aspnet_Users]
      ON [dbo].[aspnet_Membership].[UserId] = [dbo].[aspnet_Users].[UserId]
GO
/****** Object:  StoredProcedure [dbo].[ShoppingCartRemoveItem]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ShoppingCartRemoveItem]
(@CartID char(36),
 @ProductID int)
AS
DELETE FROM ShoppingCart
WHERE CartID = @CartID and ProductID = @ProductID
GO
/****** Object:  StoredProcedure [dbo].[ShoppingCartGetTotalAmount]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ShoppingCartGetTotalAmount]
(@CartID char(36))
AS
SELECT ISNULL(SUM(Product.Price * ShoppingCart.Quantity), 0)
FROM ShoppingCart INNER JOIN Product
ON ShoppingCart.ProductID = Product.ProductID
WHERE ShoppingCart.CartID = @CartID
GO
/****** Object:  StoredProcedure [dbo].[ShoppingCartGetItems]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ShoppingCartGetItems]
(@CartID char(36))
AS
SELECT Product.ProductID, Product.Image, Product.Name, ShoppingCart.Attributes, Product.Price, ShoppingCart.Quantity,Product.Price * ShoppingCart.Quantity AS Subtotal
FROM ShoppingCart INNER JOIN Product
ON ShoppingCart.ProductID = Product.ProductID
WHERE ShoppingCart.CartID = @CartID
GO
/****** Object:  StoredProcedure [dbo].[ShoppingCartDeleteOldCarts]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ShoppingCartDeleteOldCarts]
(@Days smallint)
AS
DELETE FROM ShoppingCart
WHERE CartID IN
(SELECT CartID
FROM ShoppingCart
GROUP BY CartID
HAVING MIN(DATEDIFF(dd,DateAdded,GETDATE())) >= @Days)
GO
/****** Object:  StoredProcedure [dbo].[ShoppingCartCountOldCarts]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ShoppingCartCountOldCarts]
(@Days smallint)
AS
SELECT COUNT(CartID)
FROM ShoppingCart
WHERE CartID IN
(SELECT CartID
FROM ShoppingCart
GROUP BY CartID
HAVING MIN(DATEDIFF(dd,DateAdded,GETDATE())) >= @Days)
GO
/****** Object:  StoredProcedure [dbo].[ShoppingCartAddItem]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ShoppingCartAddItem]
(@CartID char(36),
 @ProductID int,
 @Attributes nvarchar(1000))
AS
IF EXISTS
        (SELECT CartID
         FROM ShoppingCart
         WHERE ProductID = @ProductID AND CartID = @CartID)
    UPDATE ShoppingCart
    SET Quantity = Quantity + 1
    WHERE ProductID = @ProductID AND CartID = @CartID
ELSE
    IF EXISTS (SELECT Name FROM Product WHERE ProductID=@ProductID)
        INSERT INTO ShoppingCart (CartID, ProductID, Attributes, Quantity, DateAdded)
        VALUES (@CartID, @ProductID, @Attributes, 1, GETDATE())
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_User]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_WebPartState_User]
  AS SELECT [dbo].[aspnet_PersonalizationPerUser].[PathId], [dbo].[aspnet_PersonalizationPerUser].[UserId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationPerUser].[PageSettings]), [dbo].[aspnet_PersonalizationPerUser].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationPerUser]
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Shared]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_WebPartState_Shared]
  AS SELECT [dbo].[aspnet_PersonalizationAllUsers].[PathId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationAllUsers].[PageSettings]), [dbo].[aspnet_PersonalizationAllUsers].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationAllUsers]
GO
/****** Object:  View [dbo].[vw_aspnet_UsersInRoles]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_UsersInRoles]
  AS SELECT [dbo].[aspnet_UsersInRoles].[UserId], [dbo].[aspnet_UsersInRoles].[RoleId]
  FROM [dbo].[aspnet_UsersInRoles]
GO
/****** Object:  StoredProcedure [dbo].[ShoppingCartUpdateItem]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ShoppingCartUpdateItem]
(@CartID char(36),
 @ProductID int,
 @Quantity int)
AS
IF @Quantity <= 0
  EXEC ShoppingCartRemoveItem @CartID, @ProductID
ELSE
  UPDATE ShoppingCart
  SET Quantity = @Quantity, DateAdded = GETDATE()
  WHERE ProductID = @ProductID AND CartID = @CartID
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteProfiles]    Script Date: 04/14/2013 12:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_DeleteProfiles]
    @ApplicationName        nvarchar(256),
    @UserNames              nvarchar(4000)
AS
BEGIN
    DECLARE @UserName     nvarchar(256)
    DECLARE @CurrentPos   int
    DECLARE @NextPos      int
    DECLARE @NumDeleted   int
    DECLARE @DeletedUser  int
    DECLARE @TranStarted  bit
    DECLARE @ErrorCode    int

    SET @ErrorCode = 0
    SET @CurrentPos = 1
    SET @NumDeleted = 0
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    WHILE (@CurrentPos <= LEN(@UserNames))
    BEGIN
        SELECT @NextPos = CHARINDEX(N',', @UserNames,  @CurrentPos)
        IF (@NextPos = 0 OR @NextPos IS NULL)
            SELECT @NextPos = LEN(@UserNames) + 1

        SELECT @UserName = SUBSTRING(@UserNames, @CurrentPos, @NextPos - @CurrentPos)
        SELECT @CurrentPos = @NextPos+1

        IF (LEN(@UserName) > 0)
        BEGIN
            SELECT @DeletedUser = 0
            EXEC dbo.aspnet_Users_DeleteUser @ApplicationName, @UserName, 4, @DeletedUser OUTPUT
            IF( @@ERROR <> 0 )
            BEGIN
                SET @ErrorCode = -1
                GOTO Cleanup
            END
            IF (@DeletedUser <> 0)
                SELECT @NumDeleted = @NumDeleted + 1
        END
    END
    SELECT @NumDeleted
    IF (@TranStarted = 1)
    BEGIN
    	SET @TranStarted = 0
    	COMMIT TRANSACTION
    END
    SET @TranStarted = 0

    RETURN 0

Cleanup:
    IF (@TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END
    RETURN @ErrorCode
END
GO
/****** Object:  Default [DF__aspnet_Ap__Appli__3C34F16F]    Script Date: 04/14/2013 12:55:27 ******/
ALTER TABLE [dbo].[aspnet_Applications] ADD  DEFAULT (newid()) FOR [ApplicationId]
GO
/****** Object:  Default [DF_Orders_DateCreated]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[Orders] ADD  CONSTRAINT [DF_Orders_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_Orders_Verified]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[Orders] ADD  CONSTRAINT [DF_Orders_Verified]  DEFAULT ((0)) FOR [Verified]
GO
/****** Object:  Default [DF_Orders_Completed]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[Orders] ADD  CONSTRAINT [DF_Orders_Completed]  DEFAULT ((0)) FOR [Completed]
GO
/****** Object:  Default [DF_Orders_Canceled]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[Orders] ADD  CONSTRAINT [DF_Orders_Canceled]  DEFAULT ((0)) FOR [Canceled]
GO
/****** Object:  Default [DF__aspnet_Us__UserI__40F9A68C]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_Users] ADD  DEFAULT (newid()) FOR [UserId]
GO
/****** Object:  Default [DF__aspnet_Us__Mobil__41EDCAC5]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_Users] ADD  DEFAULT (NULL) FOR [MobileAlias]
GO
/****** Object:  Default [DF__aspnet_Us__IsAno__42E1EEFE]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_Users] ADD  DEFAULT ((0)) FOR [IsAnonymous]
GO
/****** Object:  Default [DF__aspnet_Pa__PathI__43D61337]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_Paths] ADD  DEFAULT (newid()) FOR [PathId]
GO
/****** Object:  Default [DF__aspnet_Ro__RoleI__44CA3770]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_Roles] ADD  DEFAULT (newid()) FOR [RoleId]
GO
/****** Object:  Default [DF__aspnet_Perso__Id__45BE5BA9]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] ADD  DEFAULT (newid()) FOR [Id]
GO
/****** Object:  Default [DF__aspnet_Me__Passw__46B27FE2]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_Membership] ADD  DEFAULT ((0)) FOR [PasswordFormat]
GO
/****** Object:  ForeignKey [FK_OrderDetail_Orders]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_Orders] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Orders] ([OrderID])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_Orders]
GO
/****** Object:  ForeignKey [FK_Category_Department]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[Category]  WITH CHECK ADD  CONSTRAINT [FK_Category_Department] FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[Department] ([DepartmentID])
GO
ALTER TABLE [dbo].[Category] CHECK CONSTRAINT [FK_Category_Department]
GO
/****** Object:  ForeignKey [FK_AttributeValue_Attribute]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[AttributeValue]  WITH CHECK ADD  CONSTRAINT [FK_AttributeValue_Attribute] FOREIGN KEY([AttributeID])
REFERENCES [dbo].[Attribute] ([AttributeID])
GO
ALTER TABLE [dbo].[AttributeValue] CHECK CONSTRAINT [FK_AttributeValue_Attribute]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__Appli__4A8310C6]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_Users]  WITH CHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
/****** Object:  ForeignKey [FK__aspnet_Pa__Appli__4B7734FF]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_Paths]  WITH CHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
/****** Object:  ForeignKey [FK__aspnet_Ro__Appli__4C6B5938]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_Roles]  WITH CHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
/****** Object:  ForeignKey [FK_Product_Brand]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Brand] FOREIGN KEY([BrandID])
REFERENCES [dbo].[Brand] ([BrandID])
ON UPDATE CASCADE
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Brand]
GO
/****** Object:  ForeignKey [FK_ShoppingCart_Product]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[ShoppingCart]  WITH CHECK ADD  CONSTRAINT [FK_ShoppingCart_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[ShoppingCart] CHECK CONSTRAINT [FK_ShoppingCart_Product]
GO
/****** Object:  ForeignKey [FK_ProductRating_Product]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[ProductRating]  WITH CHECK ADD  CONSTRAINT [FK_ProductRating_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProductRating] CHECK CONSTRAINT [FK_ProductRating_Product]
GO
/****** Object:  ForeignKey [FK_ProductRating_User]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[ProductRating]  WITH CHECK ADD  CONSTRAINT [FK_ProductRating_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[aspnet_Users] ([UserId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProductRating] CHECK CONSTRAINT [FK_ProductRating_User]
GO
/****** Object:  ForeignKey [FK_ProductCategory_Category]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[ProductCategory]  WITH CHECK ADD  CONSTRAINT [FK_ProductCategory_Category] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([CategoryID])
GO
ALTER TABLE [dbo].[ProductCategory] CHECK CONSTRAINT [FK_ProductCategory_Category]
GO
/****** Object:  ForeignKey [FK_ProductCategory_Product]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[ProductCategory]  WITH CHECK ADD  CONSTRAINT [FK_ProductCategory_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[ProductCategory] CHECK CONSTRAINT [FK_ProductCategory_Product]
GO
/****** Object:  ForeignKey [FK_ProductAttributeValue_AttributeValue]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[ProductAttributeValue]  WITH CHECK ADD  CONSTRAINT [FK_ProductAttributeValue_AttributeValue] FOREIGN KEY([AttributeValueID])
REFERENCES [dbo].[AttributeValue] ([AttributeValueID])
GO
ALTER TABLE [dbo].[ProductAttributeValue] CHECK CONSTRAINT [FK_ProductAttributeValue_AttributeValue]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__540C7B00]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser]  WITH CHECK ADD FOREIGN KEY([PathId])
REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__UserI__55009F39]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
/****** Object:  ForeignKey [FK__aspnet_Pr__UserI__55F4C372]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_Profile]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
/****** Object:  ForeignKey [FK__aspnet_Me__Appli__56E8E7AB]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_Membership]  WITH CHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
/****** Object:  ForeignKey [FK__aspnet_Me__UserI__57DD0BE4]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_Membership]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__58D1301D]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers]  WITH CHECK ADD FOREIGN KEY([PathId])
REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
/****** Object:  ForeignKey [FK__aspnet_Us__RoleI__59C55456]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_UsersInRoles]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[aspnet_Roles] ([RoleId])
GO
/****** Object:  ForeignKey [FK__aspnet_Us__UserI__5AB9788F]    Script Date: 04/14/2013 12:55:28 ******/
ALTER TABLE [dbo].[aspnet_UsersInRoles]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
